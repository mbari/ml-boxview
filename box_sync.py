# -*- coding: utf-8 -*-
"""
box_sync.py -- Class to sync LCM box list messages and publish stereo box messages
Author: pldr
Copyright 2020  Monterey Bay Aquarium Research Institute
Distributed under MIT license. See license.txt for more information.

"""

import lcm
import time
import numpy as np
from collections import deque
from mwt.stereo_bounding_box_t import stereo_bounding_box_t
from mwt.bounding_box_list_t import bounding_box_list_t

class BoxSync:

    def __init__(self, max_wait=100):
        self.left_boxes = None
        self.right_boxes = None
        self.l_buf = deque()
        self.r_buf = deque()
        self.left_channel = 'BOX_LEFT'
        self.right_channel = 'BOX_RIGHT'
        self.lc = lcm.LCM("udpm://239.255.76.67:7667?ttl=0")

    def __del__(self):
        self.wait()

    def stop(self):
        self.stop_thread = True

    def handle_box(self, channel, data):
        if channel == self.left_channel:
            self.left_boxes = bounding_box_list_t.decode(data)
            if len(str(self.left_boxes.utime)) < 16:
                self.left_boxes.utime = self.left_boxes.utime * 1000
            self.l_buf.append(self.left_boxes)
        if channel == self.right_channel:
            self.right_boxes = bounding_box_list_t.decode(data)
            if len(str(self.right_boxes.utime)) < 16:
                self.right_boxes.utime = self.right_boxes.utime * 1000
            self.r_buf.append(self.right_boxes)

        for l_b in self.l_buf:
            for r_b in self.r_buf:
                if np.abs(l_b.utime - r_b.utime) < 100000:
                    output = stereo_bounding_box_t()
                    output.left_boxes = l_b
                    output.right_boxes = r_b
                    self.lc.publish('BOX_STEREO', output.encode())
                    self.l_buf.remove(l_b)
                    self.r_buf.remove(r_b)
                    return

        # if self.left_boxes is not None and self.right_boxes is not None:
        #     output = stereo_bounding_box_t()
        #     output.left_boxes = self.left_boxes
        #     output.right_boxes = self.right_boxes
        #     # print(self.left_boxes.utime - self.right_boxes.utime)
        #     if np.abs(self.left_boxes.utime - self.right_boxes.utime) < 10000:
        #         time.sleep(0.05)
        #         self.lc.publish('BOX_STEREO_TEST', output.encode())
        #         self.left_boxes = None
        #         self.right_boxes = None


    def run(self):

        # LCM on local machine only
        # Image handles for ML
        self.lc.subscribe(self.left_channel, self.handle_box)
        self.lc.subscribe(self.right_channel, self.handle_box)

        try:
            while True:
                self.lc.handle_timeout(1000)

        except KeyboardInterrupt:
            pass

if __name__=="__main__":

    import sys

    bs = BoxSync()
    bs.run()


