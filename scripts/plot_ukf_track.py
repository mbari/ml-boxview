import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from numpy import ma
from scipy import interpolate, signal
import sys

import plot_config
import colorcet as cc

def parse_line(line):
    tokens = line.split(",")
    data = []
    try:
        for t in tokens:
            data.append(float(t))
    except:
        pass

    data = np.array(data)
    return data


if __name__=="__main__":

    input_file = sys.argv[1]

    # read file
    with open(input_file) as f:
        lines = f.readlines()

    data = []

    for ind, l in enumerate(lines):
        data.append(parse_line(l))

    data = np.array(data)

    time_vector = (data[:,0]-data[0,0])/1000000

    fig, ax1 = plt.subplots()

    color1 = 'tab:red'
    color2 = 'tab:blue'
    ax1.set_xlabel('time (s)')
    ax1.set_ylabel('Range (m)')
    # ax1.plot(time_vector, signal.medfilt(data[:, 1], 3), color)
    ml_inds = data[:,1] != 0
    ax1.plot(time_vector[ml_inds], data[ml_inds, 1], 'b*')
    ax1.plot(time_vector, data[:, 4], color1)
    ax1.legend(['ML Range (m)', 'UKF Range (m)'])
    ax1.set_ylim([0, 1.1])

    fig, ax2 = plt.subplots()
    color1 = 'tab:green'
    color2 = 'tab:blue'
    ax2.set_xlabel('time (s)')
    ax2.set_ylabel('Bearing (deg)')
    # ax1.plot(time_vector, signal.medfilt(data[:, 1], 3), color)
    ml_inds = data[:,1] != 0
    ax2.plot(time_vector[ml_inds], 180/np.pi*data[ml_inds, 2], 'b*')
    ax2.plot(time_vector, 180/np.pi*data[:, 5], color1)
    ax2.legend(['ML Bearing (deg)', 'UKF Bearing (deg)'])

    fig, ax3 = plt.subplots()

    color1 = 'tab:green'
    color2 = 'tab:blue'
    ax3.set_xlabel('time (s)')
    ax3.set_ylabel('Altitude (m)')
    # ax1.plot(time_vector, signal.medfilt(data[:, 1], 3), color)
    ml_inds = data[:, 1] != 0
    ax3.plot(time_vector[ml_inds], data[ml_inds, 3], 'b*')
    ax3.plot(time_vector, data[:, 6], color1)
    ax3.legend(['ML Altitude (m)', 'UKF Altitude (m)'])
    ax3.set_ylim([-0.5, 0.5])

    ax1.grid()
    ax2.grid()
    ax3.grid()

    fig, ax4 = plt.subplots()

    x = data[:, 4] * np.cos(data[:, 5])
    y = data[:, 4] * np.sin(data[:, 5])
    z = data[:, 6]

    color1 = 'tab:blue'
    ax4.set_xlabel('time (s)')
    ax4.set_ylabel('Position (m)',color=color1)
    ax4.plot(time_vector, data[:, 4], color1, linestyle='-')
    ax4.plot(time_vector, data[:, 6], color1, linestyle='--')
    ax4.tick_params(axis='y', labelcolor=color1)
    ax4.legend(['Range (m)', 'Altitude (m)'])
    ax4.set_ylim([0 , 1])

    if data.shape[1] > 12:
        color2 = 'tab:orange'
        ax5 = ax4.twinx()  # instantiate a second axes that shares the same x-axis
        ax5.set_ylabel('Bearing (deg)', color=color2)
        ax5.plot(time_vector, 180/np.pi*data[:, 5], color2)
        ax5.plot(time_vector, data[:, 12]-np.mean(data[:, 12]), 'tab:red', linestyle='--')
        ax5.tick_params(axis='y', labelcolor=color2)
        ax5.legend(['3D Bearing (deg)', 'Vehicle Yaw (deg)'])
        #ax5.set_ylim([-30 , 30])

    ax4.grid()
    plt.show()

