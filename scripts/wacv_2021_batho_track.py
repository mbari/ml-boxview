import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from numpy import ma
from scipy import interpolate, signal
from scipy.ndimage.filters import uniform_filter1d
import sys

import plot_config

def make_patch_spines_invisible(ax):
    ax.set_frame_on(True)
    ax.patch.set_visible(False)
    for sp in ax.spines.values():
        sp.set_visible(False)

def parse_line(line):
    tokens = line.split(",")
    data = []
    try:
        for t in tokens:
            data.append(float(t))
    except:
        pass

    data = np.array(data)
    return data


if __name__=="__main__":

    input_file = sys.argv[1]

    # read file
    with open(input_file) as f:
        lines = f.readlines()

    data = []

    for ind, l in enumerate(lines[1:]):
        data.append(parse_line(l))

    data = np.array(data)
    
    print(data.shape)



    fig, ax1 = plt.subplots()
    
    fig.subplots_adjust(right=0.75)

    depth = signal.medfilt(data[:, 10], 11)
    
    no_control = uniform_filter1d(data[:, 14] == 0, size=11)
    #no_control[no_control == 0.0] = -1
    #no_control_time_inds = time_vector[ml_okay == 0]
    #no_control_vals = .1*np.ones(len(no_control_time_inds))

    start_ind = 0
    end_ind = -1
    
    time_vector = (data[:, 0]-data[start_ind, 0])/1000000

    range_data = signal.medfilt(data[:, 7], 33)

    color = 'tab:gray'
    ax1.set_xlabel('time (s)')
    ax1.set_ylabel('Target Range (m)', color=color)
    # ax1.plot(time_vector, signal.medfilt(data[:, 1], 3), color)
    ax1.plot(time_vector[start_ind:end_ind], range_data[start_ind:end_ind] , color)
    ax1.plot(time_vector[start_ind:end_ind], 0.4*no_control[start_ind:end_ind], linestyle='', marker='.', color='tab:orange')
    ax1.tick_params(axis='y', labelcolor=color)
    #ax1.legend(['Target Range (m)', 'No Control Signal'], loc='upper left')
    ax1.set_ylim([.3, 1.3])


    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

    depth = signal.medfilt(data[:, 10], 33)

    color = 'tab:blue'
    ax2.set_ylabel('Vehicle Depth (m)', color=color)  # we already handled the x-label with ax1
    ax2.plot(time_vector[start_ind:end_ind],depth[start_ind:end_ind] , color)
    #ax2.plot(no_control_time_inds[0::10], no_control_vals[0::10], '.')

    ax2.tick_params(axis='y', labelcolor=color)
    
    ax2.invert_yaxis()

    ax3 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    
    # Offset the right spine of par2.  The ticks and label have already been
    # placed on the right by twinx above.
    ax3.spines["right"].set_position(("axes", 1.15))
    # Having been created by twinx, par2 has its frame off, so the line of its
    # detached spine is invisible.  First, activate the frame but make the patch
    # and spines invisible.
    make_patch_spines_invisible(ax3)
    # Second, show the right spine.
    ax3.spines["right"].set_visible(True)

    heading = signal.medfilt(data[:, 11], 33)

    color = 'tab:green'
    ax3.set_ylabel('Vehicle Heading (deg)', color=color)  # we already handled the x-label with ax1
    ax3.plot(time_vector[start_ind:end_ind], heading[start_ind:end_ind] , color)
    #ax2.plot(no_control_time_inds[0::10], no_control_vals[0::10], '.')
    ax3.tick_params(axis='y', labelcolor=color)
    ax3.set_ylim([0, 360])

    #fig, ax = plt.subplots()
    #ax.plot(time_vector, signal.medfilt(data[:, 1], 3))
    #ax.plot(time_vector, signal.medfilt(data[:, 2], 3)*np.pi/180)
    #ax.plot(time_vector, signal.medfilt(data[:, 3], 3))
    #ax.set_xlabel('Time (s)')
    #ax.set_ylabel('Meters / Radians)')

    ax1.grid()
    plt.show()
