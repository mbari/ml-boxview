# -*- coding: utf-8 -*-
"""
plot_tracks.py -- tools to plot derived box tracks over time
Author: pldr
Copyright 2020  Monterey Bay Aquarium Research Institute
Distributed under MIT license. See license.txt for more information.

"""

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from numpy import ma
from scipy import interpolate
import sys
from pykalman import UnscentedKalmanFilter


def parse_line(line):
    tokens = line.split(",")
    name = tokens[0]
    data = []
    for ind in [1, 2, 3, 5, 6, 7]:
        data.append(float(tokens[ind]))

    data = np.array(data)
    return name, data

# read file
with open(sys.argv[1]) as f:
    lines = f.readlines()

kcf_data = []
ml_data = []

for ind, l in enumerate(lines):
    name, data = parse_line(l)
    if name == 'ml-left':
        ml_data.append(data)
    else:
        kcf_data.append(data)

ml_data = np.array(ml_data)
kcf_data = np.array(kcf_data)


def F(state, noise):
    return [state[0] + state[1]/10, state[1] + state[2]/10, state[2]] + noise

def G(state, noise):
    return state[0] + noise

#ml_masked = ma.array(ml_data[:, 1])
#ml_masked[::2] = ma.masked

state_cov = np.array([[1,0,0],[0,1,0],[0,0,1]])
print(state_cov)
trans_cov = 1*state_cov
obs_cov = 1000*state_cov
print(obs_cov)

pixel_range = 1

ukf = UnscentedKalmanFilter(F, G,
                            initial_state_mean=[ml_data[0, 1]/pixel_range, .1, .1],
                            initial_state_covariance=state_cov,
                            transition_covariance=trans_cov,
                            observation_covariance=obs_cov
                            )
#(filtered_state_means, filtered_state_covariances) = ukf.filter(ml_data[:, 1])

(filtered_state_mean, filtered_state_covariance) = ukf.filter(ml_data[0, 1]/pixel_range)
filtered_state_mean = filtered_state_mean[0]
filtered_state_covariance = filtered_state_covariance[0]
ml_time = ((ml_data[:, 0]-ml_data[0, 0])/1000000)
ukf_estimate = []
ml_estimate = []

time_samples = np.arange(0, ml_time[-1], 0.1)
f = interpolate.interp1d(ml_time, ml_data[:, 1])
ml_data_samples = f(time_samples)

for i in range(0,len(time_samples)):
    if i % 10 == 0:
        (filtered_state_mean, filtered_state_covariance) = ukf.filter_update(filtered_state_mean, filtered_state_covariance, observation=ml_data_samples[i]/pixel_range)
        ml_estimate.append([time_samples[i], ml_data_samples[i]])
        ukf_estimate.append([time_samples[i], filtered_state_mean[0]*pixel_range])
    else:
        (filtered_state_mean, filtered_state_covariance) = ukf.filter_update(filtered_state_mean, filtered_state_covariance)
        ukf_estimate.append([time_samples[i], filtered_state_mean[0]*pixel_range])

ml_estimate = np.array(ml_estimate)
ukf_estimate = np.array(ukf_estimate)

print(ukf_estimate)

fig, ax = plt.subplots()
ax.plot(time_samples, ml_data_samples, '+')
ax.plot(ukf_estimate[:, 0], ukf_estimate[:, 1], '-*')
ax.plot(ml_estimate[:, 0], ml_estimate[:, 1], '*')

ax.grid()
plt.show()
