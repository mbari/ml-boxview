# -*- coding: utf-8 -*-
"""
test_bounding_box.py -- bounding box test code
Author: pldr
Copyright 2020  Monterey Bay Aquarium Research Institute
Distributed under MIT license. See license.txt for more information.

"""

from cvision import bounding_box_t
import time
from lcm import LCM

if __name__=="__main__":

    lc = LCM()

    try:
        while True:
            msg = bounding_box_t()
            msg.utime = int(time.time()*1000)
            msg.left = 100
            msg.top = 200
            msg.width = 50
            msg.height = 150
            msg.num_classes = 1
            msg.scores = [0.5]
            msg.class_name = "bathochordaeus"
            msg.channel = 0

            lc.publish('BOUNDING_BOX',msg.encode())

            msg.channel = 1

            lc.publish('BOUNDING_BOX', msg.encode())

            time.sleep(0.1)

    except KeyboardInterrupt:
        pass