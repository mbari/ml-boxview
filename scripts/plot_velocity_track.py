import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from numpy import ma
from scipy import interpolate, signal
import sys

import plot_config

def parse_line(line):
    tokens = line.split(",")
    data = []
    try:
        for t in tokens:
            data.append(float(t))
    except:
        pass

    data = np.array(data)
    return data


if __name__=="__main__":

    input_file = sys.argv[1]

    # read file
    with open(input_file) as f:
        lines = f.readlines()

    data = []

    for ind, l in enumerate(lines):
        data.append(parse_line(l))

    data = np.array(data)

    time_vector = (data[:,0]-data[0,0])/1000000

    fig, ax1 = plt.subplots()

    depth = signal.medfilt(data[:, 9], 11)
    d_depth = 100*signal.medfilt(depth[10:] - depth[:-10], 33)

    color = 'tab:red'
    ax1.set_xlabel('time (s)')
    ax1.set_ylabel('Vehicle Depth (m)', color=color)
    # ax1.plot(time_vector, signal.medfilt(data[:, 1], 3), color)
    ax1.plot(time_vector, depth, color)
    ax1.tick_params(axis='y', labelcolor=color)
    ax1.invert_yaxis()


    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

    color = 'tab:blue'
    ax2.set_ylabel('Vertical Velocity (cm/s)', color=color)  # we already handled the x-label with ax1
    ax2.plot(time_vector[10:], d_depth, 'tab:green')
    ax2.plot(time_vector, -signal.medfilt(100*data[:, 8], 33), color)

    ax2.tick_params(axis='y', labelcolor=color)
    ax2.set_ylim([-30, 30])

    ax2.legend(['Depth Derivative (cm/s)', 'Phase Correlation Estimate (cm/s)'])

    #fig, ax = plt.subplots()
    #ax.plot(time_vector, signal.medfilt(data[:, 1], 3))
    #ax.plot(time_vector, signal.medfilt(data[:, 2], 3)*np.pi/180)
    #ax.plot(time_vector, signal.medfilt(data[:, 3], 3))
    #ax.set_xlabel('Time (s)')
    #ax.set_ylabel('Meters / Radians)')

    ax1.grid()
    plt.show()

