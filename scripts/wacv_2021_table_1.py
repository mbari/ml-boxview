import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from numpy import ma
from scipy import interpolate, signal
from scipy.ndimage.filters import uniform_filter1d
import sys

def parse_line(line):
    tokens = line.split(",")
    data = []
    try:
        for t in tokens:
            data.append(float(t))
    except:
        pass

    data = np.array(data)
    return data


if __name__=="__main__":

    start = 0
    end = -2

    input_file = sys.argv[1]

    # read file
    with open(input_file) as f:
        lines = f.readlines()

    data = []

    for ind, l in enumerate(lines[1:]):
        d = parse_line(l)
        if len(d) == 15:
            data.append(d)

    data = np.array(data)
    
    print(data.shape)

    time_vector = (data[:,0]-data[0,0])/1000000

    control = uniform_filter1d(data[:,14], size=10)
    control[control > 0] = 1

    print('Autonomy: ' + str(100*np.mean(control[start:end])) + ' %')
    print('Range RMS Error ' + str(100*np.std(data[start:end,7])) + ' cm')
    print('Bearing RMS Error ' + str(np.std(data[start:end,8])) + ' deg')
    print('Altitude RMS Error ' + str(100*np.std(data[start:end,9])) + ' cm')
    