import sys
import numpy as np
from scipy import signal
import matplotlib
import matplotlib.pyplot as plt

file = sys.argv[1]

with open(file + '.dat', 'r') as f:
    dat_lines = f.readlines()

vel = np.zeros((len(dat_lines), 3))
dbar = np.zeros(len(dat_lines))

for i, line in enumerate(dat_lines):
    vals = line.split(' ')
    vals = [v for v in vals if v != '']
    vel[i, :] = [float(vals[2]), float(vals[3]), float(vals[4])]
    dbar[i] = float(vals[14])

with open(file + '.sen', 'r') as f:
    sen_lines = f.readlines()

hpr = np.zeros((len(sen_lines), 3))

for i, line in enumerate(sen_lines):
    vals = line.split(' ')
    vals = [v for v in vals if v != '']
    hpr[i, :] = [float(vals[10]), float(vals[11]), float(vals[12])]

# Average to one second
avg_vel = signal.decimate(vel, 32, axis=0)
avg_dbar = signal.decimate(dbar, 32, axis=0)

# convert from ENU to XYZ (from forum: https://support.nortekgroup.com/hc/en-us/articles/360029820971-How-is-a-Coordinate-transformation-done-)
T = np.array([[2.6755, -1.3066, -1.3701],
              [0.0388,  2.2908,  -2.3225],
              [0.3457,  0.3352,  0.3516]])

avg_vel_xyz = np.zeros(avg_vel.shape)

print(hpr.shape)
print(avg_vel.shape)

for i in range(0, hpr.shape[0]-1):

    statusbit0 = 1

    if statusbit0 == 1:
        T[1, :] = -T[1, :]
        T[2, :] = -T[2, :]


    # heading, pitch and roll are the angles output in the data in degrees
    hh = np.pi * (hpr[i, 0] - 90) / 180
    pp = np.pi * hpr[i, 1] / 180
    rr = np.pi * hpr[i, 2] / 180

    # Make heading matrix
    H = np.array([[np.cos(hh), np.sin(hh), 0.0],
                  [-np.sin(hh), np.cos(hh), 0.0],
                  [0.0, 0.0, 1.0]])

    # Make tilt matrix
    P = np.array([[np.cos(pp), -np.sin(pp) * np.sin(rr), -np.cos(rr) * np.sin(pp)],
                  [0, np.cos(rr), -np.sin(rr)],
                  [np.sin(pp), np.sin(rr) * np.cos(pp), np.cos(pp) * np.cos(rr)]])

    # Make resulting transformation matrix
    R = H @ P @ T

    avg_vel_xyz[i, :] = T @ np.linalg.inv(R) @ np.transpose(avg_vel[i, :])


avgs = 15

avg_vel = signal.decimate(avg_vel_xyz, avgs, axis=0)
avg_dbar = signal.decimate(avg_dbar, avgs, axis=0)

time_vector = np.linspace(0, avgs*avg_vel.shape[0], avg_vel.shape[0])

font = {'family' : 'normal',
        'weight' : 'normal',
        'size'   : 14}

matplotlib.rc('font', **font)

fig, ax1 = plt.subplots()
color = 'tab:red'
ax1.set_xlabel('time (s)')
ax1.set_ylabel('Vehicle Depth (dBar)', color=color)
# convert to positive forward
avg_vel[:, 2] = -1*avg_vel[:, 2]
ax1.plot(time_vector, avg_dbar, color)
ax1.tick_params(axis='y', labelcolor=color)
ax1.invert_yaxis()
ax1.grid(True)

ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

color = 'tab:blue'
ax2.set_ylabel('Velocity (m/s)', color=color)  # we already handled the x-label with ax1
ax2.plot(time_vector, avg_vel)
ax2.tick_params(axis='y', labelcolor=color)
ax2.set_ylim([-0.5, 0.5])
ax2.legend(['Down (X)', 'Right (Y)', 'Forward (Z)'])

plt.title('Vector Sensor Data (XYZ coordinates) July 20, 2020')

plt.show()