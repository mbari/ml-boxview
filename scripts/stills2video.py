import cv2
import glob
import time
import sys
import os

files = sorted(glob.glob(os.path.join(sys.argv[1], '*jpg')))

fourcc = cv2.VideoWriter_fourcc('h', '2', '6', '4')
video_out = cv2.VideoWriter('stills-video-' + str(int(time.time())) + '.mp4', fourcc, 10.0, (1032, 772))

for f in files:
    img = cv2.imread(f)
    video_out.write(img)
    print(f)

video_out.release()