import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from numpy import ma
from scipy import interpolate, signal
import sys

def parse_line(line):
    tokens = line.split(",")
    data = []
    for t in tokens:
        data.append(float(t))

    data = np.array(data)
    return data


if __name__=="__main__":

    input_file = sys.argv[1]

    # read file
    with open(input_file) as f:
        lines = f.readlines()

    data = []

    for ind, l in enumerate(lines):
        data.append(parse_line(l))

    data = np.array(data)

    time_vector = (data[:,0]-data[0,0])/1000000

    fig, ax1 = plt.subplots()

    color = 'tab:red'
    ax1.set_xlabel('time (s)')
    ax1.set_ylabel('Altitude (m)', color=color)
    # ax1.plot(time_vector, signal.medfilt(data[:, 1], 3), color)
    ax1.plot(time_vector, -signal.medfilt(data[:, 3], 3), color)
    ax1.tick_params(axis='y', labelcolor=color)

    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

    color = 'tab:blue'
    ax2.set_ylabel('Bearing (deg)', color=color)  # we already handled the x-label with ax1
    ax2.plot(time_vector, signal.medfilt(data[:, 2], 3), color)
    ax2.tick_params(axis='y', labelcolor=color)
    ax2.set_ylim([-30, 30])

    fig.tight_layout()  # otherwise the right y-label is slightly clipped

    #fig, ax = plt.subplots()
    #ax.plot(time_vector, signal.medfilt(data[:, 1], 3))
    #ax.plot(time_vector, signal.medfilt(data[:, 2], 3)*np.pi/180)
    #ax.plot(time_vector, signal.medfilt(data[:, 3], 3))
    #ax.set_xlabel('Time (s)')
    #ax.set_ylabel('Meters / Radians)')

    ax1.grid()
    plt.show()

