# README #

# Latest Version Notes
This app has evolved significantly 

# ml-boxview
A python app to display stereo images and bounding boxes for ML-Tracking

The app listens on LCM (https://lcm-proj.github.io/) for images on a left-channel and right-channel 
and also on the "BOUNDING_BOX" channel for bounding boxes from a running machine learning model.
It compares the timestamps on the boxes to the images and draws these boxes on the images 
before displaying the merged stereo pair with boxes in an opencv window. 

When running the app, a small GUI is opened to allow customization of the processing parameters.
When images are received and merged into stereo pairs, they are displayed in an opencv window scaled
to fit on a 1920 x 1080 screen.

The app allows the user to specify a target class to look for and combine into a stereo
pair of bounding boxes. The centroid of these boxes is published over LCM on the EXT_TARGET_3
channel.

Recently, several additional tracking and estimation tools have been added to allow for 
tracking the stereo solution using Kalman filters, estimating relative motion of the vehicle
wrt the water using either PIV or opencv's phaseCorrelate. These are very much a work in progress

## Setup (linux)

#### Install miniconda and activate environment

- Follow instructions here: https://docs.conda.io/en/latest/miniconda.html

#### Install additional python modules

```bash
$ pip install loguru
$ pip install nvgpu
$ pip install pyqt5
$ pip install pyqtgraph
$ pip install opencv-python
$ pip install opencv-contrib-python
$ pip install pykalman
$ pip install openpiv
```

#### Build and install lcm 

- Follow instructions here to build lcm: https://lcm-proj.github.io/build_instructions.html

#### Build python lcm module
```bash 
cd lcm/lcm-python
python setup.py install
```

#### Generate LCM types in root directory
```bash
lcm-gen -p --ppath . mwt-lcm-types/*.lcm
```

## Usage

#### Command line arguments

```--leftchannel [LEFT]``` :  The subscribe to ```LEFT``` channel for images from the left camera

```--rightchannel [RIGHT]``` : The subscribe to ```RIGHT``` channel for images from the right camera

```--ml_output_mod [mod]``` : When ```mod``` is > 0, save out ml annotations and images every ```mod``` seconds

```--vehicle_pos_output [PATH]``` : Save out position (RBA) and velocity information to a csv file at ```PATH```

#### Run on the command line
```bash
python boxview.py --leftchannel [left] --rightchannel [right] --ml_output_mod [interval]
```

Examples:  
```bash
# Run app with custom right and left channels
python boxview.py --leftchannel LEFT_CAM --rightchannel RIGHT_CAM

# Run app with default channels (MANTA_LEFT and MANTA_RIGHT) and save out ml annotations every 2 seconds
python boxview.py --ml_output_mod 2

# Run app with default channels (MANTA_LEFT and MANTA_RIGHT) and save out position data to test.csv
python boxview.py --vehicle_position_output test.csv
```

### Important Parameters
- Score Threshold [0, 100] -- Only boxes with max score above this will be processed
- Box Similarity [0, 100] -- Only boxes with similarity above this and matching class will be combined
- Target Class [list] -- The name of the target class for matching boxes. Edit config/labels.txt to add

### Stereo Solution
The app currently uses a very basic method to decide if a valid stereo pair of boxes has been
published. It first enumerates all of the left and right boxes that match the target class. Then it
checks all combinations of these boxes for similarity in width and height. The pair with the highest
similarity that is greater than a given threshold is then published on EXT_TARGET_3

#### Improvements to box similarity method:
Add in a stereo constraint on the box pair to filter out erroneous box pairs

#### GUI Example and notes

![alt text](resources/GUI_Example.png)

The image above shows what the GUI will look like when tracking an object with ML stereo solution (orange)
KCF tracker (pink), and Kalman tracker info (UKF text). The small UI on the bottom left has controls for updating
the important parameters mentioned above. 

NOTE: The other GUI tabs are not implemented yet. We need to add in and connect the controls for the tracking
parameters and possibly some display of additional text information that would otherwise clutter the images more
that they already are.
