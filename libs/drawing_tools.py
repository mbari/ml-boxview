# -*- coding: utf-8 -*-
"""
drawing_tools.py -- functions for drawing text in images using opencv
Author: pldr
Copyright 2020  Monterey Bay Aquarium Research Institute
Distributed under MIT license. See license.txt for more information.

"""

import cv2
import time
import datetime

def draw_position(stereo_image, pos, err, prefix='ML', y_offset=65, draw_color=(255, 255, 0)):

    output_img = stereo_image.boxed_img

    txt = prefix + ", Range = " + "{:04.1f}".format(pos[0] * 1000) + " mm, Bear = " + \
          "{:04.1f}".format(pos[1]) + " deg, Alt = " + \
          "{:04.1f}".format(pos[2] * 1000) + " mm, Error = " \
                                             "{:04.1f}".format(err) + " pixels"
    cv2.putText(output_img, txt, (40, y_offset), cv2.FONT_HERSHEY_COMPLEX, 0.85,
                draw_color)

def draw_ukf_position(stereo_image, pos_rba, draw_color=(255, 255, 0)):

    output_img = stereo_image.boxed_img

    txt = "UKF : Range = " + "{:04.1f}".format(pos_rba[0] * 1000) + " mm, Bear = " + \
          "{:04.1f}".format(pos_rba[1]) + " deg, Alt = " + \
          "{:04.1f}".format(pos_rba[2] * 1000) + " mm"
    cv2.putText(output_img, txt, (40, 90), cv2.FONT_HERSHEY_COMPLEX, 0.65,
                draw_color)

def draw_system_stats(stereo_image, insert_timestamps, last_frame_time, frame_rate_estimate,
                      cpu_usage, gpu_usage, proc_stereo_boxes, latest_depth, latest_yaw, overlay_color, depth_color):

        output_img = stereo_image.boxed_img

        left_utime = stereo_image.msg.left_utime
        if len(str(left_utime)) < 16:
            left_utime = left_utime * 1000


        timestamp = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(left_utime/1.0e6))

        # calc some stats and overlap on hstack of left and right images
        display_time = time.time()

        if stereo_image.left_key in insert_timestamps:
            insert_time = insert_timestamps.pop(stereo_image.left_key)
            latency = display_time - insert_time
        else:
            latency = 0.0
        frame_rate = 1 / (time.time() - last_frame_time)

        txt = timestamp + ", "
        if latest_depth is not None:
            txt += "ROV Depth: " + "{:04.1f}".format(latest_depth) + " m"
        if latest_yaw is not None:
            txt += ", ROV Yaw: " + "{:04.1f}".format(latest_yaw) + " deg"
        #cv2.putText(output_img, txt, (40, 40), cv2.FONT_HERSHEY_COMPLEX, 0.85,
        #            depth_color)

        frame_rate_estimate.append(frame_rate)
        txt += ", FPS: " + "{:04.1f}".format(frame_rate_estimate.average()) + " Hz"
        txt += ", CPU: " + "{:04.1f}".format(cpu_usage) + " %"
        txt += ", GPU: " + "{:04.1f}".format(gpu_usage) + " %"
        #txt += ", Latency: " + str(int(latency * 1000)) + " ms" + ", Boxes: " + str(proc_stereo_boxes)
        #cv2.putText(output_img, txt, (40, 70), cv2.FONT_HERSHEY_COMPLEX, 0.80,
        #            overlay_color)

        cv2.putText(output_img, txt, (40, 40), cv2.FONT_HERSHEY_COMPLEX, 0.80,
                    depth_color)
