# -*- coding: utf-8 -*-
"""
camera_calibration.py -- code to help with camera calibration from chessboard images
Author: pldr
Copyright 2020  Monterey Bay Aquarium Research Institute
Distributed under MIT license. See license.txt for more information.

"""

import numpy as np
import cv2
import glob
import sys
import os
import json
import time
import matplotlib.pyplot as plt

# numpy to json helper
class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)

# draws a grid on an image
def draw_grid(img, line_color=(0, 255, 0), thickness=1, type_=cv2.LINE_AA, pxstep=50):
    '''(ndarray, 3-tuple, int, int) -> void
    draw gridlines on img
    line_color:
        BGR representation of colour
    thickness:
        line thickness
    type:
        8, 4 or cv2.LINE_AA
    pxstep:
        grid line frequency in pixels
    '''
    x = pxstep
    y = pxstep
    while x < img.shape[1]:
        cv2.line(img, (x, 0), (x, img.shape[0]), color=line_color, lineType=type_, thickness=thickness)
        x += pxstep

    while y < img.shape[0]:
        cv2.line(img, (0, y), (img.shape[1], y), color=line_color, lineType=type_, thickness=thickness)
        y += pxstep

def read_and_scale(image_path, scale=2):
    img = cv2.imread(image_path)
    if scale != 1:
        img = cv2.resize(img, (2 * img.shape[1], 2 * img.shape[0]), cv2.INTER_CUBIC)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    return gray

def get_image_points(images, output_dir, board_shape, scale=2, criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)):

    imgpoints = []
    all_images = []
    # Detect the corners
    for fname in images:
        print(fname)

        # read image and scale to increase pixels/corner
        gray = read_and_scale(fname, scale=1)
        h, w = gray.shape[:2]
        gray_scaled = cv2.resize(gray, (gray.shape[1]*scale, gray.shape[0]*scale), cv2.INTER_CUBIC)

        # Find the chess board corners
        ret, corners = cv2.findChessboardCorners(gray_scaled, board_shape)

        # If found, add object points, image points (after refining them)
        if ret:

            # refine corners
            corners2 = cv2.cornerSubPix(gray_scaled, corners, (11, 11), (-1, -1), criteria)

            # rescale corners to true pixel scale
            corners2 = corners2 / scale

            imgpoints.append(corners2)
            all_images.append(gray)

            # Draw and display the corners
            img = cv2.cvtColor(gray, cv2.COLOR_GRAY2BGR)
            img = cv2.drawChessboardCorners(img, board_shape, corners2, ret)
            cv2.imshow('img', img)
            cv2.imwrite(os.path.join(output_dir, os.path.basename(fname[:-4]) + '_corners.jpg'), img)
            cv2.waitKey(100)
        else:
            print('Error finding corners on image: ' + os.path.basename(fname))

    return imgpoints, all_images

def compute_distortion_map(images, grid_prefix, output_dir, mtx, dist, R, P, image_size):

    mapx, mapy = cv2.initUndistortRectifyMap(mtx, dist, R, P, image_size, cv2.CV_32F)

    print(np.max(mapx))
    print(np.max(mapy))

    w, h = image_size

    # undistort the calibration images
    for fname in images:
        img = read_and_scale(fname, scale=1)
        dst = cv2.remap(img, mapx, mapy, cv2.INTER_LINEAR)
        cv2.imwrite(os.path.join(output_dir, os.path.basename(fname[:-4]) + '_remap.jpg'), dst)

    # invert the undistortion brute-force like (need some extra pixel room in the arrays)
    w_p = w
    h_p = h
    mapx_inv = np.zeros((h_p, w_p), np.float32)
    mapy_inv = np.zeros((h_p, w_p), np.float32)
    for i in range(0, mapx.shape[0]):
        for j in range(0, mapx.shape[1]):
            if int(mapy[i, j]) >= 0 and int(mapy[i, j]) < h and int(mapx[i, j]) >= 0 and int(mapx[i, j]) < w:
                mapx_inv[int(mapy[i, j]), int(mapx[i, j])] = j
                mapy_inv[int(mapy[i, j]), int(mapx[i, j])] = i

    # create a qualitative example of the distortion of a grid
    grid_img = 0 * cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    draw_grid(grid_img)
    dst = cv2.remap(grid_img, mapx_inv, mapy_inv, cv2.INTER_LINEAR)
    cv2.imwrite(os.path.join(output_dir, grid_prefix + '_distortion_grid.jpg'), dst[0:h - 1, 0:w - 1])

def stereo_calibration(image_path, board_shape, criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)):

    scale = 2

    # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
    objp = 30*np.zeros((board_shape[0] * board_shape[1], 3), np.float32)
    objp[:, :2] = 30*np.mgrid[0:board_shape[0], 0:board_shape[1]].T.reshape(-1, 2)

    # Arrays to store object points and image points from all the images.
    objpoints = []  # 3d point in real world space
    imgpoints_left = []  # 2d points in left image plane.
    imgpoints_right = [] # 2d points in the right image plane

    left_image_names = sorted(glob.glob(os.path.join(image_path, 'left', '*.tif')))
    right_image_names = sorted(glob.glob(os.path.join(image_path, 'right', '*.tif')))

    # load one image to get the image size
    img = cv2.imread(left_image_names[0])
    image_size = (img.shape[1], img.shape[0])

    if len(left_image_names) != len(right_image_names):
        print('Error, mismatch in number of left and right images, aborting')
        return

    # setup the object points
    for p in range(0, len(left_image_names)):
        objpoints.append(objp)

    # create a new calibration output dir
    output_dir = os.path.join(image_path, 'stereo-cal')
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    imgpoints_left, left_images = get_image_points(left_image_names, output_dir, board_shape, scale, criteria)
    imgpoints_right, right_images = get_image_points(right_image_names, output_dir, board_shape, scale, criteria)

    if len(imgpoints_left) != len(imgpoints_right):
        print('Error: mismatch in the number of detected corners, aborting.')
        return

    term_crit = (cv2.TERM_CRITERIA_MAX_ITER + cv2.TERM_CRITERIA_EPS, 100, 1e-5)

    flags = 0
    #flags |= cv2.CALIB_FIX_INTRINSIC
    #flags |= cv2.CALIB_USE_INTRINSIC_GUESS
    #flags |= cv2.CALIB_FIX_PRINCIPAL_POINT
    #flags |= cv2.CALIB_FIX_FOCAL_LENGTH
    #flags |= cv2.CALIB_FIX_ASPECT_RATIO
    #flags |= cv2.CALIB_ZERO_TANGENT_DIST
    #flags |= cv2.CALIB_SAME_FOCAL_LENGTH
    #flags |= cv2.CALIB_RATIONAL_MODEL
    # flags |= cv2.CALIB_FIX_K3
    # flags |= cv2.CALIB_FIX_K4
    # flags |= cv2.CALIB_FIX_K5

    left_intrinsics = None
    left_distortion = None
    right_intrinsics = None
    right_distortion = None
    R = None
    T = None
    E = None
    F = None

    # run opencv's stereo calibration
    (error, left_intrinsics, left_distortion,
            right_intrinsics, right_distortion,
            R, T, E, F) = cv2.stereoCalibrate(objpoints, imgpoints_left, imgpoints_right,
                    left_intrinsics, left_distortion, right_intrinsics, right_distortion, image_size, R, T, E, F,
                    criteria=term_crit, flags=flags)

    w, h = image_size
    #left_intrinsics, roi = cv2.getOptimalNewCameraMatrix(left_intrinsics, left_distortion, (w, h), 1, (w, h))
    #right_intrinsics, roi = cv2.getOptimalNewCameraMatrix(right_intrinsics, right_distortion, (w, h), 1, (w, h))

    print('Error: ' + str(error))

    #

    # Get the stereo rectification matricies:
    R1 = None
    R2 = None
    P1 = None
    P2 = None
    Q = None


    (R1, R2, P1, P2, Q, val_left, val_right) = cv2.stereoRectify(left_intrinsics, left_distortion, right_intrinsics, right_distortion, image_size,
                      R, T, R1, R2, P1, P2, Q=None, alpha=-1, flags=cv2.CALIB_ZERO_DISPARITY)

    if error < 0.5:
        print('Calibration okay.')
        output = {}
        output['left_intrinsics'] = left_intrinsics
        output['left_distortion'] = left_distortion
        output['right_intrinsics'] = right_intrinsics
        output['right_distortion'] = right_distortion
        output['R'] = R
        output['T'] = T
        output['E'] = E
        output['F'] = F
        output['R1'] = R1
        output['R2'] = R2
        output['P1'] = P1
        output['P2'] = P2
        output['Q'] = Q
        output['error'] = error
        with open(os.path.join(output_dir, 'stereo_calibration.json'), 'w') as outfile:
            json.dump(output, outfile, sort_keys=True, indent=4, cls=NumpyEncoder)

    print(P1)
    print(P2)

    # rectify images
    map_left_x, map_left_y = cv2.initUndistortRectifyMap(left_intrinsics, left_distortion, R1, P1, image_size, cv2.CV_32F)
    map_right_x, map_right_y = cv2.initUndistortRectifyMap(right_intrinsics, right_distortion, R2, P2, image_size, cv2.CV_32F)
    for i in range(0, len(left_images)):
        img_rect_left = cv2.remap(left_images[i], map_left_x, map_left_y, cv2.INTER_LINEAR)
        img_rect_right = cv2.remap(right_images[i], map_right_x, map_right_y, cv2.INTER_LINEAR)
        img_tmp = np.zeros((image_size[1], 2*image_size[0]), 'uint8')
        img_tmp[:, :image_size[0]] = img_rect_left
        img_tmp[:, image_size[0]:] = img_rect_right
        img_tmp = cv2.cvtColor(img_tmp, cv2.COLOR_GRAY2BGR)
        # plot scan lines
        left_points = np.transpose(np.squeeze(imgpoints_left[i]))
        right_points = np.transpose(np.squeeze(imgpoints_right[i]))
        rect_img_points_left = None
        rect_img_points_right = None
        rect_img_points_left = cv2.undistortPoints(left_points, left_intrinsics, left_distortion, rect_img_points_left, R1, P1)
        rect_img_points_left = np.squeeze(rect_img_points_left).astype('int32')
        rect_img_points_right = cv2.undistortPoints(right_points, right_intrinsics, right_distortion, rect_img_points_right, R2, P2)
        rect_img_points_right = np.squeeze(rect_img_points_right).astype('int32')
        for j in range(0, len(imgpoints_left[i]), 6):
            cv2.line(img_tmp, (rect_img_points_left[j][0], rect_img_points_left[j][1]), (image_size[0] + rect_img_points_right[j][0], rect_img_points_right[j][1]), (0, 255, 0))

        cv2.imshow('Rectified', img_tmp)
        cv2.imwrite(os.path.join(output_dir, os.path.basename(left_image_names[i])[:-4] + '_rectified_pair.tif'), img_tmp)
        cv2.waitKey()

    # compute distortion maps for each camera
    compute_distortion_map(left_image_names, 'left', output_dir, left_intrinsics, left_distortion, None, left_intrinsics, image_size)
    compute_distortion_map(right_image_names, 'right', output_dir, right_intrinsics, right_distortion, None, right_intrinsics, image_size)


    # triangulate the calibration points
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    for i in range(0, len(imgpoints_left)):
        left_points = np.transpose(np.squeeze(imgpoints_left[i]))
        right_points = np.transpose(np.squeeze(imgpoints_right[i]))
        rect_img_points_left = None
        rect_img_points_right = None
        points_4d = None
        left_points = np.transpose(np.squeeze(imgpoints_left[i]))
        right_points = np.transpose(np.squeeze(imgpoints_right[i]))

        rect_img_points_left = cv2.undistortPoints(left_points, left_intrinsics, left_distortion, rect_img_points_left, R1, P1)
        rect_img_points_left = np.squeeze(rect_img_points_left).astype('float').T

        rect_img_points_right = cv2.undistortPoints(right_points, right_intrinsics, right_distortion, rect_img_points_right, R2, P2)
        rect_img_points_right = np.squeeze(rect_img_points_right).astype('float').T

        points_4d = cv2.triangulatePoints(P1, P2, rect_img_points_left, rect_img_points_right, points_4d)
        points_3d = points_4d[0:3, :] / np.tile(points_4d[3, :], 3).reshape(3, -1)
        print(points_4d)
        ax.scatter(points_3d[0, :], points_3d[1, :], points_3d[2, :], marker='o')
        plt.draw()
        plt.waitforbuttonpress()
    plt.show()

# run calibration on a given image path
def intrinsic_calibration(image_path, board_shape, criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)):

    scale = 2

    # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
    objp = np.zeros((board_shape[0] * board_shape[1], 3), np.float32)
    objp[:, :2] = np.mgrid[0:board_shape[0], 0:board_shape[1]].T.reshape(-1, 2)

    # Arrays to store object points and image points from all the images.
    objpoints = []  # 3d point in real world space
    imgpoints = []  # 2d points in image plane.

    images = sorted(glob.glob(os.path.join(image_path, '*.tif')))

    # create a new calibration output dir
    output_dir = os.path.join(image_path, 'cal')
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # just defaults, will be filled in when loading images
    h = 772
    w = 1032

    # Detect the corners
    for fname in images:
        print(fname)

        # read image and scale to increase pixels/corner
        gray = read_and_scale(fname, scale=1)
        h, w = gray.shape[:2]
        gray_scaled = cv2.resize(gray, (gray.shape[1]*scale, gray.shape[0]*scale), cv2.INTER_CUBIC)

        # Find the chess board corners
        ret, corners = cv2.findChessboardCorners(gray_scaled, board_shape)

        # If found, add object points, image points (after refining them)
        if ret:
            objpoints.append(objp)

            # refine corners
            corners2 = cv2.cornerSubPix(gray_scaled, corners, (11, 11), (-1, -1), criteria)

            # rescale corners to true pixel scale
            corners2 = corners2 / scale

            imgpoints.append(corners2)

            # Draw and display the corners
            img = cv2.cvtColor(gray, cv2.COLOR_GRAY2BGR)
            img = cv2.drawChessboardCorners(img, board_shape, corners2, ret)
            cv2.imshow('img', img)
            cv2.imwrite(os.path.join(output_dir, os.path.basename(fname[:-4]) + '_corners.jpg'), img)
            cv2.waitKey(100)

    # Do the calibration
    ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)

    # show the RMS re-projection error
    print("RMS error: ", ret)

    # Save out the results
    if ret < 0.5:
        print('Calibration okay.')
        output = {}
        output['mtx'] = mtx
        output['dist'] = dist
        output['rvecs'] = rvecs
        output['tvecs'] = tvecs
        output['image_width'] = img.shape[1]
        output['image_height'] = img.shape[0]
        output['rms_error'] = ret
        with open(os.path.join(output_dir, 'calibration.json'), 'w') as outfile:
            json.dump(output, outfile, sort_keys=True, indent=4, cls=NumpyEncoder)

    # revise the calibration matrix
    newcameramtx, roi = cv2.getOptimalNewCameraMatrix(mtx, dist, (w, h), 1, (w, h))

    # build the distortion correction mapping
    mapx, mapy = cv2.initUndistortRectifyMap(mtx, dist, None, newcameramtx, (w, h), 5)

    print(np.max(mapx))
    print(np.max(mapy))

    # undistort the calibration images
    for fname in images:
        img = read_and_scale(fname, scale=1)
        dst = cv2.remap(img, mapx, mapy, cv2.INTER_LINEAR)
        cv2.imwrite(os.path.join(output_dir, os.path.basename(fname[:-4]) + '_remap.jpg'), dst)

    # invert the undistortion brute-force like (need some extra pixel room in the arrays)
    w_p = int(np.max(mapx)) + 1
    h_p = int(np.max(mapy)) + 1
    mapx_inv = np.zeros((h_p, w_p), np.float32)
    mapy_inv = np.zeros((h_p, w_p), np.float32)
    for i in range(0, mapx.shape[0]):
        for j in range(0, mapx.shape[1]):
            mapx_inv[int(mapy[i, j]), int(mapx[i, j])] = j
            mapy_inv[int(mapy[i, j]), int(mapx[i, j])] = i

    # create a qualitative example of the distortion of a grid
    grid_img = 0 * cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    draw_grid(grid_img)
    dst = cv2.remap(grid_img, mapx_inv, mapy_inv, cv2.INTER_LINEAR)
    cv2.imwrite(os.path.join(output_dir, 'distortion_grid.jpg'), dst[0:h - 1, 0:w - 1])



# main function
if __name__=="__main__":

    if len(sys.argv) < 2:
        print('Please input path to images as first argument')
        exit()

    # Calibrate left and right separatly
    #intrinsic_calibration(os.path.join(sys.argv[1], 'left'), (9, 6))
    #intrinsic_calibration(os.path.join(sys.argv[1], 'right'), (9, 6))

    # stereo calibration
    stereo_calibration(sys.argv[1], (9, 6))
        


