# -*- coding: utf-8 -*-
"""
camera_control.py -- code to configure Vimba cameras over LCM
Author: pldr
Copyright 2020  Monterey Bay Aquarium Research Institute
Distributed under MIT license. See license.txt for more information.

"""

import numpy as np
import serial
import os
import lcm
import cv2
import time
import socket
from loguru import logger
from pyqtgraph.Qt import QtCore, QtGui
from mwt.camera_cfg_t import camera_cfg_t
from mwt.dspl_cfg_t import dspl_cfg_t

class MantaControl(QtCore.QThread):

    cam_cfg = QtCore.Signal(object)

    def __init__(self):

        QtCore.QThread.__init__(self)

        self.stop_thread = False

        self.new_config = False
        self.config_timer = QtCore.QTimer()
        self.config_timer.timeout.connect(self.sendCameraConfig)
        self.config_timer.start(250)

        # Get the latest camera config values
        self.lc = lcm.LCM("udpm://239.255.76.67:7667?ttl=0")
        subscription = self.lc.subscribe('CAM-CFG-50-0503455192', self.config_cameras)
        self.lc.handle_timeout(2000)

        # image recording
        self.stereo_image = None
        self.image_counter = 0
        self.dir_base = 'data'
        self.make_stereo_dirs()

        # just defaults, these will be overwritten
        self.exposure = 5000
        self.gain = 0
        self.framerate = 10

    def make_stereo_dirs(self):
        self.left_dir = os.path.join(self.dir_base, 'left')
        self.right_dir = os.path.join(self.dir_base, 'right')

        if not os.path.exists(self.left_dir):
            os.makedirs(self.left_dir)
        if not os.path.exists(self.right_dir):
            os.makedirs(self.right_dir)

    def config_cameras(self, channel, data):

        if not self.new_config:

            #logger.info('New camera settings from LCM')

            msg = camera_cfg_t.decode(data)

            self.gain = msg.gain
            self.exposure = msg.exposure
            self.framerate = msg.framerate
            self.cam_cfg.emit(msg)

    def get_image_path(self, prefix):
        if prefix == 'left':
            dir_path = self.left_dir
        elif prefix == 'right':
            dir_path = self.right_dir
        image_path = os.path.join(dir_path, prefix + '-'
                                  + str(int(time.time())) + '-'
                                  + "{:03d}".format(self.image_counter) + '.tif')
        return image_path

    def getDataDirectory(self):
        new_dir = QtGui.QFileDialog.getExistingDirectory(self,
            'Select Data Directory',
            self.dir_base,
            QtGui.QFileDialog.ShowDirsOnly)
        if new_dir is not None and len(new_dir) > 0:
            self.dir_base = new_dir
            self.make_stereo_dirs()
            self.image_counter = 0
            self.update_status_text()

    def grabImagePair(self):
        if self.stereo_image is not None:
            left_image = self.stereo_image[:, 0:int(self.stereo_image.shape[1]/2)]
            right_image = self.stereo_image[:, int(self.stereo_image.shape[1] / 2):]
            cv2.imwrite(self.get_image_path('left'), left_image)
            cv2.imwrite(self.get_image_path('right'), right_image)
            self.image_counter += 1
            self.update_status_text()

    def sendCameraConfig(self):

        if self.new_config:
            cfg = camera_cfg_t()
            cfg.exit_app = 0
            cfg.exposure = self.exposure
            cfg.gain = self.gain
            cfg.framerate = self.framerate

            self.lc.publish('CAM_CFG', cfg.encode())
            self.new_config = False

    def updateCameraConfig(self, exp, gain, fps):

        self.exposure = exp
        self.gain = gain
        self.framerate = fps
        self.new_config = True

    def stop(self):
        self.stop_thread = True

    def run(self):

        subscription = self.lc.subscribe('CAM-CFG-50-0503455192', self.config_cameras)

        while self.isRunning and not self.stop_thread:
            self.lc.handle_timeout(2000)

        