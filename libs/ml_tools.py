# -*- coding: utf-8 -*-
"""
ml_tools.py -- classes for abstracting ML data and image data
Author: pldr
Copyright 2020  Monterey Bay Aquarium Research Institute
Distributed under MIT license. See license.txt for more information.

"""

import time
from libs.image_tools import unpack_image, get_key


class MLImage:

    def __init__(self, msg, index):
        self.boxes = []
        self.object_tracker_box = None
        self.boxed_img = None
        self.first_box_timestamp = 0
        self.msg = msg
        self.img = unpack_image(msg)
        self.boxed_img = self.img.copy()
        self.insert_timestamp = time.time()
        self.key = get_key(msg.utime)
        self.channel = index

class StereoMLImage:

    def __init__(self, msg):
        self.left_boxes = []
        self.right_boxes = []
        self.left_object_tracker_box = None
        self.right_object_tracker_box = None
        self.boxed_img = None
        self.msg = msg
        self.img = unpack_image(msg)
        self.boxed_img = self.img.copy()
        self.insert_timestamp = time.time()
        self.left_key = get_key(msg.left_utime)
        self.right_key = get_key(msg.right_utime)

    def split_images(self):
        slice_width = int(self.msg.width / 2)
        left = SimpleMLImage(self.img[:, 0:slice_width], self.msg.left_utime, 0)
        left.boxes = self.left_boxes
        right = SimpleMLImage(self.img[:, slice_width:], self.msg.right_utime, 1)
        right.boxes = self.right_boxes

        return left, right


class SimpleMLImage:

    def __init__(self, img, utime, index):
        self.boxes = []
        self.utime = utime
        self.img = img
        self.channel = index
        self.key = get_key(self.utime)


class MLBox:

    def __init__(self, left, top, w, h, channel=0, utime=0, scores=0, class_name='none'):
        self.left = left
        self.top = top
        self.width = w
        self.height = h
        self.channel = channel
        self.utime = utime
        self.scores = scores
        self.class_name = class_name