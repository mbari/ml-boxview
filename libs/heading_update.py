def calc_heading_update(current_head,new_heading):

    if new_heading > 360:
        new_heading = new_heading - 360
    elif new_heading < 0:
        new_heading = 360 + new_heading

    if new_heading < current_head:
        print('yawSearchMoveAbsNeg(' + str(abs(new_heading)) + ')')
    else:
        print('yawSearchMoveAbsPos(' + str(abs(new_heading)) + ')')