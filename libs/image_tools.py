# -*- coding: utf-8 -*-
"""
image_tools.py -- helper methods for managing images from lcm and image timestamps
Author: pldr
Copyright 2020  Monterey Bay Aquarium Research Institute
Distributed under MIT license. See license.txt for more information.

"""

import cv2
import numpy as np
from mwt.image_t import image_t

def unpack_image(msg):
    pix = np.frombuffer(msg.data, dtype='uint8')

    if msg.pixelformat != image_t.PIXEL_FORMAT_GRAY:
        # color image, unpack and resize
        img = pix.reshape(msg.height, msg.width, 3)
    else:
        # img = np.tile(pix.reshape(msg.height, msg.width, 1), (1, 1, 3))
        img = cv2.cvtColor(pix.reshape(msg.height, msg.width, 1), cv2.COLOR_GRAY2BGR)

    return img

def unpack_stereo_image(msg):
    pix = np.frombuffer(msg.data, dtype='uint8')

    if msg.pixelformat != image_t.PIXEL_FORMAT_GRAY:
        # color image, unpack and resize
        img = pix.reshape(msg.height, msg.width, 3)
    else:
        # img = np.tile(pix.reshape(msg.height, msg.width, 1), (1, 1, 3))
        img = cv2.cvtColor(pix.reshape(msg.height, msg.width, 1), cv2.COLOR_GRAY2BGR)

    return img


def get_key(utime):
    return str(int(utime / 100000))