import os
import cv2
import time
from collections import deque
from pyqtgraph.Qt import QtCore

class BasicEntry:
    def __init__(self, img, utime, depth, yaw, target_name, rba=None, color1=(255, 255, 255), color2=(255, 255, 255)):
        self.img = img
        self.utime = utime
        self.depth = depth
        self.yaw = yaw
        self.target_name = target_name
        self.rba = rba
        self.color1 = color1
        self.color2 = color2

class StillsExporter(QtCore.QThread):

    def __init__(self, export_path='Stills'):
        QtCore.QThread.__init__(self)
        self.buffer = deque()
        self.export_path = export_path
        self.stop_thread = False

    def __del__(self):
        self.wait()

    def add_entry(self, entry=None):
        if entry is not None:
            self.buffer.append(entry)

    def proc_entry(self):

        if len(self.buffer) > 0:

            entry = self.buffer.popleft()

            if len(str(entry.utime)) < 16:
                entry.utime = entry.utime * 1000

            timestamp = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(entry.utime / 1.0e6))
            txt = timestamp + ", "
            if entry.depth is not None:
                txt += "ROV Depth: " + "{:04.1f}".format(entry.depth) + " m"
            if entry.yaw is not None:
                txt += ", ROV Yaw: " + "{:04.1f}".format(entry.yaw) + " deg"
            cv2.putText(entry.img, txt, (40, 40), cv2.FONT_HERSHEY_COMPLEX, 0.85,
                        entry.color1)
            txt = "Target Class: " + entry.target_name
            if entry.rba is not None:
                txt += ", Range = " + "{:04.1f}".format(entry.rba[0] * 1000) + " mm"
            cv2.putText(entry.img, txt, (40, 70), cv2.FONT_HERSHEY_COMPLEX, 0.85,
                       entry.color2)

            filename_timestamp = time.strftime("%Y%m%dT%H%M%SZ", time.localtime(entry.utime / 1.0e6))

            cv2.imwrite(os.path.join(self.export_path,'ml-boxview-stills-' + filename_timestamp + '.jpg'), entry.img)
            logger.info('exporting ' + 'ml-boxview-stills-' + filename_timestamp + '.jpg')

    def stop(self):
        self.stop_thread = True

    def run(self):

        while self.isRunning and not self.stop_thread:
            self.proc_entry()
            time.sleep(0.25)