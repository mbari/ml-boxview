# -*- coding: utf-8 -*-
"""
input_arguments.py -- wrapper around argparse
Author: pldr
Copyright 2020  Monterey Bay Aquarium Research Institute
Distributed under MIT license. See license.txt for more information.

"""

import argparse

def parse_args():
    parser = argparse.ArgumentParser(description="Uses LCM messages to make a video.")
    # parser.add_argument(
    #    'video_path',
    #    type=str,
    #    help="Path to output video.",
    # )
    parser.add_argument(
        '--leftchannel',
        type=str,
        default="MANTA_LEFT",
        help="optional name of left camera channel"
    )
    parser.add_argument(
        '--rightchannel',
        type=str,
        default="MANTA_RIGHT",
        help="Optional name of right camera channel"
    )
    parser.add_argument(
        '--stereo_channel',
        type=str,
        default="MWT_STEREO_IMAGE",
        help="Optional name of stereo camera channel"
    )
    parser.add_argument(
        '--output_channel',
        type=str,
        default="MWT_TARGET",
        help="Optional name of target output"
    )
    parser.add_argument(
        '--stereo_box_channel',
        type=str,
        default="BOX_STEREO",
        help="Optional name of stereo camera channel"
    )
    parser.add_argument(
        '--tracker_box_channel',
        type=str,
        default="BOX_STEREO_TRACK",
        help="Optional, name of the tracker output channel"
    )
    parser.add_argument(
        '--ml_output_mod',
        type=int,
        default=0,
        help="Optional, save out ml annotations ever mod seconds"
    )
    parser.add_argument(
        '--vehicle_pos_output',
        type=str,
        default='',
        help="Optional, save out position data to file"
    )
    parser.add_argument(
        '--video_output',
        type=str,
        default='',
        help="Optional, output video from stereo frames"
    )
    parser.add_argument(
        '--stills_output',
        type=str,
        default='',
        help="Optional, output stills from left camera boxed image"
    )
    parser.add_argument(
        '--stereo_cal',
        type=str,
        default='config/june2020_wide.xml',
        help="Stereo calibration file to use, default is config/manta_binned_cal_steve.xml"
    )
    parser.add_argument(
        '--vehicle',
        type=str,
        default="None",
        help="name of vehicle: DR => Doc Ricketts, MINIROV => MiniROV"
    )
    return parser.parse_args()
