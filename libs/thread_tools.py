# -*- coding: utf-8 -*-
"""
thread_tools.py -- Code for threading image handling, very TBD, not currently used in boxview
Author: pldr
Copyright 2020  Monterey Bay Aquarium Research Institute
Distributed under MIT license. See license.txt for more information.

"""

import cv2
import lcm
import time
from loguru import logger
from mwt.image_t import image_t
from mwt.bounding_box_t import bounding_box_t
from pyqtgraph.Qt import QtCore



class BoxHandler(QtCore.QThread):

    def __init__(self, channel, ml_img, max_latency=0.4):
        QtCore.QThread.__init__(self)
        self.channel = channel
        self.index = int('right' in self.channel.lower())
        self.ml_img = ml_img
        self.max_latency = max_latency
        self.box_color = (3, 177, 252)
        self.ext_target_3_color = (132, 3, 252)
        self.kcf_color = (207, 3, 252)
        self.text_color = (3, 252, 53)
        self.overlay_color = (200, 252, 53)

    def __del__(self):
        self.wait()

    def get_points(self, box):

        pt0 = (round(box.left), round(box.top))
        pt1 = (round(box.left + box.width), round(box.top + box.height))

        return pt0, pt1

    def handle_box(self, channel, data):

        box = bounding_box_t.decode(data)

        # Patch for ms timestamp bug in march 4 data
        if len(str(box.utime)) < 16:
            box.utime = box.utime * 1000

        pt0, pt1 = self.get_points(box)

        cv2.rectangle(self.ml_img.boxed_img, tuple(pt0), tuple(pt1), self.box_color, 2)
        cv2.putText(self.ml_img.boxed_img, box.class_name, (pt0[0], pt1[1] + 20), cv2.FONT_HERSHEY_COMPLEX, 0.75,
                    self.text_color)

        self.ml_img.boxes.append(box)

    def run(self):

        # collect boxes up to latency
        lc = lcm.LCM("udpm://239.255.76.67:7667?ttl=0")
        lc.subscribe('BOUNDING_BOX', self.handle_box)
        timer = time.time()
        while time.time() - timer < self.max_latency:
            lc.handle()
        print(self.channel + ' -- ' + str(len(self.ml_img.boxes)) + ' boxes in ' + str(int((time.time() - self.ml_img.insert_timestamp)*1000)) + ' ms -- Done.')

class MLHandler(QtCore.QThread):

    def __init__(self, channel):
        QtCore.QThread.__init__(self)
        logger.info("Initializing ML handler...")
        self.channel = channel
        self.index = int('right' in self.channel.lower())
        self.boxed_img = None
        self.image_timestamp = 0
        self.handlers = []

    def __del__(self):
        self.wait()

    def handle_img(self, channel, data):

        # decode the message
        msg = image_t.decode(data)
        # Patch for ms timestamp bug on March 4 data
        if len(str(msg.utime)) < 16:
            msg.utime = msg.utime * 1000

        ml_img = MLImage(msg)
        boxHandler = BoxHandler(channel, ml_img)
        boxHandler.start()
        self.handlers.append(boxHandler)



    def run(self):

        # LCM on local machine only
        lc = lcm.LCM("udpm://239.255.76.67:7667?ttl=0")
        logger.info("Creating subscriptions...")
        # Image handles for ML
        lc.subscribe(self.channel, self.handle_img)

        while self.isRunning:
            lc.handle()
            for ind, h in enumerate(self.handlers):
                if not h.isRunning:
                    self.handlers.pop(h)