# -*- coding: utf-8 -*-
"""
stereo_calc.py -- Python implementation of Mike Risi and Steve Rock's Stereo solution C++ code
Author: pldr
Copyright 2020  Monterey Bay Aquarium Research Institute
Distributed under MIT license. See license.txt for more information.

"""

import cv2
import json
import numpy as np

def get_rot_mat(rpy):

    phi, theta, psi = rpy

    t_phi = np.arrray([
        [1.0, 0.0, 0.0],
        [0.0, np.cos(phi), np.sin(phi)],
        [0.0, -np.sin(phi), np.cos(phi)]
    ])

    t_theta = np.arrray([
        [np.cos(theta), 0.0, -np.sin(theta)],
        [0.0, 1.0, 0.0],
        [np.sin(theta), 0.0, np.cos(theta)]
    ])

    t_psi = np.array([
        [np.cos(psi), np.sin(psi), 0.0],
        [-np.sin(psi), np.cos(psi), 0.0],
        [0.0, 0.0, 1.0]
    ])

    return t_phi @ t_theta @ t_psi


def calc_wf_location(rpy, cam_pos, target_loc):

    cl_position = cam_pos
    position_vf = target_loc

    rot_vf_wf = get_rot_mat(rpy)
    rot_wf_vf = np.transpose(rot_vf_wf)

    position_wf = rot_wf_vf @ (position_vf + cl_position)

    return position_wf

def calc_vf_location(rpy, cam_pos, target_loc):

    cl_position = cam_pos
    position_wf = target_loc

    rot_vf_wf = get_rot_mat(rpy)

    position_vf = rot_vf_wf @ position_wf + cl_position

    return position_vf

def vehicle_location_to_rba(pos_vf):
    range = np.sqrt(pos_vf[0] ** 2 + pos_vf[1] ** 2)

    if pos_vf[0] > 0.0000001:
        bearing = np.arctan2(pos_vf[1], pos_vf[0])
    else:
        bearing = 0.0

    bearing = bearing * 180.0 / np.pi

    altitude = pos_vf[2]

    return range, bearing, altitude

def rba_to_vehicle_location(pos_rba):

    pos_vf = pos_rba
    pos_vf[0] = pos_rba[0] * np.cos(pos_rba[1])
    pos_vf[1] = pos_rba[0] * np.sin(pos_rba[1])

    return pos_vf

def get_pos_in_vf(pos):

    pos[1] = np.pi / 180.0 * pos[1] # bearing from deg to radians

    pos_vf = rba_to_vehicle_location(pos)

    pos_vf = pos_vf * 1000 # convert back to mm

    return pos_vf


def get_pos_in_rba(pos):
    rot_vf_c1 = np.array([
        [0.0, 0.0, 1.0],
        [1.0, 0.0, 0.0],
        [0.0, 1.0, 0.0]]
    )

    # rotate it into vehicle coordinates
    position_vf = rot_vf_c1 @ pos

    # position of target in VF converted to meters
    pos_vf_meters = position_vf / 1000.0

    target_rba = list(vehicle_location_to_rba(pos_vf_meters))

    return target_rba

class OpenCVStereoCalc:

    def __init__(self, cal_file='../config/wide_geo.json'):

        self.cal = {}

        # load the cal file
        with open(cal_file, 'r') as f:
            self.cal = json.load(f)

        # format the matricies
        self.R1 = np.array(self.cal['R1'])
        self.R2 = np.array(self.cal['R2'])
        self.P1 = np.array(self.cal['P1'])
        self.P2 = np.array(self.cal['P2'])
        self.left_intrinsics = np.array(self.cal['left_intrinsics'])
        self.left_distortion = np.array(self.cal['left_distortion'])
        self.right_intrinsics = np.array(self.cal['right_intrinsics'])
        self.right_distortion = np.array(self.cal['right_distortion'])


    def calc_position(self, left_x, left_y, right_x, right_y):

        left_point = np.array([[left_x, left_y]]).astype('float')
        right_point = np.array([[right_x, right_y]]).astype('float')

        point_4d = None

        rect_point_left = None
        rect_point_left = cv2.undistortPoints(left_point,
                                       self.left_intrinsics,
                                       self.left_distortion,
                                       rect_point_left,
                                       self.R1,
                                       self.P1
                                    )
        rect_point_left = np.squeeze(rect_point_left).astype('float').T

        rect_point_right = None

        rect_point_right = cv2.undistortPoints(right_point,
                                       self.right_intrinsics,
                                       self.right_distortion,
                                       rect_point_right,
                                       self.R2,
                                       self.P2
                                    )
        rect_point_right = np.squeeze(rect_point_right).astype('float').T

        point_4d = cv2.triangulatePoints(self.P1, self.P2, rect_point_left, rect_point_right, point_4d)
        point_3d = point_4d[0:3, :] / np.tile(point_4d[3, :], 3).reshape(3, -1)

        # error as distance from epiline in rectified coordinate space
        error = (rect_point_left[1] - rect_point_right[1])

        return point_3d, error

    def calibration_info(self):
        print(self.cal)

class StereoCalc:

    def __init__(self, cal_file='../config/manta_binned_cal_steve.xml'):
        # just like before we specify an enum flag, but this time it is
        # FILE_STORAGE_READ
        cv_file = cv2.FileStorage(cal_file, cv2.FILE_STORAGE_READ)
        # for some reason __getattr__ doesn't work for FileStorage object in python
        # however in the C++ documentation, getNode, which is also available,
        # does the same thing
        # note we also have to specify the type to retrieve other wise we only get a
        # FileNode object back instead of a matrix
        self.alpha = cv_file.getNode("alpha").real()
        self.B = cv_file.getNode("B").mat()
        # convert B to vector
        self.B = np.squeeze(self.B)
        self.k1 = cv_file.getNode("k1").mat()
        self.k2 = cv_file.getNode("k2").mat()
        self.Fmatrix = cv_file.getNode("Fmatrix").mat()
        self.R12 = cv_file.getNode("R12").mat()

        cv_file.release()

    def calc_position(self, left_x, left_y, right_x, right_y):

        imagepoints1d = np.transpose(np.array([left_x, left_y, 1.0]))
        imagepoints2d = np.transpose(np.array([right_x, right_y, 1.0]))

        R = np.transpose(self.R12)
        thetarec = np.arctan2(-self.B[2], self.B[0])
        ctr_ = np.cos(thetarec)
        str_ = np.sin(thetarec)

        Rrectheta = np.array([
            [ctr_, 0.0, -str_],
            [0.0, 1.0, 0.0],
            [str_, 0.0, ctr_]
        ])

        Bprime = Rrectheta @ self.B

        psirec = np.arctan2(Bprime[1], Bprime[0])
        spsir = np.sin(psirec)
        cpsir = np.cos(psirec)

        Rrecpsi = np.array([
            [cpsir, spsir, 0.0],
            [-spsir, cpsir, 0.0],
            [0.0, 0.0, 1.0]
        ])

        Rrec = Rrecpsi @ Rrectheta
        Bcheck = Rrec * self.B

        R1 = Rrec

        # Left image
        pud1 = self.k1 @ imagepoints1d
        vv1 = np.transpose(np.array([pud1[0], pud1[1], 1.0]))
        rad2 = vv1[0]**2 + vv1[1]**2

        vv3 = (1 + self.alpha * rad2) * vv1
        vv3[2] = 1.0

        vv2 = np.linalg.inv(self.k1) @ vv3

        imagepoints1 = vv2

        # right image
        pud2 = self.k2 @ imagepoints2d
        vv4 = np.transpose(np.array([pud2[0], pud2[1], 1.0]))
        rad2 = vv4[0]**2 + vv4[1]**2

        vv6 = (1 + self.alpha * rad2) * vv4
        vv6[2] = 1.0

        vv5 = np.linalg.inv(self.k2) @ vv6

        imagepoints2 = vv5

        p1 = imagepoints1
        p2 = imagepoints2

        epiline = self.Fmatrix @ p1
        epiline = epiline / np.sqrt(epiline[0]**2 + epiline[1]**2)

        doterror_inpixels = np.transpose(p2) @ epiline

        vv1 = np.linalg.inv(self.k2) @ R @ self.k1 @ p1
        vv2 = np.linalg.inv(self.k2) @ R @ self.B

        #print(vv2)

        M = np.array([
            [vv1[0], -vv2[0]],
            [vv1[1], -vv2[1]],
            [vv1[2], -vv2[2]]
        ])

        #print(M)

        MtM = np.transpose(M) @ M
        vv0 = np.linalg.inv(MtM) @ np.transpose(M) @ p2

        rho1 = vv0[0] / vv0[1]

        Position = rho1 * self.k1 @ p1

        Position_perp = R1 @ Position

        return Position_perp, doterror_inpixels

    def calibration_info(self):
        print("B:")
        print(str(self.B))
        print("k1:")
        print(str(self.k1))
        print("k2:")
        print(str(self.k2))
        print("FMatrix:")
        print(str(self.Fmatrix))
        print("R12:")
        print(str(self.R12))

if __name__=="__main__":

    s_calc = OpenCVStereoCalc()
    #s_calc = StereoCalc()
    #s_calc.calibration_info()
    # test some points
    pos, err = s_calc.calc_position(521, 349, 475, 328) # example from march 5 2020 stereo pair
    print(pos)
    print(err)
    pos_vf = get_pos_in_rba(pos)
    print(pos_vf)


