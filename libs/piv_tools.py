# -*- coding: utf-8 -*-
"""
piv_tools.py -- classes for estimating velocity from images
Author: pldr
Copyright 2020  Monterey Bay Aquarium Research Institute
Distributed under MIT license. See license.txt for more information.

"""

from openpiv import tools, pyprocess, scaling, validation, filters
import numpy as np
import cv2
from collections import deque
from pyqtgraph.Qt import QtCore
import time
import os

class PhaseCorrelation(QtCore.QThread):

    def __init__(self, navg=10, frame_rate_mod=1, focal_length=695, z0=0.3, zf=0.8):
        QtCore.QThread.__init__(self)
        self.navg = navg
        self.frame_pairs = deque()
        self.velocity_avg = deque()
        self.frame_counter = 0
        self.focal_length = focal_length    # the focal length in pixels
        self.z0 = z0    # the near focal range in meters
        self.zf = zf    # the far focal range in meters

    def add_frames(self, frame_a, frame_b):
        self.frame_pairs.append([frame_a, frame_b])

    def pixels_to_velocity_sepiv(self, dpix, dt):

        # first convert pixels displacement to meters
        dm = np.array(dpix) * ((self.z0**3 + self.zf**3)/2)**(1/3) / self.focal_length
        vm = dm / dt

        return vm

    def pixels_to_velocity_mid(self, dpix, dt):

        mag = (self.z0 + (self.zf - self.z0)/2) / self.focal_length
        dm = np.array(dpix) * mag
        vm = dm / dt

        return vm

    def add_velocity(self, vm):

        if len(self.velocity_avg) < self.navg:
            self.velocity_avg.append(vm)
        else:
            self.velocity_avg.popleft()
            self.velocity_avg.append(vm)

    def get_velocity(self):

        vavg = [0, 0]

        if len(self.velocity_avg) == self.navg:
            vm = np.array(list(self.velocity_avg))
            vavg = np.mean(vm, axis=0)

        return vavg

    def run(self):

        timer = time.time()

        while self.isRunning:

            if len(self.frame_pairs) > 0:
                pair = self.frame_pairs.popleft()
                img_a = np.mean(pair[0].img, axis=2)
                img_b = np.mean(pair[1].img, axis=2)
                corr_out = cv2.phaseCorrelate(img_a, img_b)
                vm = self.pixels_to_velocity_mid(corr_out[0], (pair[1].msg.utime - pair[0].msg.utime)/1000000)
                self.add_velocity(vm)
            else:
                time.sleep(0.1)

class SimplePIV(QtCore.QThread):

    def __init__(self, window_size=32, overlap=16, dt=0.1, search_area_size=64, sig2noise_method='peak2peak'):
        QtCore.QThread.__init__(self)
        self.window_size = window_size
        self.overlap = overlap
        self.dt = dt
        self.search_area_size = search_area_size
        self.sig2noise_method = sig2noise_method
        self.frame_pairs = deque()

    def add_frames(self, frame_a, frame_b):
        self.frame_pairs.append([frame_a,frame_b])

    def get_vector_field(self, frame_a, frame_b):

        frame_a = (frame_a * 1024).astype(np.int32)
        frame_b = (frame_b * 1024).astype(np.int32)

        u, v, sig2noise = pyprocess.extended_search_area_piv(
            frame_a,
            frame_b,
            window_size=self.window_size,
            overlap=self.overlap,
            dt=self.dt,
            search_area_size=self.search_area_size,
            sig2noise_method=self.sig2noise_method
        )
        x, y = pyprocess.get_coordinates(image_size=frame_a.shape, window_size=self.window_size, overlap=self.overlap)
        u, v, mask = validation.sig2noise_val(u, v, sig2noise, threshold=1.3)
        u, v, mask = validation.global_val(u, v, (-1000, 2000), (-1000, 1000))
        u, v = filters.replace_outliers(u, v, method='localmean', max_iter=10, kernel_size=2)
        x, y, u, v = scaling.uniform(x, y, u, v, scaling_factor=96.52)

        return u, v

    def run(self):

        while self.isRunning:

            if len(self.frame_pairs) > 0:
                pair = self.frame_pairs.popleft()
                #u, v = self.get_vector_field(pair[0], pair[1])
                #print(np.mean(u), np.mean(v))
            else:
                time.sleep(0.1)

