# -*- coding: utf-8 -*-
"""
tracking_tools.py -- classes for managing object tracking
Author: pldr
Copyright 2020  Monterey Bay Aquarium Research Institute
Distributed under MIT license. See license.txt for more information.

This borrows extensively from the following resources:

https://www.learnopencv.com/object-tracking-using-opencv-cpp-python/
https://www.pyimagesearch.com/2018/07/30/opencv-object-tracking/
https://pykalman.github.io/

"""

import cv2
import time
import copy
import numpy as np
from loguru import logger
from libs.box_tools import draw_box
from collections import deque
from pyqtgraph.Qt import QtCore
from libs.ml_tools import MLBox
from pykalman import UnscentedKalmanFilter


class TrackSupervisor(QtCore.QThread):

    def __init__(self, memory=10):
        QtCore.QThread.__init__(self)
        self.left_centroid = []
        self.right_centroid = []
        self.centroid = []


class RBATracker(QtCore.QThread):

    def __init__(self, max_deviation=0.30, window_size=10, max_cont_dev=10):
        QtCore.QThread.__init__(self)

        self.max_deviation = max_deviation
        self.window_size = window_size
        self.max_cont_dev = max_cont_dev

        self.state_cov = np.diag([1, 1, 1])
        self.trans_cov = self.state_cov
        self.obs_cov = np.diag([10, 10, 10])

        self.filtered_state_mean = np.zeros(6)
        self.filtered_state_covariance = self.state_cov

        self.running_median = deque()

        self.initialized = False
        self.reinit_counter = 0
        self.ukf = None

        self.stop_thread = False

    def init_tracker(self, rba):

        initial_mean = rba

        initial_mean[1] = initial_mean[1] * np.pi / 180.0

        self.ukf = UnscentedKalmanFilter(self.F, self.G,
            initial_state_mean=initial_mean,
            initial_state_covariance=self.state_cov,
            transition_covariance=self.trans_cov,
            observation_covariance=self.obs_cov
        )

        self.initialized = True

        self.filtered_state_mean = initial_mean
        self.filtered_state_covariance = self.state_cov

    def update(self, obs=None):

        if not self.initialized:
            return

        if self.reinit_counter > self.max_cont_dev and obs is not None:
            self._initTracker(obs)
            self.reinit_counter = 0
            return

        if obs is None:
            (m, C) = self.ukf.filter_update(self.filtered_state_mean, self.filtered_state_covariance)
        else:
            obs = np.array(obs)
            obs[1] = obs[1] * np.pi / 180.0
            st = np.array(self.filtered_state_mean[0:3])
            pos_error = np.linalg.norm(obs - st)
            #if pos_error >= self.max_deviation:
            #    #print('Large Error: ' + str(pos_error))
            #    self.reinit_counter += 1
            #    #alpha = (pos_error-self.max_deviation) / pos_error
            #    obs = st
            #else:
            self.reinit_counter = 0
            (m, C) = self.ukf.filter_update(
                self.filtered_state_mean,
                self.filtered_state_covariance,
                observation=obs,
            )

        self.filtered_state_mean = m
        self.filtered_state_covariance = C

    def F(self, state, noise):
        new_state = state

        for i in range(0, 3):
            new_state[i] = new_state[i]

        return new_state + noise

    def G(self, state, noise):
        return state[0:3] + noise


class StereoOpenCVTracker(QtCore.QThread):

    # Signals
    new_box = QtCore.Signal(object)


    def __init__(self, min_reinit=0.1, thread_sleep=30):
        QtCore.QThread.__init__(self)
        self.tracker = [None, None]
        self.class_name = None
        self.initialized = False
        self.do_init = False
        self.do_update = False
        self.track_setup = None
        self.track_okay = [False, False]
        self.tracker_box = [None, None]
        self.tracker_type = None
        self.images = None
        self.reinit_timer = 0
        self.min_reinit = min_reinit
        self.thread_sleep = thread_sleep
        self.stop_thread = False

    def _init_trackers(self):

        self.do_init = False

        delta = time.time() - self.reinit_timer
        if self.tracker[0] is not None and self.tracker[1] is not None and delta <= self.min_reinit:
            return

        if self.track_setup is None:
            logger.error('Track setup is None, can not initialize')
            return
            
        # if int(time.time()*10) % 10 == 0:
        # logger.info('KCF Reinit after ' + "{:.3f}".format(delta) + " s")

        # pull out parts of setup
        ml_imgs, boxes, class_name, scores, tracker_type = self.track_setup

        for t in [0, 1]:
            if boxes[t][2] > 5 and boxes[t][3] > 5:
                try:
                    self.init_tracker(ml_imgs[t].img, boxes[t], t, tracker_type)
                except:
                    logger.error("Could not init tracker box")

        if self.tracker[0] is not None:
            self.tracker_type = tracker_type
            self.reinit_timer = time.time()
            self.class_name = class_name
            self.scores = scores

            for t in [0,1]:
                box = MLBox(boxes[t][0], boxes[t][1], boxes[t][2], boxes[t][3], channel=t, utime=ml_imgs[t].utime,
                            class_name=self.class_name)
                # self.new_box.emit(box)
                self.track_okay[t] = True
                self.tracker_box[t] = box

        else:
            logger.error('Error (re)initializing tracker')

    def init_tracker(self, img, box, index, tracker_type='KCF', class_name='target', scores=[1.0]):
        if tracker_type == 'BOOSTING':
            self.tracker[index] = cv2.TrackerBoosting_create()
        elif tracker_type == 'MIL':
            self.tracker[index] = cv2.TrackerMIL_create()
        elif tracker_type == 'KCF':
            self.tracker[index] = cv2.TrackerKCF_create()
        elif tracker_type == 'TLD':
            self.tracker[index] = cv2.TrackerTLD_create()
        elif tracker_type == 'MEDIANFLOW':
            self.tracker[index] = cv2.TrackerMedianFlow_create()
        elif tracker_type == 'GOTURN':
            self.tracker[index] = cv2.TrackerGOTURN_create()
        elif tracker_type == 'MOSSE':
            self.tracker[index] = cv2.TrackerMOSSE_create()
        elif tracker_type == "CSRT":
            self.tracker[index] = cv2.TrackerCSRT_create()
        else:
            logger.error('Unsupported OpenCV Tracker Type.')

        self.class_name = class_name
        self.tracker_type = tracker_type
        self.reinit_timer = time.time()
        #print(box)
        self.tracker[index].init(img, box)
        self.scores = np.array(scores)


    def _update_tracker(self):

        self.do_update = False

        if self.stereo_ml_img is None:
            logger.warning('Trying to update tracker with None ml_img')
            return

        imgs = self.stereo_ml_img.split_images()

        for t in [0, 1]:

            channel = t
            if self.tracker[channel] is not None:

                (okay, bbox) = self.tracker[channel].update(imgs[t].img)
                # Create a new bounding box and emit
                if okay:
                    box = MLBox(bbox[0], bbox[1], bbox[2], bbox[3], channel=channel, utime=imgs[t].utime, class_name=self.class_name)
                    self.track_okay[channel] = True
                    self.tracker_box[channel] = box
                else:
                    logger.info("KCF lost track, reset.")
                    self.track_okay[channel] = False
                    self.reset_trackers()

    def reset_trackers(self):
        self.tracker = [None, None]
        self.track_okay = [False, False]

    def get_boxes(self, boxed_img, tracker_color):

        kcf_left = None
        kcf_right = None

        if boxed_img is not None:

            if self.track_okay[0] and self.track_okay[1]:
                draw_box(boxed_img, self.tracker_box[0], tracker_color)
                box = self.tracker_box[0]
                kcf_left = [box.left + box.width/2, box.top + box.height/2]

                box = copy.copy((self.tracker_box[1]))
                kcf_right = [box.left + box.width/2, box.top + box.height/2]
                box.left += boxed_img.shape[1]/2
                draw_box(boxed_img, box, tracker_color)

        return kcf_left, kcf_right, boxed_img

    def init(self, track_setup):
        self.track_setup = track_setup
        self.do_init = True

    def update(self, stereo_ml_img):
        self.stereo_ml_img = stereo_ml_img
        self.do_update = True

    def stop(self):
        self.stop_thread = True

    def run(self):

        logger.info('Starting tracker thread...')

        while self.isRunning and not self.stop_thread:

            if self.do_init:
                self._init_trackers()
            elif self.do_update:
                self._update_tracker()

            self.msleep(self.thread_sleep)

        logger.info('tracker thread stopped')

