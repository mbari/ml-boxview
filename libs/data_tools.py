# -*- coding: utf-8 -*-
"""
data_ttols.py -- helper functions for saving vehicle and tracking data
Author: pldr
Copyright 2020  Monterey Bay Aquarium Research Institute
Distributed under MIT license. See license.txt for more information.

"""


def save_vehicle_stats(vel, vehicle_pos_output, utime, pos_ml, pos_rba, depth, phi, theta, psi, rba_init):

    if len(pos_ml) == 0:
        pos_ml = [0, 0, 0]

    if len(pos_rba) == 0:
        pos_rba = [0, 0, 0]

    with open(vehicle_pos_output, 'a+') as f:
        f.write(str(utime) + ","
                + str(pos_ml[0]) + ","
                + str(pos_ml[1]) + ","
                + str(pos_ml[2]) + ","
                + str(pos_rba[0]) + ","
                + str(pos_rba[1]) + ","
                + str(pos_rba[2]) + ","
                + str(vel[0]) + ","
                + str(vel[1]) + ","
                + str(depth) + ","
                + str(phi) + ","
                + str(theta) + ","
                + str(psi) + ","
                + str(pos_ml is not None) + ","
                + str(rba_init) + "\r\n"
                )