# -*- coding: utf-8 -*-
"""
time_tools.py -- helper methods for averaging and timecode calculation
Author: pldr
Copyright 2020  Monterey Bay Aquarium Research Institute
Distributed under MIT license. See license.txt for more information.

"""

import datetime
import numpy as np
from loguru import logger
from collections import deque


class MovingAverage:

    def __init__(self, samples=10):
        self.samples = samples
        self.data = []
    def append(self, value):
        if len(self.data) < self.samples:
            self.data.append(value)
        else:
            self.data.pop(0)
            self.data.append(value)
    def average(self):
        return np.mean(self.data)


def get_timecode(utime):

    # handle timestamps in micro and milli
    if len(str(utime)) == 16:
        utime = float(utime)/1000000
    elif len(str(utime)) == 13:
        utime = float(utime)/1000
    elif len(str(utime)) != 10:
        logger.info('Unknown timestamp type in time code calculation.')

    dt = datetime.datetime.fromtimestamp(utime)

    return dt.strftime('%y%m%dT%H%M%S.%fZ'), utime*1000000

