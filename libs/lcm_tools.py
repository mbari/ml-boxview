# -*- coding: utf-8 -*-
"""
lcm_tools.py -- LCM handlers and tools
Author: pldr
Copyright 2020  Monterey Bay Aquarium Research Institute
Distributed under MIT license. See license.txt for more information.

"""

import lcm
from loguru import logger
from pyqtgraph.Qt import QtCore
from libs.data_handler import StereoMLHandler

class LCMManager(QtCore.QThread):

    def __init__(self, args):
        QtCore.QThread.__init__(self)
        logger.info("Initializing handler...")
        self.handler = StereoMLHandler(args)
        self.handler.start()
        self.left_camera_channel = args.leftchannel
        self.right_camera_channel = args.rightchannel
        self.stereo_channel = args.stereo_channel
        self.stereo_box_channel = args.stereo_box_channel
        self.tracker_box_channel = args.tracker_box_channel
        self.vehicle = args.vehicle
        self.stop_thread = False

    def stop(self):
        self.stop_thread = True

    def run(self):

        # LCM on local machine only
        lc = lcm.LCM("udpm://239.255.76.67:7667?ttl=0")
        logger.info("Creating subscriptions...")
        # Image handles for ML
        #lc.subscribe(self.stereo_channel, self.handler.handle_stereo_img)
        #lc.subscribe(self.stereo_box_channel, self.handler.handle_stereo_boxes)
        lc.subscribe('STEREO_IMAGE_WITH_BOXES', self.handler.handle_stereo_image_with_boxes)
        lc.subscribe(self.tracker_box_channel, self.handler.handle_tracker_boxes)

        # Use this insert time to test latency of the full workflow from image arrival to display
        lc.subscribe('MANTA_LEFT', self.handler.handle_insert_time)

        if self.vehicle == "DR":
            #lc.subscribe('MINIROV_INTFC_DEPTH', self.handler.handle_depth)
            #lc.subscribe('MINIROV_INTFC_RPY', self.handler.handle_rpy)

            lc.subscribe('DR_ROV_ATTITUDE', self.handler.handle_attitude)
            lc.subscribe('DR_ROV_DEPTH', self.handler.handle_new_depth)

        elif self.vehicle == "MINIROV":

            lc.subscribe('MINIROV_INTFC_DEPTH', self.handler.handle_depth)
            lc.subscribe('MINIROV_INTFC_RPY', self.handler.handle_rpy)

            lc.subscribe('MINIROV_ATTITUDE', self.handler.handle_attitude)
            lc.subscribe('MINIROV_DEPTH', self.handler.handle_new_depth)

        else:

            logger.error("No matching vehicle found, check the --vehicle argument")
            exit(1)

        while self.isRunning and not self.stop_thread:
            lc.handle_timeout(1000)

        logger.info('LCM Manager thread stopped')
