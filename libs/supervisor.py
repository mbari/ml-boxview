import os
import lcm
import json
import time
import numpy as np
from collections import deque
from libs.stereo_calc import StereoCalc, get_pos_in_rba
import pyqtgraph as pg
from loguru import logger
from pyqtgraph.Qt import QtCore, QtGui, QtWidgets
from config.gui_state import GuiSave, GuiRestore

# LCM types
from mwt.ext_target_t import ext_target_t
from mwt.stereo_bounding_box_t import stereo_bounding_box_t
from mwt.vehicle_cmd_t import vehicle_cmd_t
from mwt.mwt_control_status_t import mwt_control_status_t
from mwt.mwt_search_status_t import mwt_search_status_t
from mwt.mini_rov_status_t import mini_rov_status_t
from mwt.mwt_target_t import mwt_target_t
from mwt.mini_rov_attitude_t import mini_rov_attitude_t
from mwt.mini_rov_depth_t import mini_rov_depth_t
from mwt.mini_rov_status_t import mini_rov_status_t
from mwt.dr_rov_status_t import dr_rov_status_t
from pcon.ui_msg_t import ui_msg_t

class Supervisor(QtCore.QThread):

    # Passthough lcm objects to GUI
    mwt_target_sig = QtCore.Signal(object)
    search_stat_sig = QtCore.Signal(object)
    state_change_sig = QtCore.Signal(int)
    vehicle_depth = QtCore.Signal(float)
    vehicle_yaw = QtCore.Signal(float)
    resume_search_sig = QtCore.Signal(float)

    def __init__(self, args):
        QtCore.QThread.__init__(self)
        self.stop_thread = False
        self.states = ['IDLE', 'SEARCH', 'ACQUIRE', 'TRACK']
        self.command_lists = []
        self.current_state = 0
        self.lc = lcm.LCM("udpm://239.255.76.67:7667?ttl=0")

        self.vehicle = args.vehicle

        self.z_trim = 0.0

        self.latest_depth = 0.0
        self.latest_rpy = np.array([0.0,0.0,0.0])
        self.auto_control = False
        self.adv_nav = False

        self.target_class = ''

        # mods for mitigating jitter in class label
        self.target_class_label_history = []
        self.target_class_label_history_length = 1
        self.ignore_class_label_in_track = False

        # Handle infered states from ui messages
        self.last_mode = ''

        self.acquire_mode = 'Object Detection'
        self.track_mode = 'Centered'
        self.acquireInMode3 = False

        # Save the last heading, depth, and thrust when in search
        self.last_search_heading = 0.0
        self.last_search_depth = 0.0
        self.last_search_xeff = 0.0
        self.resume_search_flag = False

        self.latest_mwt_target = mwt_target_t()
        self.latest_ext_target = ext_target_t()

        self.range_setpoint = 1.0
        self.bearing_setpoint = 1.0
        self.vertical_setpoint = 1.0
        self.range_deadband = 0.01
        self.z_deadband = 0.01
        self.bearing_deadband = 5.0

        self.min_acquire_range = 0.75
        self.max_aquire_range = 2.5
        self.max_aquire_bearing = 30
        self.max_aquire_z = 2.5

        self.min_track_range = 0.35
        self.max_track_range = 1.75
        self.max_track_bearing = 20
        self.max_track_z = 0.5

        self.acquire_range_gain = 5
        self.acquire_bearing_gain = .25
        self.acquire_z_gain = .5

        self.track_range_gain = .01
        self.track_bearing_gain = .5
        self.track_z_gain = .01

        self.z_search_slew_rate = 0.1

        self.acquire_timeout = 10
        self.track_timeout = 10
        self.search_timeout = 3
        self.track_duration = 10

        self.trackable_list = []

        self.latest_tracker_time = time.time()

        self.pointing_timer = time.time()
        self.tracking_timer = time.time()

        self.acquire_timer = time.time()
        self.track_timer = time.time()
        self.search_timer = time.time()
        self.track_duration_timer = time.time()

        self.search_resume_timer = time.time()
        self.search_to_acquire_timer = time.time()

        self.state_control_timer = QtCore.QTimer()
        self.state_control_timer.timeout.connect(self.state_control)
        self.state_control_timer.start(200)


    def set_z_trim(self, trim):
        self.z_trim = trim
        self.setContVar('zTestCmd', str(self.z_trim))

    def class_is_trackable(self, class_name):

        #logger.info(self.trackable_list)

        for name in self.trackable_list:
            #logger.info(name + " =? " + class_name)
            if name.lower() == class_name:
                return True

        return False

    def majority_class_is_trackable(self):

        # If we don't have enough samples yet, return false
        if len(self.target_class_label_history) < self.target_class_label_history_length:
            self.target_class_label_history.append(self.latest_ext_target.left_class_name)
            return False

        # enumerate the labels for each sample and check for majority
        trackable_count = 0
        for label in self.target_class_label_history:
            trackable_count += int(self.class_is_trackable(label))
        
        
        # moving window of class labels
        self.target_class_label_history = self.target_class_label_history[1:]
        self.target_class_label_history.append(self.latest_ext_target.left_class_name)
        self.latest_ext_target.left_class_name = 'None'
        logger.info("Trackable Fraction: " + str(trackable_count/self.target_class_label_history_length))
        logger.info(self.target_class_label_history)
        #self.target_class_label_history = []
        
        if trackable_count/self.target_class_label_history_length > 0.5:
            return True
        else:
            return False

    def target_in_range(self, min_r, max_r, max_b, max_z):

        if time.time() - self.latest_tracker_time > 1:
            return False

        r_valid, b_valid, z_valid = [False, False, False]

        object_okay = False

        if self.current_state == 1:
            if self.acquire_mode == 'Object Detection':
                object_okay = True
            elif self.acquire_mode == 'Classification':
                #object_okay = self.latest_ext_target.left_class_name == self.target_class
                #object_okay = self.class_is_trackable(self.latest_ext_target.left_class_name)
                if self.target_class_label_history_length > 1:
                    object_okay = self.majority_class_is_trackable()
                else:
                    object_okay = self.class_is_trackable(self.latest_ext_target.left_class_name)

        elif self.current_state == 2:
            #if len(self.trackable_list) > 0:
            if self.target_class_label_history_length > 1:
                object_okay = self.majority_class_is_trackable()
            else:
                object_okay = self.class_is_trackable(self.latest_ext_target.left_class_name)
            #else: 
            #    object_okay = self.latest_ext_target.left_class_name == self.target_class

        elif self.current_state == 3:
            #if len(self.trackable_list) > 0:
            if not self.ignore_class_label_in_track:
                if self.target_class_label_history_length > 1:
                    object_okay = self.majority_class_is_trackable()
                else:
                    object_okay = self.class_is_trackable(self.latest_ext_target.left_class_name)
            else:
                object_okay = True
            #else: 
            #    object_okay = self.latest_ext_target.left_class_name == self.target_class

        #logger.info("Target okay: " + str(object_okay))

        if self.latest_mwt_target.range_meters > min_r and self.latest_mwt_target.range_meters < max_r:
            r_valid = True
        if abs(self.latest_mwt_target.bearing_degrees) < max_b:
            b_valid = True
        if abs(self.latest_mwt_target.z_meters) < max_z:
            z_valid = True

        if object_okay and r_valid and b_valid and z_valid:
            return True

        return False

    def target_in_acquire_range(self):
        return self.target_in_range(self.min_acquire_range, self.max_aquire_range, self.max_aquire_bearing, self.max_aquire_z)

    def target_in_track_range(self):
        return self.target_in_range(self.min_track_range, self.max_track_range, self.max_track_bearing, self.max_track_z)

    def point_towards_target(self):

        if time.time() - self.pointing_timer > 0.25:

            if int(time.time()*10) % 10 == 0:
                logger.info("Heading towards target in acquire...")
            
            # Proportional control 
            range_error = self.latest_mwt_target.range_meters - self.max_track_range
            bearing_error = self.latest_mwt_target.bearing_degrees # positive bering when target is right of center of FOV
            z_error = self.latest_mwt_target.z_meters # positive z when target is below center of FOV
            
            self.setContVar('xSearchEffort', str(int(self.acquire_range_gain * range_error)))
            
            # Relative Adjustments
            #self.callContFunc('yawSearchMoveRel', str(self.acquire_bearing_gain * bearing_error))
            #self.callContFunc('zSearchMoveRel', str(self.acquire_z_gain*z_error))

            # Absolute Adjustments
            new_heading = self.latest_rpy[2] + self.acquire_bearing_gain*bearing_error
            if new_heading > 360:
                new_heading = new_heading - 360
            elif new_heading < 0:
                new_heading = 360 + new_heading

            depth_adjustment = self.latest_depth + self.acquire_z_gain*z_error

            if bearing_error < -0.5:
                self.sendContCmd('pause')
                self.callContFunc('yawSearchMoveAbsNeg', str(abs(new_heading)))
            elif bearing_error > 0.5:
                self.sendContCmd('pause')
                self.callContFunc('yawSearchMoveAbsPos', str(abs(new_heading)))
            self.callContFunc('zSearchMoveAbs', str(depth_adjustment))

            self.pointing_timer = time.time()


    def center_target(self):

        if time.time() - self.tracking_timer > 0.25:
        
            # Proportional control 
            range_error = -1.0 * (self.latest_mwt_target.range_meters - self.range_setpoint)
            bearing_error = -1.0 * self.latest_mwt_target.bearing_degrees # positive bering when target is right of center of FOV
            z_error = -1.0 * self.latest_mwt_target.z_meters # positive z when target is below center of FOV

            if abs(self.track_range_gain*range_error) > self.range_deadband or abs(self.track_bearing_gain*bearing_error) > self.bearing_deadband or abs(self.track_z_gain*z_error) > self.z_deadband:

                logger.info("Centering target in track...")

                #self.callContFunc('rangeMoveRel', str((self.track_range_gain * range_error)))
                #self.callContFunc('bearingMoveRel', str(self.track_bearing_gain * bearing_error))
                #self.callContFunc('verticalMoveRel', str(self.track_z_gain*z_error))

                self.callContFunc('rangeMoveAbs', str(self.range_setpoint))
                if self.latest_mwt_target.bearing_degrees < self.bearing_setpoint:
                    self.callContFunc('bearingMoveAbsPos', str(self.bearing_setpoint))
                else:
                    self.callContFunc('bearingMoveAbsNeg', str(self.bearing_setpoint))
                self.callContFunc('verticalMoveAbs', str(self.vertical_setpoint))

            self.tracking_timer = time.time()

    
    def handle_mwt_target(self, channel, data):
        self.mwt_target_sig.emit(data)
        self.latest_mwt_target = mwt_target_t.decode(data)
        self.latest_tracker_time = time.time()
        #logger.debug('Received MWT_TARGET at: ' + str(time.time()))

    def resume_search(self):
        self.resume_search_sig.emit(1.0)

    def state_control(self):

        # Automatic aquire logic
        if self.auto_control:

            if self.current_state == 1: # SEARCH
                
                # Resume the last search if we get here from acquire or track
                if self.resume_search_flag and time.time() - self.search_resume_timer > self.search_timeout:
                    # set the xeff, auto depth and auto heading
                    logger.info("resuming search")
                    self.resume_search()
                    self.resume_search_flag = False
                
                # If target is in range and we have been searching long enough, acquire
                if self.target_in_acquire_range() and time.time() - self.search_resume_timer > self.acquire_timeout:
                    self.current_state = 2 # ACQUIRE
                    self.acquire_timer = time.time()
                    self.state_change_sig.emit(2)
                else:
                    # Otherwise just save the last depth and yaw values to resume later
                    self.last_search_depth = self.latest_depth
                    self.last_search_heading = self.latest_rpy[2]

            elif self.current_state == 2: # ACQUIRE
                if self.target_in_track_range():
                    self.current_state = 3 # TRACK
                    self.track_timer = time.time()
                    self.track_duration_timer = time.time()
                    self.state_change_sig.emit(3)
                elif self.target_in_acquire_range():
                    if self.acquireInMode3:
                        self.track_timer = time.time()
                        self.center_target()
                    else:
                        self.point_towards_target()
                    self.acquire_timer = time.time()
                else:
                    if time.time() - self.acquire_timer > self.acquire_timeout:
                        self.current_state = 1 # Resume SEARCH
                        self.resume_search_flag = True
                        self.search_resume_timer = time.time()
                        self.state_change_sig.emit(1)
                        logger.info("Resuming search mode from acquire")
                        self.target_class_label_history = []
            elif self.current_state == 3: # TRACK
                if self.target_in_track_range():
                    self.track_timer = time.time()
                    # Define the target setpoint if enabled
                    if self.track_mode == 'Centered':
                        self.center_target()
                    # Check if we have exceeded the track duration
                    if time.time() - self.track_duration_timer > self.track_duration:
                        self.current_state = 1 # Resume SEARCH
                        self.resume_search_flag = True
                        self.search_resume_timer = time.time()
                        self.state_change_sig.emit(1)
                        self.target_class_label_history = []
                elif self.target_in_acquire_range():
                    if time.time() - self.track_timer > self.track_timeout:
                        self.current_state = 2 # Resume ACQUIRE
                        self.state_change_sig.emit(2)
                        self.target_class_label_history = []
                else:
                    if time.time() - self.track_timer > self.track_timeout:
                        self.current_state = 1 # Resume SEARCH
                        self.resume_search_flag = True
                        self.search_resume_timer = time.time()
                        self.state_change_sig.emit(1)
                        self.target_class_label_history = []

        else:
            if self.current_state == 3:
                if self.track_mode == 'Centered':
                    self.center_target()

    def handle_ext_target(self, channel, data):
        self.latest_ext_target = ext_target_t.decode(data)

    def handle_search_stat(self, channel, data):
        self.search_stat_sig.emit(data)

    def handle_attitude(self, channel, data):

        attitude = mini_rov_attitude_t.decode(data)

        roll = attitude.roll_deg
        pitch = attitude.pitch_deg
        yaw = attitude.yaw_deg
        self.latest_rpy = np.array([roll, pitch, yaw])
        self.vehicle_yaw.emit(yaw)

    def handle_depth(self, channel, data):

        depth = mini_rov_depth_t.decode(data).depth
        self.latest_depth = depth
        self.vehicle_depth.emit(depth)

    def handle_vehicle_status(self, channel, data):

        nav_stat = 0

        if self.vehicle == "DR":
            msg = dr_rov_status_t.decode(data)
            nav_stat = msg.ext_ctrl
        else:
            msg = mini_rov_status_t.decode(data)
            nav_stat = msg.adv_nav
        
        # If the pilot takes control, set the controller to mode(1)
        if self.adv_nav == 1 and nav_stat == 0:
            self.sendContCmd('mode(1)')
        
        # The the pilot returns control, set the controller to idle
        if self.adv_nav == 0 and nav_stat == 1:
            self.state_change_sig.emit(1)

        # store the adv_nav state
        self.adv_nav = nav_stat

    def handle_ui_req(self, channel, data):
        
        msg = ui_msg_t.decode(data)
        if 'mode' in msg.msg:
            self.last_mode = msg
        
        # Track state is easy
        if msg.msg == 'mode(3)':
            self.current_state = 3 # TRACK
            self.track_timer = time.time()
            #self.state_change_sig.emit(3)
        elif msg.msg == 'mode(1)':
            self.current_state = 0 # IDLE
            self.track_timer = time.time()
            #self.state_change_sig.emit(0)
        elif msg.msg == 'xSearchEffort=0':
            self.current_state = 1 # SEARCH
            #self.state_change_sig.emit(1)
        elif msg.msg == 'xSearchEffort=5':
            self.current_state = 2 # ACQUIRE
            #self.state_change_sig.emit(2)

        logger.info(msg.msg)


    def setContVar(self, cmd, val):
        """send string to the controller of the form: cmd = val 

        Args:
            cmd (string): The variable name
            val (double): the variable value
        """
        self.sendContCmd(cmd + ' = ' + str(val))

    def callContFunc(self,func, val):
        """Send string to the controller of the form: func(val)

        Args:
            cmd (string): The function name
            val (double): the variable value
        """
        self.sendContCmd(func + '(' + str(val) + ')')

    def sendContCmd(self, cmd):
        """Send command string to controller

        Args:
            cnd (string): the string to send
        """
        msg = ui_msg_t()
        msg.msg = cmd
        self.lc.publish('UI_CONTROL_REQ', msg.encode())
        logger.info(cmd)

    def sendContCmds(self, cmds):
        """Send list of command strings to controller

        Args:
            cmds (list): list of strings
        """
        for cmd in cmds:
            self.sendContCmd(cmd)
            time.sleep(0.05)

        # Add vertical thrust to trim out vehicle
        #if abs(self.z_trim) > 0:
        self.setContVar('zTestCmd', self.z_trim)

    def stop(self):
        self.stop_thread = True

    def run(self):

        # LCM on local machine only

        logger.info("Creating subscriptions...")
        self.lc.subscribe('MWT_TARGET', self.handle_mwt_target)
        self.lc.subscribe(('MWT_SEARCH_STAT'), self.handle_search_stat)

        if self.vehicle == "DR":
            self.lc.subscribe('DR_ROV_ATTITUDE', self.handle_attitude)
            self.lc.subscribe('DR_ROV_DEPTH', self.handle_depth)
            self.lc.subscribe('DR_ROV_STATUS', self.handle_vehicle_status)
        elif self.vehicle == "MINIROV":
            self.lc.subscribe('MINIROV_ATTITUDE', self.handle_attitude)
            self.lc.subscribe('MINIROV_DEPTH', self.handle_depth)
            self.lc.subscribe('MINIROV_STATUS', self.handle_vehicle_status)
        else:
            logger.error("No matching vehicle found, check --vehicle argument")
            exit(1)

        self.lc.subscribe('EXT_TARGET_3', self.handle_ext_target)
        
        #self.lc.subscribe('UI_CONTROL_REQ', self.handle_ui_req)

        logger.info('Supervisor thread starting...')

        while self.isRunning and not self.stop_thread:
            self.lc.handle_timeout(1000)

        logger.info('Supervisor thread stopped.')