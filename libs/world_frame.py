import lcm
import time
from loguru import logger
import numpy as np
from mwt.mini_rov_attitude_t import mini_rov_attitude_t
from mwt.mini_rov_depth_t import mini_rov_depth_t
from pyqtgraph.Qt import QtCore

def get_rot_mat(rpy):

    phi, theta, psi = rpy * np.pi / 180.0

    t_phi = np.array([
        [1.0, 0.0, 0.0],
        [0.0, np.cos(phi), np.sin(phi)],
        [0.0, -np.sin(phi), np.cos(phi)]
    ])

    t_theta = np.array([
        [np.cos(theta), 0.0, -np.sin(theta)],
        [0.0, 1.0, 0.0],
        [np.sin(theta), 0.0, np.cos(theta)]
    ])

    t_psi = np.array([
        [np.cos(psi), np.sin(psi), 0.0],
        [-np.sin(psi), np.cos(psi), 0.0],
        [0.0, 0.0, 1.0]
    ])

    r_mat = t_phi @ t_theta @ t_psi

    return r_mat


def calc_wf_location(rpy, cam_pos, target_loc):

    cl_position = cam_pos
    position_vf = target_loc

    rot_vf_wf = get_rot_mat(rpy)
    rot_wf_vf = np.transpose(rot_vf_wf)

    position_wf = rot_wf_vf @ (position_vf + cl_position)

    return position_wf

def calc_vf_location(rpy, cam_pos, target_loc):

    cl_position = cam_pos
    position_wf = target_loc

    rot_vf_wf = get_rot_mat(rpy)

    position_vf = rot_vf_wf @ position_wf + cl_position

    return position_vf

class WorldFrame(QtCore.QThread):

    def __init__(self, args, thread_sleep=10):

        QtCore.QThread.__init__(self)

        self.thread_sleep = thread_sleep

        self.lc = lcm.LCM("udpm://239.255.76.67:7667?ttl=0")

        self.latest_depth = 0.0
        self.latest_rpy = np.array([0.0,0.0,0.0])
        self.camera_pos = np.array([1.0,0.0,0.0])
        self.latest_vf_position = np.array([0.0,0.0,0.0])
        self.latest_wf_position = np.array([0.0,0.0,0.0])
        self.last_wf_update = 0

        self.stop_thread = False

        self.lc.subscribe('MINIROV_ATTITUDE', self.handle_attitude)
        self.lc.subscribe('MINIROV_DEPTH', self.handle_depth)

    def handle_attitude(self, channel, data):

        attitude = mini_rov_attitude_t.decode(data)

        roll = attitude.roll_deg
        pitch = attitude.pitch_deg
        yaw = attitude.yaw_deg
        self.latest_rpy = np.array([roll, pitch, yaw])

    def handle_depth(self, channel, data):

        depth = mini_rov_depth_t.decode(data).depth
        self.latest_depth = depth

    def update_wf_position(self, position_vf):

        self.latest_wf_position = calc_wf_location(self.latest_rpy, self.camera_pos, position_vf)
        self.last_wf_update = time.time()

    def get_wf_position(self):

        self.latest_vf_position = calc_vf_location(self.latest_rpy, self.camera_pos, self.latest_wf_position)
        return self.latest_vf_position, time.time() - self.last_wf_update

    def stop(self):
        self.stop_thread = True

    def run(self):

        while self.isRunning and not self.stop_thread:
            self.lc.handle_timeout(1000)

