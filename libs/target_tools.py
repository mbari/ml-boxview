# -*- coding: utf-8 -*-
"""
target_tools.py -- functions for processing and publishing LCM target data
Author: pldr
Copyright 2020  Monterey Bay Aquarium Research Institute
Distributed under MIT license. See license.txt for more information.

"""

import time
import numpy as np
from loguru import logger
from mwt.ext_target_t import ext_target_t
from mwt.mwt_target_t import mwt_target_t

class PositionAverage:

    def __init__(self, nsamples=10):
        self.nsamples = nsamples
        self.history = np.zeros((nsamples, 3))
        self.index = 0
        self.latest_average = np.array([0.0,0.0,0.0])

    def update(self, pos):
        if self.index < self.nsamples:
            self.history[self.index,:] = pos
            self.index += 1
        else:
            self.history = np.roll(self.history,-1,0)
            self.history[self.index-1,:] = pos
            self.latest_average = np.mean(self.history,0)

    def get_latest(self):
        return self.latest_average

def rba_to_xyz(pos):
    xyz = [0.0,0.0,0.0] 
    xyz[0] = pos[0]
    xyz[1] = np.tan(pos[1]*np.pi/180.0)*pos[0]
    xyz[2] = pos[2]

    return xyz


def average_positions(p1, p2, p3):
    out = p1
    for i in range(0,3):
        out[i] = np.mean([[p1[i], p2[i], p3[i]]])

    return out


def pos_diff(last_pos, pos_new):

    if last_pos is not None and pos_new is not None:
        last_xyz = rba_to_xyz(last_pos)
        xyz_new = rba_to_xyz(pos_new)
        #diff = np.sqrt(np.abs((xyz_new[0]-last_xyz[0]))**2 + (np.abs(xyz_new[1]-last_xyz[1]))**2 + np.abs(xyz_new[2]-last_xyz[2])**2)
        diff = np.abs(xyz_new[0]-last_xyz[0])
        return diff
    else:
        return 0.0


def fuse_rba(target_source, ml_rba, kcf_rba, ukf_rba, tracker_rba, is_coasting=False, last_rba=None, max_diff=0.145):

    log_flag = int(time.time()*10) % 10 == 0

    if target_source == 'KCF Box' and kcf_rba is not None and pos_diff(kcf_rba, last_rba) < max_diff:
        if log_flag:
            logger.info('KCF, diff = ' + "{:.3f}".format(pos_diff(kcf_rba, last_rba)))
        return kcf_rba, 'KCF Box'
    if target_source == '3D Tracker' and tracker_rba is not None and pos_diff(tracker_rba, last_rba) < max_diff:
        if log_flag:
            logger.info('Stereo Tracker, diff = ' + "{:.3f}".format(pos_diff(kcf_rba, last_rba)))
        return tracker_rba, '3D Tracker'
    if target_source == 'ML Box' and ml_rba is not None and pos_diff(ml_rba, last_rba) < max_diff:
        if log_flag:
            logger.info('ML, diff = ' + "{:.3f}".format(pos_diff(ml_rba, last_rba)))
        return ml_rba, 'ML Box'
    if target_source == 'Best Available':

        # average targest if all are available and close
        #if tracker_rba is not None and pos_diff(tracker_rba, last_rba) < max_diff and \
        #        ml_rba is not None and pos_diff(ml_rba, last_rba) < max_diff and \
        #        kcf_rba is not None and pos_diff(kcf_rba, last_rba) < max_diff:

        #    return average_positions(tracker_rba, ml_rba, kcf_rba), 'AVG Box'

        if not is_coasting and tracker_rba is not None and pos_diff(tracker_rba, last_rba) < max_diff:
            if log_flag:
                logger.info('Best Available => Stereo Tracker, diff = ' + "{:.3f}".format(pos_diff(tracker_rba, last_rba)))
            return tracker_rba, '3D Tracker'
        elif ml_rba is not None and pos_diff(ml_rba, last_rba) < max_diff:
            if log_flag:
                logger.info('Best Available => ML, diff = ' + "{:.3f}".format(pos_diff(ml_rba, last_rba)))
            return ml_rba, 'ML Box'
        elif kcf_rba is not None and pos_diff(kcf_rba, last_rba) < max_diff:
            if log_flag:
                logger.info('Best Available => KCF, diff = ' + "{:.3f}".format(pos_diff(kcf_rba, last_rba)))
            return kcf_rba, 'KCF Box'
        else:
            return None, 'None'

    return None, 'None'


def validate(last_rba, rba, min_range, max_range, min_alt, max_alt):
    if last_rba is None:
        return True, True, True
    else:
        delta_rba = np.array(last_rba) - np.array(rba)
        ret_val = [True, True, True]
        # it's proving problematic to use a change threshold
        #for i in [0, 1, 2]:
        #    if delta_rba[i] < self.max_delta_rba[i]:
        #        ret_val[i] = True
        if rba[0] < min_range or rba[0] > max_range:
            ret_val[0] = False
        if rba[2] < min_alt or rba[2] > max_alt:
            ret_val[0] = False
        return ret_val

def pub_ext_target_3(lc, left_x, left_y,
                     right_x, right_y,
                     left_utime, right_utime,
                     right_class_name, left_class_name,
                     conf_left, conf_right):
    msg_out = ext_target_t()
    msg_out.conf_left = 100 * conf_left
    msg_out.conf_right = 100 * conf_right
    msg_out.left_class_name = left_class_name
    msg_out.right_class_name = right_class_name
    msg_out.left_utime = left_utime
    msg_out.right_utime = right_utime
    msg_out.left_x = left_x
    msg_out.right_x = right_x
    msg_out.left_y = left_y
    msg_out.right_y = right_y
    msg_out.source = 3.0

    lc.publish('EXT_TARGET_3', msg_out.encode())

def publish_mwt_target(lc, x_left, y_left, x_right, y_right, fused_rba, valid_vec, chan='MWT_TARGET'):

    if fused_rba is None:
        return

    # for now just publish on the UKF target
    mwt_target = mwt_target_t()

    mwt_target.range_valid = valid_vec[0]
    mwt_target.bearing_valid = valid_vec[1]
    mwt_target.z_valid = valid_vec[2]

    mwt_target.range_meters = fused_rba[0]
    mwt_target.z_meters = fused_rba[2]
    mwt_target.bearing_degrees = fused_rba[1]

    mwt_target.left_pix_x = x_left
    mwt_target.left_pix_y = y_left
    mwt_target.right_pix_x = x_right
    mwt_target.right_pix_y = y_right

    if mwt_target.range_valid and mwt_target.bearing_valid and mwt_target.z_valid:
        lc.publish(chan, mwt_target.encode())