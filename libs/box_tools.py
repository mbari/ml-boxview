# -*- coding: utf-8 -*-
"""
box_tools.py -- helper methods for dealing with bounding boxes
Author: pldr
Copyright 2020  Monterey Bay Aquarium Research Institute
Distributed under MIT license. See license.txt for more information.

"""

import cv2
import json
import copy
from loguru import logger
import numpy as np
from libs.stereo_calc import get_pos_in_rba
from libs.target_tools import pos_diff


def get_centroid(p0, p1):
    horz = p0[0] + (p1[0] - p0[0]) / 2
    vert = p0[1] + (p1[1] - p0[1]) / 2

    return horz, vert


def constrain_size(s, s_max):
    if s > s_max:
        return s_max
    else:
        return s


def get_box(pt0, pt1, center, box_from_ml, box_pad, box_size_x, box_size_y):
    if not box_from_ml:
        width = box_size_x
        height = box_size_y
    else:
        width = constrain_size(pt1[0] - pt0[0] + 2 * box_pad, box_size_x)
        height = constrain_size(pt1[1] - pt0[1] + 2 * box_pad, box_size_y)

    box = (center[0] - width / 2, center[1] - height / 2, width, height)

    return box


def draw_box_pairs(img, left_boxes, right_boxes, box_color):

    for b in left_boxes:
        draw_box(img, b, box_color)

    for b in right_boxes:
        box_copy = copy.copy(b)
        box_copy.left += img.shape[1] / 2  # shift box for display
        draw_box(img, box_copy, box_color)


def draw_box(img, box, box_color):

    pt0, pt1 = get_points(box)
    cv2.rectangle(img, tuple(pt0), tuple(pt1), box_color, 2)


def draw_labeled_box(img, box, box_color, label_color):

    pt0, pt1 = get_points(box)
    cv2.rectangle(img, tuple(pt0), tuple(pt1), box_color, 2)
    cv2.putText(img, box.class_name + " : " + str(int(100*np.max(box.scores))),
                (pt0[0], pt1[1] + 20), cv2.FONT_HERSHEY_COMPLEX, 0.65, label_color)

def draw_tracker_box(img, box, box_color, label_color):

    pt0, pt1 = get_points(box)
    tracker_data = json.loads(box.aux)
    cv2.rectangle(img, tuple(pt0), tuple(pt1), box_color, 2)
    cv2.putText(img, 'Score: ' + str(int(np.max(box.scores))) + ', Id: ' + str(tracker_data['track_id']) + ', IOU: ' + "{:.2f}".format(tracker_data['iou']),
                (pt0[0], pt1[1] + 40), cv2.FONT_HERSHEY_COMPLEX, 0.65, label_color)
    #cv2.putText(img, box.class_name, (pt0[0], pt1[1] + 20), cv2.FONT_HERSHEY_COMPLEX, 0.65, label_color)


def get_points(box):
    pt0 = (round(box.left), round(box.top))
    pt1 = (round(box.left + box.width), round(box.top + box.height))

    return pt0, pt1


def get_boxes_for_class(boxes, class_name):
    matches = []
    for b in boxes:
        if b.class_name == class_name:
            matches.append(b)
    return matches

def get_boxes_in_class_list(boxes, class_list):
    matches = []
    #logger.info(class_list)
    for b in boxes:
        for name in class_list:
            #logger.info(name + " =? " + b.class_name.lower())
            if b.class_name.lower() == name:
                #logger.info("Box Match: " + b.class_name.lower())
                matches.append(b)
    return matches

def box_similarity(b1, b2):
    d1 = 1.0 - abs(b1.width - b2.width + 0.01) / (b1.width + b2.width)
    d2 = 1.0 - abs(b1.height - b2.height + 0.01) / (b1.height + b2.height)
    sim = 100*np.mean([d1, d2])

    return sim

def box_epierror(b1, b2, stereo_calc):

    left_x = b1.left + float(b1.width)/2
    left_y = b1.top + float(b1.height)/2
    right_x = b2.left + float(b2.width)/2 - 1032
    right_y = b2.top + float(b2.height)/2

    pos_perp, err = stereo_calc.calc_position(
        left_x,
        left_y,
        right_x,
        right_y
    )

    return err


def location_diff(b1, b2, last_pos, stereo_calc):

    left_x = b1.left + float(b1.width)/2
    left_y = b1.top + float(b1.height)/2
    right_x = b2.left + float(b2.width)/2 - 1032
    right_y = b2.top + float(b2.height)/2

    pos_perp, err = stereo_calc.calc_position(
        left_x,
        left_y,
        right_x,
        right_y
    )
    pos_new = get_pos_in_rba(pos_perp)
    diff = pos_diff(last_pos, pos_new)

    return diff


def match_boxes_epipolar(left_boxes, right_boxes, max_epipolar, max_jump, stereo_calc=None, last_pos=None, class_list=[]):

    # extract boxes with matching class names
    left_matches = get_boxes_in_class_list(left_boxes, class_list)
    right_matches = get_boxes_in_class_list(right_boxes, class_list)

    #left_matches = left_boxes
    #right_matches = right_boxes


    min_error = 100
    max_dist = 100
    sim_pair = None

    for i in range(len(left_matches)):
        for j in range(len(right_matches)):
            if last_pos == None:
                epi_error = box_epierror(left_matches[i], right_matches[j], stereo_calc)
                if abs(epi_error) < min_error and abs(epi_error) < max_epipolar:
                    min_error = epi_error
                    #logger.info("ML box from epi_error: " + str(epi_error))
                    if left_matches[i].class_name ==  right_matches[j].class_name:
                        sim_pair = [left_matches[i], right_matches[j]]

            else:
                dist = location_diff(left_matches[i], right_matches[j], last_pos, stereo_calc)
                #logger.info(str(i) + "," + str(j) + " : " + str(dist))
                if dist < max_dist and dist < max_jump:
                    max_dist = dist
                    if left_matches[i].class_name ==  right_matches[j].class_name:
                        sim_pair = [left_matches[i], right_matches[j]]
                        break

    return sim_pair


def match_boxes(left_boxes, right_boxes, class_name, similarity=box_similarity, threshold=80, stereo_calc=None, last_pos=None):

    # extract boxes with matching class names
    left_matches = get_boxes_for_class(left_boxes, class_name)
    right_matches = get_boxes_for_class(right_boxes, class_name)
    

    # find most similar box pair
    max_sim = 0
    max_diff = (1 - threshold / 100)
    sim_pair = None

    for i in range(len(left_matches)):
        for j in range(len(right_matches)):

            if last_pos is not None:
                diff = location_diff(left_matches[i], right_matches[j], last_pos, stereo_calc)
                if diff < max_diff:
                    max_diff = diff
                    sim_pair = [left_matches[i], right_matches[j]]
            if sim_pair is None:
                sim = similarity(left_matches[i], right_matches[j])
                if sim > threshold and sim > max_sim:
                    max_sim = sim
                    sim_pair = [left_matches[i], right_matches[j]]

    return sim_pair
