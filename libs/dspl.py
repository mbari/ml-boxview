import lcm
import socket
from loguru import logger
from mwt.dspl_cfg_t import dspl_cfg_t

def set_light_output(dimming, ipaddr="192.168.127.254", lights=["0","1","2"], ports=[4002]):

    for l in lights:

        for i, port in enumerate(ports):

            try:
                dim = dimming[i]
            except:
                dim = dimming

            msgFromClient       = "!00"+l+":LOUT=" + str(dim) + "\r\n"
            bytesToSend         = str.encode(msgFromClient)
            serverAddressPort   = (ipaddr, port)
            bufferSize          = 1024

            # Create a UDP socket at client side
            UDPClientSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)

            # Send to server using created UDP socket
            UDPClientSocket.sendto(bytesToSend, serverAddressPort)

            UDPClientSocket.close()

            logger.info("set light @ port: " + str(port) + " to " + str(dim) + " %")

            msg = dspl_cfg_t()
            msg.lout = dim
            msg.port = port
            lc = lcm.LCM("udpm://239.255.76.67:7667?ttl=0")
            lc.publish('DSPL_CFG', msg.encode())