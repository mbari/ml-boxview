

class FilteredClassLabel:

    def __init__(self, samples=10, class_labels = []):

        self.counter = 0

        self.filtered_classes = {}
        self.class_labels = class_labels
        self.reset()


    def update(self,class_label, priority=1.0, selected=False):

        # update current value in class_label column using input priority and selected state

        # return the latest estimate for the given class_label

        return self.filtered_classes[class_label]

    def reset(self):

        for label in self.class_labels:
            self.filtered_classes[label] = 0.0

        self.counter = 0
