# -*- coding: utf-8 -*-
"""
lcm_image_mux.py -- Classes to distribute images to new channels
Author: pldr
Copyright 2020  Monterey Bay Aquarium Research Institute
Distributed under MIT license. See license.txt for more information.

"""


from pyqtgraph.Qt import QtCore
from lcm import LCM
from mwt.image_t import image_t

class LCMImageMux(QtCore.QThread):
    
    def __init__(self, chan_in, chan_out):
        QtCore.QThread.__init__(self)
        self.chan_in = chan_in
        self.chan_out = chan_out
        self.lc = LCM("udpm://239.255.76.67:7667?ttl=0")
        self.mux_counter = 0

    def __del__(self):
        self.wait()

    def handler(self, channel, data):
        if self.mux_counter % 3 == 0:
            self.lc.publish(self.chan_out, data)
        self.mux_counter += 1

    def run(self):

        self.lc.subscribe(self.chan_in, self.handler)

        while self.isRunning:
            self.lc.handle()


class LCMMux:

    def __init__(self, chan_in, chan_out):
        self.chan_in = chan_in
        self.chan_out = chan_out
        self.lc = LCM("udpm://239.255.76.67:7667?ttl=0")
        self.mux_counter = 0

    def __del__(self):
        self.wait()

    def __call__(self, channel, data):
        if self.mux_counter % 1 == 0:
            msg = image_t.decode(data)
            # Patch for timestamp bug in March 4 data
            if len(str(msg.utime)) < 16:
                msg.utime = msg.utime*1000
                self.lc.publish(self.chan_out, msg.encode())
            else:
                self.lc.publish(self.chan_out, data)
        self.mux_counter += 1
