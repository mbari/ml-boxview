# -*- coding: utf-8 -*-
"""
data_handler.py -- classes for managing lcm data and synchronizing images with bounding boxes for display
Author: pldr
Copyright 2020  Monterey Bay Aquarium Research Institute
Distributed under MIT license. See license.txt for more information.

"""

import os
import lcm
import cv2
import json
import copy
import time
import psutil
import numpy as np
from loguru import logger
from nvgpu import list_gpus
from collections import deque
from mwt.phi_theta_psi_t import phi_theta_psi_t
from mwt.mini_rov_attitude_t import mini_rov_attitude_t
from mwt.mini_rov_depth_t import mini_rov_depth_t
from mwt.image_t import image_t
from libs.world_frame import WorldFrame
from libs.data_exporter import BasicEntry, StillsExporter
from libs.target_tools import PositionAverage, fuse_rba, validate, pub_ext_target_3, publish_mwt_target, pos_diff
from libs.drawing_tools import draw_position, draw_ukf_position, draw_system_stats
from pyqtgraph.Qt import QtCore
from mwt.stereo_bounding_box_t import stereo_bounding_box_t
from mwt.stereo_image_t import stereo_image_t
from mwt.stereo_image_with_boxes_t import stereo_image_with_boxes_t
from mwt.skapp_dval_t import skapp_dval_t
from libs.stereo_calc import StereoCalc, get_pos_in_rba, get_pos_in_vf, vehicle_location_to_rba
from libs.piv_tools import PhaseCorrelation
from libs.ml_tools import StereoMLImage
from libs.image_tools import get_key
from libs.tracking_tools import StereoOpenCVTracker, RBATracker
from libs.time_tools import MovingAverage
from libs.box_tools import match_boxes_epipolar, draw_box_pairs, draw_labeled_box, get_points, get_centroid, get_box, draw_box, draw_tracker_box


class StereoMLHandler(QtCore.QThread):

    # Signals
    stereoFrame = QtCore.Signal(object)
    tracker_setup = QtCore.Signal(object)
    tracker_images = QtCore.Signal(object)

    def __init__(self, args, thread_sleep=10):

        QtCore.QThread.__init__(self)

        # general members
        self.left_channel = args.leftchannel
        self.right_channel = args.rightchannel
        self.ml_output_mod = args.ml_output_mod
        self.output_channel = args.output_channel
        self.stereo_cal = args.stereo_cal
        self.vehicle_pos_output = args.vehicle_pos_output
        self.video_output = args.video_output
        self.last_saved_ml = 0
        self.last_pos = None
        self.last_track_score = 0
        self.last_track_iou = 0
        self.is_coasting = False
        self.latest_img_timestamp = 0
        self.last_frame_time = time.time()
        self.ml_img_buffer = [{}, {}]
        self.insert_timestamps = {}
        self.stereo_ml_img_buffer = {}
        self.image_buffer = None
        self.process_queue = deque()
        self.tracker_queue = deque()
        self.box_buffer = []
        self.stereo_box_buffer = []
        self.box_buf = deque()
        self.tracker_box_buf = deque()
        self.selected_box_source = 'None'
        self.latest_tracker_boxes = None
        self.latest_tracker_pos = None
        self.display_buffer = [None, None]
        self.lost_boxes = [0, 0]
        self.lost_stereo_boxes = 0
        self.proc_boxes = [0, 0]
        self.proc_stereo_boxes = 0
        self.max_box_latency = 200
        self.latency = 0.0
        self.cpu_usage = 0.0
        self.gpu_usage = 0.0
        self.frame_rate_estimate = MovingAverage(samples=5)
        self.last_stereo_img = None
        self.stop_thread = False
        self.last_key = None
        self.tracker_pixel_error = 0.0
        self.latest_tracker_pos = None
        self.latest_tracker_time = time.time()
        self.latest_tracker_iou = 0.0
        self.latest_tracker_score = 0.0
        self.first_stats = True
        self.world_frame_timeout = 10

        self.class_list = []

        self.stills_output = args.stills_output
        if len(self.stills_output) > 0:
            self.stills_exporter = StillsExporter(export_path='Stills')
            self.stills_exporter.start()

        # Threaded tools
        self.object_tracker = StereoOpenCVTracker()
        self.tracker_setup.connect(self.object_tracker.init)
        self.tracker_images.connect(self.object_tracker.update)
        self.object_tracker.start()
        self.object_tracker_boxes = [None, None]

        # World Frame
        self.world_frame = WorldFrame(None)
        self.world_frame.start()

        # Filtered position
        self.filtered_pos = PositionAverage(nsamples=10)

        # Stereo Tools
        # self.stereo_calc = OpenCVStereoCalc(cal_file=self.stereo_cal)
        self.stereo_calc = StereoCalc(cal_file=self.stereo_cal)
        # self.stereo_calc.calibration_info()

        # PIV tools
        self.piv_estimator = PhaseCorrelation()
        self.piv_estimator.start()
        self.last_frame = None
        self.last_piv_estimate = 0

        # vehicle tools
        self.depth_history = deque()
        self.latest_depth = None
        self.yaw_history = deque()
        self.latest_yaw = None
        self.rpy_history = deque()
        self.latest_rpy = None

        # Tracking tools
        self.rba_tracker = RBATracker()
        self.max_delta_rba = [0.15, 1.0, 0.15]

        # Mouse tools
        self.left_click = None
        self.right_click = None

        # UI values
        self.max_target_step = 1.0
        self.min_track_score = 65
        self.min_track_iou = 0.65
        self.max_diff = 0.145
        self.max_epipolar_error = 10
        self.min_range = 0.2
        self.max_range = 3.5
        self.min_alt = -0.5
        self.max_alt = 0.5
        self.rate_limit = 0.0
        self.min_reinit_time = 250
        self.match_classes = True
        self.pub_kcf = True
        self.box_pad = 10
        self.score_threshold = 0.25
        self.box_match_threshold = 0.1
        self.match_method = None
        self.save_kcf_rois = False
        self.kcf_okay = False
        self.match_methods = []
        self.target_class_name = 'None'
        self.object_tracker_type = 'KCF'
        self.publish_MWT_TARGET = False
        self.publish_ext_target_3 = False
        self.use_world_frame = False
        self.missed_update = 0
        self.target_source = 'ML Box'
        self.reinit_threshold = 0.25
        self.box_size_x = 100
        self.box_size_y = 100
        self.box_size_from_ml = False

        # Colors
        self.box_color = (80, 80, 80)
        self.ext_target_3_color = (52, 235, 146)
        self.tracker_color = (235, 180, 52)
        self.box_tracker_color = (89, 164, 235)
        self.ml_color = (80, 140, 80)
        self.depth_color = (200, 175, 175)
        self.ukf_color = (50, 207, 207)
        self.kcf_color = (235, 52, 168)
        self.text_color = (3, 252, 53)
        self.overlay_color = (200, 252, 53)
        self.good_tracker_color = (250, 200, 52)

        # misc
        self.thread_sleep = thread_sleep

        # lcm public
        self.lc = lcm.LCM("udpm://239.255.76.67:7667?ttl=0")

    def handle_insert_time(self, channel, data):

        msg = image_t.decode(data)
        key = get_key(msg.utime)
        self.insert_timestamps[key] = time.time()

    def add_left_boxes(self, stereo_image, boxes):
        for box in boxes:
            self.process_left_stereo_box(stereo_image, box)
            self.proc_stereo_boxes += 1

    def add_right_boxes(self, stereo_image, boxes):
        for box in boxes:
            self.process_right_stereo_box(stereo_image, box)
            self.proc_stereo_boxes += 1

    def calc_fused_rba(self, pub_ml, pub_kcf, pos_rba, pos_tracker, is_coasting=False, last_rba=None, max_diff=100):

        if self.missed_update >= 30:
            max_diff = 100
            self.last_pos = None
            self.missed_update = 0

        fused_rba, self.selected_box_source = fuse_rba(self.target_source, pub_ml, pub_kcf, pos_rba, pos_tracker, is_coasting, last_rba, max_diff)

        if fused_rba is None:
            self.missed_update += 1
        else:
            self.missed_update = 0

        if self.missed_update > 0 and self.missed_update % 10 == 0:
            logger.info('Missed Updates: ' + str(self.missed_update))

        valid_vec = [False, False, False]
        if fused_rba is not None:

            valid_vec = validate(last_rba, fused_rba, self.min_range, self.max_range, self.min_alt, self.max_alt)

            # limit large changes in rba if rate_limit is not zero
            if self.last_pos is not None:
                mult = [1.0, 10.0, 1.0]
                if self.rate_limit > 0.0:
                    for i in [0, 1, 2]:
                        if np.abs(fused_rba[i]-last_rba[i]) > self.rate_limit:
                            fused_rba[i] = fused_rba[i] + np.sign(fused_rba[i]-last_rba[i])*self.rate_limit*mult[i]

        return fused_rba, valid_vec

    def process_tracker_boxes(self, output_img, tracker_data):

        if tracker_data is not None:

            tracker_boxes = tracker_data

            # Since only one box is sent now and pixel error is the same for each camera just grab it here
            if len(tracker_boxes.left_boxes.boxes) > 0:
                tracker_data = json.loads(tracker_boxes.left_boxes.boxes[0].aux)
                self.is_coasting = np.max(tracker_boxes.left_boxes.boxes[0].scores) < self.last_track_score
                self.last_track_score = np.max(tracker_boxes.left_boxes.boxes[0].scores)
                self.last_track_iou = tracker_data['iou']
                self.tracker_pixel_error = float(tracker_data['pixel_error'])

            for b in tracker_boxes.left_boxes.boxes:
                if np.max(tracker_boxes.left_boxes.boxes[0].scores) >= self.min_track_score:
                    draw_tracker_box(output_img, b, self.box_tracker_color, self.box_tracker_color)

            for b in tracker_boxes.right_boxes.boxes:
                if np.max(tracker_boxes.left_boxes.boxes[0].scores) >= self.min_track_score:
                    box_copy = copy.copy(b)
                    box_copy.left += output_img.shape[1] / 2  # shift box for display
                    draw_tracker_box(output_img, box_copy, self.box_tracker_color, self.box_tracker_color)

        return output_img

    def process_stereo_data(self, data, tracker_data=None):

        stereo_boxes = data[1]
        stereo_image = data[0]

        self.proc_stereo_boxes = 0
        if stereo_boxes is not None:
            self.add_left_boxes(stereo_image, stereo_boxes.left_boxes.boxes)
            self.add_right_boxes(stereo_image, stereo_boxes.right_boxes.boxes)

        self.proc_box_data(stereo_image, tracker_data)
        draw_system_stats(stereo_image, self.insert_timestamps, self.last_frame_time, self.frame_rate_estimate,
                          self.cpu_usage, self.gpu_usage, self.proc_stereo_boxes, self.latest_depth, self.latest_yaw,
                          self.overlay_color, self.depth_color)

        # Save the last frame time for frame rate calculation
        self.last_frame_time = time.time()

        return stereo_image.boxed_img

    def proc_box_data(self, stereo_image, tracker_data=None):

        # check for matching target boxes
        sim_pair = match_boxes_epipolar(stereo_image.left_boxes,
                               stereo_image.right_boxes,
                               self.max_epipolar_error,
                               self.max_target_step,
                               stereo_calc=self.stereo_calc,
                               last_pos=self.last_pos,
                               class_list = self.class_list)

        left_center = [0, 0]
        right_center = [0, 0]

        pos_rba = None
        pos_ml = None
        pos_kcf = None
        tracker_rba = None
        kcf_left = None
        kcf_right = None
        ml_target_class = "None"
        err_ml = 0.0
        err_kcf = 0.0

        # setup the output image that will have the boxes and overlay
        output_img = stereo_image.boxed_img

        # Get boxes and draw them on stereo image if available
        kcf_left, kcf_right, tmp = self.object_tracker.get_boxes(output_img, self.kcf_color)

        # Get tracker boxes
        if tracker_data is not None:
            output_img = self.process_tracker_boxes(output_img, tracker_data)

        # If we have a matching box-pair
        if sim_pair is not None:

            # Left Box
            pt0, pt1 = get_points(sim_pair[0])
            left_center = get_centroid(pt0, pt1)
            left_box = get_box(pt0, pt1, left_center, self.box_size_from_ml,
                               self.box_pad, self.box_size_x, self.box_size_y)
            #cv2.rectangle(output_img, pt0, pt1, self.ml_color, 2)
            draw_labeled_box(output_img, sim_pair[0], self.ext_target_3_color, self.ext_target_3_color)

            # Right Box
            pt0, pt1 = get_points(sim_pair[1])
            right_center = get_centroid(pt0, pt1)
            right_center = list(right_center)
            right_center[0] = right_center[0] - stereo_image.msg.width/2
            right_box = get_box(pt0, pt1, right_center, self.box_size_from_ml,
                                self.box_pad, self.box_size_x, self.box_size_y)
            #cv2.rectangle(output_img, pt0, pt1, self.ml_color, 2)
            draw_labeled_box(output_img, sim_pair[1], self.ext_target_3_color, self.ext_target_3_color)

            # compute stereo solution and range and map to RBA space
            pos_perp, err_ml = self.stereo_calc.calc_position(left_center[0], left_center[1],
                                                              right_center[0], right_center[1])
            pos_ml = get_pos_in_rba(pos_perp)

            ml_target_class = sim_pair[0].class_name

            # Have a matching ML pair, draw, init trackers, etc
            if np.max(sim_pair[0].scores) > self.reinit_threshold \
                    and np.max(sim_pair[1].scores) > self.reinit_threshold:
                left, right = stereo_image.split_images()
                setup = ([left, right], [left_box, right_box],
                         ml_target_class, np.mean([np.max(sim_pair[0].scores), np.max(sim_pair[1].scores)]),
                         self.object_tracker_type)
                self.tracker_setup.emit(setup)

        # 3D position tracking
        have_ml = pos_ml is not None

        # KCF position
        have_kcf = False
        if kcf_left is not None and kcf_right is not None:
            kcf_pos_perp, err_kcf = self.stereo_calc.calc_position(
                kcf_left[0],
                kcf_left[1],
                kcf_right[0],
                kcf_right[1]
            )
            
            log_flag = int(time.time()*10) % 10 == 0

            #if log_flag:
            #    logger.info('KCF Epipolar Error = ' + str(err_kcf))

            pos_kcf = get_pos_in_rba(kcf_pos_perp)

            if abs(err_kcf) < self.max_epipolar_error and pos_kcf[0] <= self.max_range and pos_kcf[0] >= self.min_range:

                have_kcf = True

                if not self.rba_tracker.initialized:
                    self.rba_tracker.init_tracker(copy.copy(pos_kcf))

                if self.rba_tracker.initialized:
                    self.rba_tracker.update(obs=copy.copy(pos_kcf))

            else:

                pos_kcf = None
                self.object_tracker.reset_trackers()
                #logger.info('KCF Reset')

        have_tracker = self.rba_tracker.initialized

        pos_rba = copy.copy(self.rba_tracker.filtered_state_mean[0:3])
        pos_rba[1] = 180.0 / np.pi * pos_rba[1]

        # error check the position estimates
        if have_ml and err_ml < self.max_epipolar_error:
            pub_ml = pos_ml
        else:
            pub_ml = None

        if have_kcf and err_kcf < self.max_epipolar_error:
            pub_kcf = pos_kcf
        else:
            pub_kcf = None

        if have_tracker:
            pub_rba = pos_rba
        else:
            pub_rba = None

        tracker_rba = None
        if self.latest_tracker_pos is not None: 

            tracker_good = False
            if self.last_track_iou >= self.min_track_iou and self.last_track_score >= self.min_track_score:
                tracker_good = True
                pos = np.array([self.latest_tracker_pos['x'], self.latest_tracker_pos['y'], self.latest_tracker_pos['z']])
                tracker_rba = get_pos_in_rba(pos)
                draw_box_pairs(output_img, tracker_data.left_boxes.boxes, tracker_data.right_boxes.boxes, self.good_tracker_color)

                # logger.info("tracker data good.")

                # KCF Reinit or init

                if len(tracker_data.left_boxes.boxes) > 0 and len(tracker_data.right_boxes.boxes) > 0:

                    #logger.info("KCF REINIT REQUEST.")

                    do_reinit = False # ML box will reinit KCF rather than Stereo tracker

                    # if KCF is tracking reinit if class names match
                    #if self.object_tracker.track_okay[0] == True and self.object_tracker.track_okay[1] == True:
                    #    if tracker_data.left_boxes.boxes[0].class_name != self.object_tracker.class_name:
                    #        logger.info("Stereo Tracker class does not match KCF")
                    #        do_reinit = False
                    #    else:
                    #        do_reinit = True
                    #else:
                    #    do_reinit = True
                    
                    if do_reinit:

                        # Left Box
                        pt0, pt1 = get_points(tracker_data.left_boxes.boxes[0])
                        left_center = get_centroid(pt0, pt1)
                        left_box = get_box(pt0, pt1, left_center, self.box_size_from_ml,
                                    self.box_pad, self.box_size_x, self.box_size_y)

                        # Right Box
                        pt0, pt1 = get_points(tracker_data.right_boxes.boxes[0])
                        right_center = get_centroid(pt0, pt1)
                        right_box = get_box(pt0, pt1, right_center, self.box_size_from_ml,
                                        self.box_pad, self.box_size_x, self.box_size_y)

                        left, right = stereo_image.split_images()
                        setup = ([left, right], [left_box, right_box],
                                tracker_data.left_boxes.boxes[0].class_name, self.last_track_score,
                                self.object_tracker_type)
                        self.tracker_setup.emit(setup)
            
            # Periodically check to see if we have not received tracker boxes recently (250 ms) if so
            # remove any stale box data
            if time.time() - self.latest_tracker_time > 0.25:
                self.latest_tracker_pos = None
                self.latest_tracker_boxes = None
                self.latest_tracker_time = time.time()

        # publish ext_target_3
        if self.publish_ext_target_3:
            if self.target_source == 'ML Box' and have_ml:
                pub_ext_target_3(self.lc, left_center[0], left_center[1], right_center[0], right_center[1],
                                 stereo_image.msg.left_utime, stereo_image.msg.right_utime,
                                 sim_pair[0].class_name, sim_pair[1].class_name, np.max(sim_pair[0].scores),
                                 np.max(sim_pair[1].scores))
            elif self.target_source == 'KCF Box' and have_kcf:
                pub_ext_target_3(self.lc, kcf_left[0], kcf_left[1], kcf_right[0], kcf_right[1],
                                 stereo_image.msg.left_utime, stereo_image.msg.right_utime,
                                 self.object_tracker.class_name, self.object_tracker.class_name,
                                 np.max(self.object_tracker.scores), np.max(self.object_tracker.scores))
            elif self.target_source == 'ML or KCF' or self.target_source == 'Best Available':
                if self.selected_box_source == 'ML Box' and have_ml:
                    pub_ext_target_3(self.lc, left_center[0], left_center[1], right_center[0], right_center[1],
                                     stereo_image.msg.left_utime, stereo_image.msg.right_utime,
                                     sim_pair[0].class_name, sim_pair[1].class_name, np.max(sim_pair[0].scores),
                                     np.max(sim_pair[1].scores))
                elif self.selected_box_source == 'KCF Box' and have_kcf:
                    pub_ext_target_3(self.lc, kcf_left[0], kcf_left[1], kcf_right[0], kcf_right[1],
                                     stereo_image.msg.left_utime, stereo_image.msg.right_utime,
                                     self.object_tracker.class_name, self.object_tracker.class_name,
                                     np.max(self.object_tracker.scores), np.max(self.object_tracker.scores))
                elif self.selected_box_source == '3D Tracker' or self.selected_box_source == 'AVG Box' and tracker_rba is not None:
                    tracker_left = None
                    tracker_right = None
                    class_name = 'None'
                    class_score = 0.0
                    if tracker_data.left_boxes.boxes is not None and len(tracker_data.left_boxes.boxes) > 0:
                        tracker_left = tracker_data.left_boxes.boxes[0]
                        class_name = tracker_data.left_boxes.boxes[0].class_name
                        class_score = np.max(tracker_data.left_boxes.boxes[0].scores)
                    if tracker_data.right_boxes.boxes is not None and len(tracker_data.right_boxes.boxes) > 0:
                        tracker_right = tracker_data.right_boxes.boxes[0]

                    if tracker_left is not None and tracker_right is not None:

                        left_center = [tracker_left.left + tracker_left.width/2,
                                       tracker_left.top + tracker_left.height/2]
                        right_center = [tracker_right.left + tracker_right.width / 2,
                                       tracker_right.top + tracker_right.height / 2]

                        pub_ext_target_3(self.lc, left_center[0], left_center[1], right_center[0], right_center[1],
                                         stereo_image.msg.left_utime, stereo_image.msg.right_utime,
                                         class_name, class_name, class_score, class_score)



        # Get the combined rba from multiple sources and limits
        fused_rba, valid_vec = self.calc_fused_rba(pub_ml, pub_kcf, pos_rba, tracker_rba, is_coasting=self.is_coasting, last_rba=self.last_pos, max_diff=self.max_target_step)

        # Ignore target position steps larger than a threshold
        fused_rba_unfiltered = fused_rba
        if fused_rba is not None:
            diff = pos_diff(self.last_pos, fused_rba)
            logger.info("Target Step: " + str(diff))
            if diff < self.max_target_step:
                self.last_pos = fused_rba
                self.filtered_pos.update(fused_rba)
                # replace the fused rba with the average
                if self.filtered_pos.index == self.filtered_pos.nsamples:
                    fused_rba = self.filtered_pos.get_latest()
            else:
                fused_rba = None

        # Handle world frame after selecting rba if available
        used_world_frame = False

        if self.use_world_frame:
            if fused_rba is None:
                position_wf, time_since_last_update = self.world_frame.get_wf_position()
                if time_since_last_update < self.world_frame_timeout:
                    used_world_frame = True
                    position_wf = position_wf / 1000.0
                    fused_rba = vehicle_location_to_rba(position_wf)
                    valid_vec = [True, True, True]
                    if self.missed_update > 0 and self.missed_update % 10 == 0:
                        if self.last_pos is not None:
                            logger.info('World Frame, diff = ' + "{:.3f}".format(pos_diff(fused_rba, self.last_pos)))
                        else:
                            logger.info('World Frame, diff = ' + "{:.3f}".format(pos_diff(fused_rba, fused_rba)))
                        #logger.info("Using World Frame: " + str(fused_rba))
                    #self.last_pos = fused_rba
            else:
                self.world_frame.update_wf_position(get_pos_in_vf(np.array(fused_rba)))



        if self.publish_MWT_TARGET and fused_rba is not None:
            publish_mwt_target(self.lc, left_center[0], left_center[1],
                               right_center[0], right_center[1],
                               fused_rba, valid_vec, chan=self.output_channel+"FILTERED")
            publish_mwt_target(self.lc, left_center[0], left_center[1],
                               right_center[0], right_center[1],
                               fused_rba_unfiltered, valid_vec, chan=self.output_channel)

        # Output vehicle and tracking data to CSV file if requested
        if self.vehicle_pos_output != '':
            self.save_stats(stereo_image.msg.left_utime, pos_ml, pos_kcf, tracker_rba, self.latest_depth,
                                    self.latest_yaw, pos_ml is not None, pos_kcf is not None, tracker_rba is not None)

        if len(self.stills_output) > 0 and int(10*time.time()) % 10 == 0:

            entry = BasicEntry(copy.copy(output_img[:, 0:int(stereo_image.img.shape[1]/2)]),
                               stereo_image.msg.left_utime,
                               self.latest_depth,
                               self.latest_yaw,
                               self.target_class_name,
                               tracker_rba,
                               self.depth_color,
                               self.box_tracker_color
                               )
            self.stills_exporter.add_entry(entry)


        # Draw ML-derived 3D position on image overlay if available
        #if pos_ml is not None:
        #    draw_position(stereo_image, pos_ml, err_ml, prefix='ML', y_offset=65, draw_color=self.ml_color)

        # Draw KCF-derived 3D position on image overlay if available
        #if pos_kcf is not None:
        #    draw_position(stereo_image, pos_kcf, err_kcf, prefix='KCF', y_offset=95, draw_color=self.kcf_color)

        # Draw 3DTracker-derived 3D position on image overlay if available
        #if tracker_rba is not None:
        #    draw_position(stereo_image, tracker_rba, self.tracker_pixel_error, prefix="Target Class: "+self.target_class_name, y_offset=70, draw_color=self.box_tracker_color)

        display_class_name = self.target_class_name
        if self.selected_box_source == 'AVG Box':
            display_class_name = "Target Class (AVG): " + tracker_data.left_boxes.boxes[0].class_name
        if self.selected_box_source == 'KCF Box':
            display_class_name = "Target Class (KCF): " + self.object_tracker.class_name
        if self.selected_box_source == 'ML Box':
            display_class_name = "Target Class (ML ): " + ml_target_class
        if self.selected_box_source == '3D Tracker':
            display_class_name = "Target Class (3DT): " + tracker_data.left_boxes.boxes[0].class_name

        if used_world_frame:
            display_class_name = "Inertial Frame"

        if fused_rba is not None:
            draw_position(stereo_image, fused_rba, self.tracker_pixel_error, prefix=display_class_name, y_offset=70, draw_color=self.good_tracker_color)


        # Draw UKF-derived 3D position on image overlay if available
        #if pos_rba is not None:
        #    draw_ukf_position(stereo_image, pos_rba, draw_color=self.ukf_color)

        return output_img

    def save_stats(self, utime, pos_ml, pos_kcf, pos_tracker, depth, yaw, ml_valid, kcf_valid, tracker_valid):

        def print_three(d,is_valid):
            line = '0.0,0.0,0.0,'
            if is_valid:
                line = ''
                for p in range(0, 3):
                    line += str(d[p]) + ","
            return line[0:-1]


        if self.vehicle_pos_output is not None:
            # create dir structure if needed
            if not os.path.exists(self.vehicle_pos_output):
                dirname = os.path.dirname(self.vehicle_pos_output)
                if len(dirname) > 0:
                    os.makedirs(dirname)

            line = ""
            line += str(utime) + ","
            line += print_three(pos_ml, ml_valid) + ","
            line += print_three(pos_kcf, kcf_valid) + ","
            line += print_three(pos_tracker, tracker_valid) + ","
            line += str(depth) + ","
            line += str(yaw) + ","
            line += str(int(ml_valid)) + ","
            line += str(int(kcf_valid)) + ","
            line += str(int(tracker_valid))

            if self.first_stats:
                with open(self.vehicle_pos_output, "w") as f:
                    f.write("Time,ml_r,ml_b,ml_a,kcf_r,kcf_b,kcf_a,trk_r,trk_b,trk_a,depth,yaw,ml_valid,kcf_valid,trk_valid\n")
                    f.write(line + '\n')
                self.first_stats = False
            else:
                with open(self.vehicle_pos_output, "a") as f:
                    f.write(line + '\n')

    def handle_mouse_input(self, pos):
        if self.last_stereo_img is not None:
            [left, right] = self.last_stereo_img.split_images()
            if pos[0] < 1032:
                left_box = (pos[0] - self.box_size_x/2, pos[1] - self.box_size_y/2, self.box_size_x, self.box_size_y)
                self.object_tracker.init_tracker(left.img, left_box, 0, self.object_tracker_type)
            else:
                right_box = (pos[0] - self.box_size_x/2 - 1032, pos[1] - self.box_size_y/2, self.box_size_x, self.box_size_y)
                self.object_tracker.init_tracker(right.img, right_box, 1, self.object_tracker_type)

    def handle_stereo_img(self, channel, data):

        msg = stereo_image_t.decode(data)

        # Patch for ms timestamp bug on March 4 data
        if len(str(msg.left_utime)) < 16:
            msg.left_utime = msg.left_utime * 1000
            msg.right_utime = msg.right_utime * 1000

        # Use this insert time to test latency between stereo image arrival and display
        # key = get_key(msg.left_utime)
        # self.insert_timestamps[key] = time.time()

        # save the latest timestamp for latency estimation
        self.latest_img_timestamp = msg.left_utime
        ml_img = StereoMLImage(msg)
        self.last_stereo_img = ml_img
        # We need to use both left and right keys here in case the detector
        # does not find boxes in one of the images.
        self.stereo_ml_img_buffer[ml_img.left_key] = ml_img
        self.stereo_ml_img_buffer[ml_img.right_key] = ml_img
        self.last_key = ml_img.left_key
        self.tracker_images.emit(ml_img)

    def process_left_stereo_box(self, stereo_ml_img, box):

        # Box must have score above threshold and have matching timestamp to image
        if box.class_name == self.target_class_name:
            box_color = self.ml_color
        else:
            box_color = self.box_color
        if np.max(box.scores) > self.score_threshold:
            draw_labeled_box(stereo_ml_img.boxed_img, box, box_color, box_color)
            stereo_ml_img.left_boxes.append(box)

    def process_right_stereo_box(self, stereo_ml_img, box):

        # Box must have score above threshold and have matching timestamp to image
        if box.class_name == self.target_class_name:
            box_color = self.ml_color
        else:
            box_color = self.box_color
        if np.max(box.scores) > self.score_threshold:
            box.left += stereo_ml_img.msg.width/2 # shift box for display
            draw_labeled_box(stereo_ml_img.boxed_img, box, box_color, box_color)
            stereo_ml_img.right_boxes.append(box)

    def handle_stereo_boxes(self, channel, data):

        stereo_boxes = stereo_bounding_box_t.decode(data)

        self.box_buf.append(stereo_boxes)

        matching_box = None
        ml_img = None

        for b in self.box_buf:

            # Assume we will always have a stereo image before the arrival of a box:
            left_key = get_key(b.left_boxes.utime)
            right_key = get_key(b.right_boxes.utime)

            if left_key in self.stereo_ml_img_buffer:
                ml_img = self.stereo_ml_img_buffer.pop(left_key)
            elif right_key in self.stereo_ml_img_buffer:
                ml_img = self.stereo_ml_img_buffer.pop(right_key)

            if ml_img is not None:
                matching_box = b
                break

        if ml_img is not None and matching_box is not None:
            # Use this insert time to test latency between stereo box arrival and display
            #key = get_key(ml_img.msg.left_utime)
            #self.insert_timestamps[key] = time.time()
            self.process_queue.append([ml_img, matching_box])
            self.box_buf.remove(matching_box)

    def handle_stereo_image_with_boxes(self, channel, data):
        image_and_boxes = stereo_image_with_boxes_t.decode(data)
        ml_img = StereoMLImage(image_and_boxes.stereo_image)
        self.tracker_images.emit(ml_img)
        self.process_queue.append([ml_img, image_and_boxes.stereo_boxes])

    def handle_tracker_boxes(self, channel, data):

        box_data = stereo_bounding_box_t.decode(data)

        self.latest_tracker_boxes = box_data
        if len(box_data.left_boxes.boxes) > 0:
            tracker_data = json.loads(box_data.left_boxes.boxes[0].aux)
            self.latest_tracker_pos = tracker_data['state']
            self.latest_tracker_time = time.time()


    def handle_depth(self, channel, data):

        depth = skapp_dval_t.decode(data).val

        self.depth_history.append(depth)
        self.latest_depth = depth
        if len(self.depth_history) > 10:
            self.depth_history.popleft()

    def handle_rpy(self, channel, data):

        rpy = phi_theta_psi_t.decode(data)

        self.rpy_history.append(rpy)
        self.yaw_history.append(rpy.psi)
        self.latest_rpy = rpy
        self.latest_yaw = rpy.psi
        if len(self.rpy_history) > 10:
            self.rpy_history.popleft()

    def handle_new_depth(self, channel, data):

        depth = mini_rov_depth_t.decode(data).depth

        self.depth_history.append(depth)
        self.latest_depth = depth
        if len(self.depth_history) > 10:
            self.depth_history.popleft()

    def handle_attitude(self, channel, data):

        yaw = mini_rov_attitude_t.decode(data).yaw_deg

        self.yaw_history.append(yaw)
        self.latest_yaw = yaw
        if len(self.yaw_history) > 10:
            self.yaw_history.popleft()

    def stop(self):
        self.stop_thread = True

    def run(self):

        usage_timer = time.time()

        while self.isRunning and not self.stop_thread:
            # If there is a pair of ml images for display, display them
            if len(self.process_queue) > 0:

                tmp = self.process_queue.popleft()
                trk_tmp = self.latest_tracker_boxes
                output_img = self.process_stereo_data(tmp, tracker_data=trk_tmp)
                self.stereoFrame.emit(output_img)

            else:

                # Update CPU and GPU usage each second
                if time.time() - usage_timer > 1:
                    #gpu_info = list_gpus.device_statuses()
                    self.cpu_usage = psutil.cpu_percent()
                    #self.gpu_usage = gpu_info[0]['utilization']
                    usage_timer = time.time()
                    # flush out older images
                    to_remove = []
                    for i in list(self.stereo_ml_img_buffer.keys()):
                        if (time.time() * 1000000 - self.stereo_ml_img_buffer[i].insert_timestamp) > 1000000:
                            to_remove.append(i)

                    for i in to_remove:
                        self.stereo_ml_img_buffer.pop(i)

            self.msleep(self.thread_sleep)

        if len(self.stills_output) > 0:
            self.stills_exporter.stop()

        logger.info('Handler thread stopped')

    # UI Helper functions below this line
    # -------------------------------------

    def set_pub_kcf(self, pub):
        self.pub_kcf = pub

    def set_box_pad(self, pad):
        self.box_pad = pad

    def set_score_threshold(self, score_threshold):
        logger.info(score_threshold)
        self.score_threshold = float(score_threshold) / 100

    def set_match_classes(self, should_match):
        logger.info(should_match)
        self.match_classes = should_match

    def set_save_kcf(self, save_kcf):
        logger.info(save_kcf)
        self.save_kcf_rois = save_kcf

    def set_match_method(self, method):
        self.match_method = method
        logger.info(method)

    def set_target_class(self, target_class):
        self.target_class_name = target_class
        logger.info(self.target_class_name)

    def set_box_deviation(self, dev):
        logger.info(dev)
        self.box_match_threshold = dev

    def set_box_x_size(self, val):
        self.box_size_x = val

    def set_box_y_size(self, val):
        self.box_size_y = val

    def set_target_source(self, src):
        logger.info(src)
        self.target_source = src

    def set_publish_target(self, pub):
        self.publish_MWT_TARGET = pub

    def set_publish_ext_target(self, pub):
        self.publish_ext_target_3 = pub

    def set_reinit_threshold(self, val):
        self.reinit_threshold = val

    def set_max_epipolar_error(self, val):
        self.max_epipolar_error = val

    def set_min_range(self, val):
        self.min_range = val

    def set_max_range(self, val):
        self.max_range = val

    def set_min_altitude(self, val):
        self.min_alt = val

    def set_max_altitude(self, val):
        self.max_alt = val

    def set_rate_limit(self, val):
        self.rate_limit = val

    def set_min_reinit_time(self, val):
        self.min_reinit_time = val
        self.object_tracker.min_reinit = val/1000 # in seconds

    def set_box_size_from_ml(self, val):
        self.box_size_from_ml = val

    def set_world_frame_timeout(self, val):
        self.world_frame_timeout = val




