# -*- coding: utf-8 -*-
"""
env_sensors.py -- code to log and publish sensors over LCM
Author: pldr
Copyright 2021  Monterey Bay Aquarium Research Institute
Distributed under MIT license. See license.txt for more information.

"""

import numpy as np
import lcm
import time
import socket
from loguru import logger
from pyqtgraph.Qt import QtCore, QtGui
from mwt.env_sensors_t import env_sensors_t

class EnvSensors(QtCore.QThread):


    def __init__(self):

        QtCore.QThread.__init__(self)

        self.stop_thread = False

        # Get the latest camera config values
        self.lc = lcm.LCM("udpm://239.255.76.67:7667?ttl=0")

        # sensor values
        self.temperature = 0.0
        self.humidity = 0.0
        self.last_update = time.time()

        logger.info("starting env sensors thread")


    def stop(self):
        self.stop_thread = True

    def parse_env_sensors(self, line):
        tokens = line.decode().split(",")
        if len(tokens) == 2 and tokens[0][0] == 'A':
            
            self.last_update = time.time()
            self.temperature = float(tokens[0][1:])/10.0
            self.humidity = float(tokens[1])/10.0
            
            msg = env_sensors_t()
            msg.utime = int(1000000*self.last_update)
            msg.temperature = self.temperature
            msg.humidity = self.humidity
            
            logger.info("Temp: " + str(self.temperature) + " C, Humidity: " + str(self.humidity) + " %")


    def run(self):

        UDP_IP = "192.168.1.18"
        UDP_PORT = 4001

        sock = socket.socket(socket.AF_INET, # Internet
                         socket.SOCK_DGRAM) # UDP
        sock.bind((UDP_IP, UDP_PORT))

        while self.isRunning and not self.stop_thread:
            try:
                sock.settimeout(2.0)
                data, addr = sock.recvfrom(32)
                self.parse_env_sensors(data)
            except:
                logger.warning('sensor socket timeout')
                self.parse_env_sensors(b'A340,30')

        