# -*- coding: utf-8 -*-
"""
image_sync.py -- Class for receiving left- and right-images from LCM and publishing combined stereo image
Author: pldr
Copyright 2020  Monterey Bay Aquarium Research Institute
Distributed under MIT license. See license.txt for more information.

"""

import lcm
import time
import numpy as np
from collections import deque
from mwt.stereo_image_t import stereo_image_t
from mwt.stereo_bounding_box_t import stereo_bounding_box_t
from mwt.stereo_image_with_boxes_t import stereo_image_with_boxes_t
from mwt.image_t import image_t

def stereo_frame_2_msg(msg, left, right):

    msg.left_utime = left.utime
    msg.right_utime = right.utime
    left_frame = msg_2_frame(left)
    right_frame = msg_2_frame(right)
    stereo_img = np.hstack((left_frame, right_frame))
    #print(stereo_img.shape)
    msg.width = stereo_img.shape[1]
    msg.height = stereo_img.shape[0]
    msg.data = stereo_img.tobytes()

    msg.size = len(msg.data)
    if len(stereo_img.shape) > 2:
        msg.pixelformat = image_t.PIXEL_FORMAT_RGB
    else:
        msg.pixelformat = image_t.PIXEL_FORMAT_GRAY
    return msg, stereo_img

def msg_2_frame(msg):

    frame = np.frombuffer(msg.data, dtype=np.uint8)
    # a few conditions here for image types, many more are possible
    if msg.pixelformat == image_t.PIXEL_FORMAT_RGB:
        bytes_per_pixel = 3
    if msg.pixelformat == image_t.PIXEL_FORMAT_GRAY:
        bytes_per_pixel = 1
    frame = np.squeeze(np.reshape(frame, newshape=(msg.height, msg.width, bytes_per_pixel)))

    return frame

class StereoSync:

    def __init__(self, max_wait=100):
        self.box_buf = deque()
        self.image_buf = deque()
        self.box_channel = 'BOX_STEREO'
        self.image_channel = 'MWT_STEREO_IMAGE'
        self.lc = lcm.LCM("udpm://239.255.76.67:7667?ttl=0")

    def __del__(self):
        self.wait()

    def stop(self):
        self.stop_thread = True

    def handle_image(self, channel, data):
        self.image_buf.append(stereo_image_t.decode(data))

    def handle_box(self, channel, data):
        box = stereo_bounding_box_t.decode(data)
        to_remove = []
        for i in self.image_buf:
            if np.abs(box.left_boxes.utime - i.left_utime) < 100000:
                stereo_msg = stereo_image_with_boxes_t()
                stereo_msg.stereo_boxes = box
                stereo_msg.stereo_image = i
                self.lc.publish('STEREO_IMAGE_WITH_BOXES', stereo_msg.encode())
                self.image_buf.remove(i)
                return
            # Check for old images
            if box.left_boxes.utime - i.left_utime > 1000000:
                to_remove.append(i)

        # remove stale images to prevent memory leaks
        for i in to_remove:
            self.image_buf.remove(i)



    def run(self):

        # LCM on local machine only
        # Image handles for ML
        self.lc.subscribe(self.box_channel, self.handle_box)
        self.lc.subscribe(self.image_channel, self.handle_image)

        try:
            while True:
                self.lc.handle_timeout(1000)

        except KeyboardInterrupt:
            pass

if __name__=="__main__":

    import sys

    image_sync = StereoSync()
    image_sync.run()


