# -*- coding: utf-8 -*-
"""
boxview.py -- A python app to display stereo images and bounding boxes for ML-Tracking
Author: pldr
Copyright 2021  Monterey Bay Aquarium Research Institute
Distributed under MIT license. See license.txt for more information.

The app uses LCM to listen for images from cameras and also machine learning output in the form
of BOUNDING_BOX messages. It tries to display these synchronously while minimising the latency between the
reception of the image and the display and output of data.

The "heavy lifting" is done in libs/data_handler.py and libs/supervisor.py

"""


import os
import lcm
import cv2
import time
import configparser
import pyqtgraph as pg
from collections import deque
from loguru import logger
from libs.env_sensors import EnvSensors
from libs.camera_control import MantaControl
from libs.dspl import set_light_output
from libs.supervisor import Supervisor
from mwt.ml_cfg_t import ml_cfg_t
from mwt.supervisor_cfg_t import supervisor_cfg_t
from mwt.mwt_search_status_t import mwt_search_status_t
from mwt.mwt_target_t import mwt_target_t
from libs.lcm_tools import LCMManager
from pyqtgraph.Qt import QtCore, QtGui, QtWidgets
from libs.input_arguments import parse_args
from config.gui_state import GuiSave, GuiRestore


pg.mkQApp()

## Define main window class from template
path = os.path.dirname(os.path.abspath(__file__))
uiFile = os.path.join(path, 'boxview.ui')
WindowTemplate, TemplateBaseClass = pg.Qt.loadUiType(uiFile)

class MainWindow(TemplateBaseClass):

    def __init__(self, argv):

        TemplateBaseClass.__init__(self)
        self.setWindowTitle('Boxview - Python - Qt')

        args = parse_args()

        # the name of the CV window
        self.cv_window_name = 'ML-Tracking Output'

        # flag for when window is displayed
        self.window_enabled = False

        # window scaling
        self.window_scaling = 1.15

        # Create the main window
        self.ui = WindowTemplate()
        self.ui.setupUi(self)

        self.video_buffer = deque()

        # setup and connect the lcm manager
        self.lcm_manager = LCMManager(args)
        self.lcm_manager.handler.stereoFrame.connect(self.drawStereoFrames)
        self.lcm_manager.start()

        # LCM
        self.lc = lcm.LCM("udpm://239.255.76.67:7667?ttl=0")

        # camera
        self.camera_control = MantaControl()
        self.camera_control.start()

        # Sensors
        #self.env_sensors = EnvSensors()
        #self.env_sensors.start()

        # supervisor
        self.supervisor = Supervisor(args)
        self.supervisor.start()

        # ml config messages

        # read the ml-model command into a string
        self.ml_model_command = "other"
        with open('../scripts/run_ml_low_latency', 'r') as f:
            self.ml_model_command = f.read().rstrip('\n')


        self.ml_cfg_timer = QtCore.QTimer()
        self.ml_cfg_timer.timeout.connect(self.send_ml_cfg)
        self.ml_cfg_timer.start(1000)

        self.supervisor_cfg_timer = QtCore.QTimer()
        self.supervisor_cfg_timer.timeout.connect(self.send_supervisor_cfg)
        self.supervisor_cfg_timer.start(1000)

        # set defaults from args
        self.lcm_manager.handler.set_score_threshold(self.ui.scoreSlider.value())
        self.lcm_manager.handler.match_methods = [
            self.ui.matchMethodComboBox.itemText(i) for i in range(self.ui.matchMethodComboBox.count())
        ]
        self.lcm_manager.handler.set_match_method(self.ui.matchMethodComboBox.currentText())
        self.lcm_manager.handler.set_box_pad(self.ui.boxPaddingSlider.value())

        # populate target class and trackable class from labels.txt
        with open(os.path.join('config', 'labels.txt')) as f:

            # Populate the class list
            lines = f.readlines()
            self.ui.targetClassComboBox.clear()
            rows_per_col = 4
            for i, line in enumerate(sorted(lines)):
                self.ui.targetClassComboBox.addItem(line.lower().strip('\t\r\n'))
                col_ind = int(i / rows_per_col)
                item = QtWidgets.QTableWidgetItem(line.lower().strip('\t\r\n'))
                self.ui.trackableClassList.setItem(i % rows_per_col, col_ind, item)

        # Set the target class in supervisor

        # Populate commands
        self.command_names = []
        with open(os.path.join('config', 'controller_cmds.txt')) as f:
            lines = f.readlines()
            for line in sorted(lines):
                self.command_names.append(line.strip('\t\r\n'))
        self.command_completer = QtWidgets.QCompleter(self.command_names)
        #self.ui.cmdEdit.setCompleter(self.command_completer)

        # load command sets
        self.command_sets = configparser.ConfigParser()
        self.command_sets.read(os.path.join('config', 'command_sets.ini'))
        logger.info(self.command_sets)

        self.video_output = args.video_output
        if len(args.video_output) > 0:
            fourcc = cv2.VideoWriter_fourcc('h', '2', '6', '4')
            self.video_out = cv2.VideoWriter(os.path.join(args.video_output,'boxview-video-'+str(int(time.time()))+'.mp4'), fourcc, 10.0, (2064, 772))

        # restore previous GUI settings
        GuiRestore(self, QtCore.QSettings(os.path.join('config', 'gui.ini'), QtCore.QSettings.IniFormat))


        self.ui.exposureSpinBox.setValue(self.camera_control.exposure)
        self.ui.gainSpinBox.setValue(self.camera_control.gain)
        self.ui.fpsSpinBox.setValue(self.camera_control.framerate)

        # connect slots
        self.ui.trackableClassList.itemChanged.connect(self.updateTrackableClasses)
        self.ui.scoreSpinBox.valueChanged.connect(self.lcm_manager.handler.set_score_threshold)
        self.ui.matchMethodComboBox.currentTextChanged.connect(self.lcm_manager.handler.set_match_method)
        self.ui.boxMatchSpinBox.valueChanged.connect(self.lcm_manager.handler.set_box_deviation)
        self.ui.targetClassComboBox.currentTextChanged.connect(self.updateTargetClass)
        self.ui.boxPaddingSlider.valueChanged.connect(self.lcm_manager.handler.set_box_pad)
        self.camera_control.cam_cfg.connect(self.update_cam_cfg)
        self.ui.exposureSpinBox.valueChanged.connect(self.updateCameraConfig)
        self.ui.gainSpinBox.valueChanged.connect(self.updateCameraConfig)
        self.ui.fpsSpinBox.valueChanged.connect(self.updateCameraConfig)
        self.ui.boxSizeXSpinBox.valueChanged.connect(self.lcm_manager.handler.set_box_x_size)
        self.ui.boxSizeYSpinBox.valueChanged.connect(self.lcm_manager.handler.set_box_y_size)
        self.ui.targetBoxSourceComboBox.currentTextChanged.connect(self.lcm_manager.handler.set_target_source)
        self.ui.publishEXTTarget.stateChanged.connect(self.lcm_manager.handler.set_publish_ext_target)
        self.ui.publishMWTTarget.stateChanged.connect(self.lcm_manager.handler.set_publish_target)
        self.ui.reinitThresholdSpinBox.valueChanged.connect(self.lcm_manager.handler.set_reinit_threshold)
        self.ui.maxEpipolarErrorSpinBox.valueChanged.connect(self.lcm_manager.handler.set_max_epipolar_error)
        self.ui.minRangeSpinBox.valueChanged.connect(self.lcm_manager.handler.set_min_range)
        self.ui.maxRangeSpinBox.valueChanged.connect(self.lcm_manager.handler.set_max_range)
        self.ui.minAltitudeSpinBox.valueChanged.connect(self.lcm_manager.handler.set_min_altitude)
        self.ui.minAltitudeSpinBox.valueChanged.connect(self.lcm_manager.handler.set_min_altitude)
        self.ui.rateLimitSpinBox.valueChanged.connect(self.lcm_manager.handler.set_rate_limit)
        self.ui.boxSizeFromMLCheckbox.stateChanged.connect(self.lcm_manager.handler.set_box_size_from_ml)
        self.ui.minReinitTimeSpinBox.valueChanged.connect(self.lcm_manager.handler.set_min_reinit_time)
        self.ui.worldFrameTimeoutSpinBox.valueChanged.connect(self.lcm_manager.handler.set_world_frame_timeout)

        #timeouts
        self.ui.resumeSearchTimeoutSpinBox.valueChanged.connect(self.updateTimeouts)
        self.ui.acquireTimeoutSpinBox.valueChanged.connect(self.updateTimeouts)
        self.ui.trackTimeoutSpinBox.valueChanged.connect(self.updateTimeouts)
        self.ui.trackDurationSpinBox.valueChanged.connect(self.updateTimeouts)

        #class label filtering
        self.ui.classLabelHistorySpinBox.valueChanged.connect(self.updateControlSettings)
        self.ui.ignoreClassLabelInTrack.toggled.connect(self.updateControlSettings)

        self.ui.maxTargetStepSpinBox.valueChanged.connect(self.updateThresholds)
        self.ui.minIOUSpinBox.valueChanged.connect(self.updateThresholds)
        self.ui.minTrackScoreSpinBox.valueChanged.connect(self.updateThresholds)

        self.supervisor.search_stat_sig.connect(self.update_search_stat)
        self.supervisor.vehicle_depth.connect(self.update_depth)
        self.supervisor.vehicle_yaw.connect(self.update_heading)
        self.supervisor.state_change_sig.connect(self.update_state)
        self.supervisor.mwt_target_sig.connect(self.update_mwt_target)
        self.supervisor.resume_search_sig.connect(self.reset_search)
        
        # set the startup state to idle
        self.supervisor.current_state = 0
        self.supervisor.target_class = self.ui.targetClassComboBox.currentText()
        self.supervisor.z_trim = self.ui.trimSpinBox.value()
        self.ui.idleButton.setChecked(True)

        self.state_buttons = [self.ui.idleButton, self.ui.searchButton, self.ui.acquireButton, self.ui.trackButton]
        

        # Light control
        self.ui.lightDimmingSpinBox.valueChanged.connect(self.updateLightOutput)
        # self.ui.lightDimmingSlider.sliderReleased.connect(self.updateLightOutput)

        # Acquire in mode 3 control
        # when True, use the station keeping mode, when false use audo head/depth
        self.supervisor.acquireInMode3 = self.ui.acquireInMode3.isChecked()
        self.ui.acquireInMode3.toggled.connect(self.updateAcquireInMode3)

        self.latest_mwt_target = None

        # Supervisor settings

        # Supervisor connections
        self.ui.idleButton.toggled.connect(self.handleIdleState)
        self.ui.searchButton.toggled.connect(self.handleSearchState)
        self.ui.acquireButton.toggled.connect(self.handleAcquireState)
        self.ui.trackButton.toggled.connect(self.handleTrackState)

        # Enable manual control by default when user clicks a button
        self.ui.idleButton.clicked.connect(self.setToManual)
        self.ui.searchButton.clicked.connect(self.setToManual)
        self.ui.acquireButton.clicked.connect(self.setToManual)
        self.ui.trackButton.clicked.connect(self.setToManual)

        self.ui.yEffortSpinBox.valueChanged.connect(self.updateYEffort)
        self.ui.xEffortSpinBox.valueChanged.connect(self.updateXEffort)
        self.ui.trimSpinBox.valueChanged.connect(self.updateZTrim)
        self.ui.depthSpinBox.valueChanged.connect(self.updateDepthCmd)
        self.ui.headingSpinBox.valueChanged.connect(self.updateHeadingCmd)

        self.ui.controlModeComboBox.currentTextChanged.connect(self.updateControlMode)

        self.ui.trimIncSpinBox.valueChanged.connect(self.updateIncrements)
        self.ui.depthIncSpinBox.valueChanged.connect(self.updateIncrements)
        self.ui.headingIncSpinBox.valueChanged.connect(self.updateIncrements)
        self.ui.xEffortIncSpinBox.valueChanged.connect(self.updateIncrements)
        self.ui.yEffortIncSpinBox.valueChanged.connect(self.updateIncrements)

        self.ui.maxTargetJump.valueChanged.connect(self.updateControlSettings)
        self.ui.acquireModeComboBox.currentText()
        self.ui.trackModeComboBox.currentText()

        self.ui.useWorldFrame.toggled.connect(self.updateControlSettings)

        self.ui.zSearchSlewRate.valueChanged.connect(self.updateControlSettings)
        self.ui.rangeSetpointSpinBox.valueChanged.connect(self.updateControlSettings)
        self.ui.bearingSetpointSpinBox.valueChanged.connect(self.updateControlSettings)
        self.ui.verticalSetpointSpinBox.valueChanged.connect(self.updateControlSettings)
        self.ui.rangeDeadbandSpinBox.valueChanged.connect(self.updateControlSettings)
        self.ui.zDeadbandSpinBox.valueChanged.connect(self.updateControlSettings)
        self.ui.bearingDeadbandSpinBox.valueChanged.connect(self.updateControlSettings)

        self.ui.acquireMaxRangeSpinBox.valueChanged.connect(self.updateControlSettings)
        self.ui.acquireMaxBearingSpinBox.valueChanged.connect(self.updateControlSettings)
        self.ui.acquireMaxZSpinBox.valueChanged.connect(self.updateControlSettings)

        self.ui.trackMaxRangeSpinBox.valueChanged.connect(self.updateControlSettings)
        self.ui.trackMaxBearingSpinBox.valueChanged.connect(self.updateControlSettings)
        self.ui.trackMaxZSpinBox.valueChanged.connect(self.updateControlSettings)

        self.ui.acquireRangeGainSpinBox.valueChanged.connect(self.updateControlSettings)
        self.ui.acquireBearingGainSpinBox.valueChanged.connect(self.updateControlSettings)
        self.ui.acquireZGainSpinBox.valueChanged.connect(self.updateControlSettings)

        self.ui.trackRangeGainSpinBox.valueChanged.connect(self.updateControlSettings)
        self.ui.trackBearingGainSpinBox.valueChanged.connect(self.updateControlSettings)
        self.ui.trackZGainSpinBox.valueChanged.connect(self.updateControlSettings)

        self.ui.acquireMinRangeSpinBox.valueChanged.connect(self.updateControlSettings)
        self.ui.trackMinRangeSpinBox.valueChanged.connect(self.updateControlSettings)

        # set defaults for data handler
        self.lcm_manager.handler.set_score_threshold(self.ui.scoreSpinBox.value())
        self.lcm_manager.handler.set_target_class(self.ui.targetClassComboBox.currentText())
        self.lcm_manager.handler.set_publish_ext_target(self.ui.publishEXTTarget.isChecked())
        self.lcm_manager.handler.set_publish_target(self.ui.publishMWTTarget.isChecked())
        self.lcm_manager.handler.set_target_source(self.ui.targetBoxSourceComboBox.currentText())
        self.lcm_manager.handler.set_box_y_size(self.ui.boxSizeYSpinBox.value())
        self.lcm_manager.handler.set_box_x_size(self.ui.boxSizeXSpinBox.value())
        self.lcm_manager.handler.set_box_pad(self.ui.boxPaddingSlider.value())
        self.lcm_manager.handler.set_max_epipolar_error(self.ui.maxEpipolarErrorSlider.value())
        self.lcm_manager.handler.set_min_range(self.ui.minRangeSpinBox.value())
        self.lcm_manager.handler.set_max_range(self.ui.maxRangeSpinBox.value())
        self.lcm_manager.handler.set_min_altitude(self.ui.minAltitudeSpinBox.value())
        self.lcm_manager.handler.set_max_altitude(self.ui.maxAltitudeSpinBox.value())
        self.lcm_manager.handler.set_rate_limit(self.ui.rateLimitSpinBox.value())
        self.lcm_manager.handler.set_box_size_from_ml(self.ui.boxSizeFromMLCheckbox.isChecked())
        self.lcm_manager.handler.set_min_reinit_time(self.ui.minReinitTimeSpinBox.value())
        self.lcm_manager.handler.set_world_frame_timeout(self.ui.worldFrameTimeoutSpinBox.value())

        # sync supervisor and GUI elements with values from config
        self.updateThresholds()
        self.updateControlMode()
        self.updateControlSettings()
        self.updateIncrements()
        self.updateTimeouts()

        # Show the main window
        self.show()

    

    def updateTimeouts(self):
        self.supervisor.search_timeout = self.ui.resumeSearchTimeoutSpinBox.value()
        self.supervisor.acquire_timeout = self.ui.acquireTimeoutSpinBox.value()
        self.supervisor.track_timeout = self.ui.trackTimeoutSpinBox.value()
        self.supervisor.track_duration = self.ui.trackDurationSpinBox.value()

    def reset_search(self):
        self.ui.depthSpinBox.setValue(self.ui.searchDepthSpinBox.value())
        self.ui.headingSpinBox.setValue(self.ui.searchHeadingSpinBox.value())
        self.updateXEffort()

    def setToManual(self):
        self.supervisor.auto_control = False
        self.ui.controlModeComboBox.setCurrentText("Manual")

    def updateAcquireInMode3(self):
        self.supervisor.acquireInMode3 = self.ui.acquireInMode3.isChecked()

    def updateLightOutput(self):
        dimming = self.ui.lightDimmingSpinBox.value()
        set_light_output(dimming)

    def updateThresholds(self):
        self.lcm_manager.handler.min_track_score = self.ui.minTrackScoreSpinBox.value()
        self.lcm_manager.handler.min_track_iou = self.ui.minIOUSpinBox.value()
        self.lcm_manager.handler.max_target_step = self.ui.maxTargetStepSpinBox.value()

    def get_trackable_classes(self):
        self.supervisor.trackable_list = []
        self.lcm_manager.handler.class_list = []
        for item in self.ui.trackableClassList.selectedItems():
            #selected_classes.append(item.text())
            self.supervisor.trackable_list.append(item.text())
            self.lcm_manager.handler.class_list.append(item.text())
        #logger.info(selected_classes)
        #for i in range(0,self.ui.trackableClassList.count()):
        #    item = self.ui.trackableClassList.item(i)
        #    if item.checkState():
        #        selected_classes.append(item.text())
        return self.supervisor.trackable_list

    def updateTrackableClasses(self):
        self.get_trackable_classes()
        #selected_classes = self.get_trackable_classes()
        #self.supervisor.trackable_list = selected_classes

    def updateControlSettings(self):

        self.lcm_manager.handler.max_diff = self.ui.maxTargetJump.value()

        self.lcm_manager.handler.use_world_frame = self.ui.useWorldFrame.isChecked()

        self.supervisor.acquire_mode = self.ui.acquireModeComboBox.currentText()
        self.supervisor.track_mode = self.ui.trackModeComboBox.currentText()

        self.supervisor.ignore_class_label_in_track = self.ui.ignoreClassLabelInTrack.isChecked()
        self.supervisor.target_class_label_history_length = self.ui.classLabelHistorySpinBox.value()
        self.supervisor.target_class_label_history = []

        self.supervisor.range_setpoint = self.ui.rangeSetpointSpinBox.value()
        self.supervisor.bearing_setpoint = self.ui.bearingSetpointSpinBox.value()
        self.supervisor.vertical_setpoint = self.ui.verticalSetpointSpinBox.value()
        self.supervisor.range_deadband = self.ui.rangeDeadbandSpinBox.value()
        self.supervisor.z_deadband = self.ui.zDeadbandSpinBox.value()
        self.supervisor.bearing_deadband = self.ui.bearingDeadbandSpinBox.value()

        self.supervisor.min_acquire_range = self.ui.acquireMinRangeSpinBox.value()
        self.supervisor.max_aquire_range = self.ui.acquireMaxRangeSpinBox.value()
        self.supervisor.max_aquire_bearing = self.ui.acquireMaxBearingSpinBox.value()
        self.max_aquire_z = self.ui.acquireMaxZSpinBox.value()

        self.supervisor.min_track_range = self.ui.trackMinRangeSpinBox.value()
        self.supervisor.max_track_range = self.ui.trackMaxRangeSpinBox.value()
        self.supervisor.max_track_bearing = self.ui.trackMaxBearingSpinBox.value()
        self.supervisor.max_track_z = self.ui.trackMaxZSpinBox.value()

        self.supervisor.acquire_range_gain = self.ui.acquireRangeGainSpinBox.value()
        self.supervisor.acquire_bearing_gain = self.ui.acquireBearingGainSpinBox.value()
        self.supervisor.acquire_z_gain = self.ui.acquireZGainSpinBox.value()

        self.supervisor.track_range_gain = self.ui.trackRangeGainSpinBox.value()
        self.supervisor.track_bearing_gain = self.ui.trackBearingGainSpinBox.value()
        self.supervisor.track_z_gain =  self.ui.trackZGainSpinBox.value()

        # update the slew rate setting on the controller
        self.supervisor.callContFunc("zSearchSlewRate", self.ui.zSearchSlewRate.value())

    def updateIncrements(self):

        self.ui.trimSpinBox.setSingleStep(self.ui.trimIncSpinBox.value())
        self.ui.depthSpinBox.setSingleStep(self.ui.depthIncSpinBox.value())
        self.ui.headingSpinBox.setSingleStep(self.ui.headingIncSpinBox.value())
        self.ui.xEffortSpinBox.setSingleStep(self.ui.xEffortIncSpinBox.value())
        self.ui.yEffortSpinBox.setSingleStep(self.ui.yEffortIncSpinBox.value())

    def updateZTrim(self, trim):
        self.supervisor.set_z_trim(trim)

    def updateTargetClass(self, target_class):
        self.lcm_manager.handler.set_target_class(target_class)
        self.supervisor.target_class = target_class

    def updateControlMode(self):
        self.supervisor.auto_control = self.ui.controlModeComboBox.currentText() == 'Automatic'


    def handleState(self,state_name):
        #self.ui.cmdList.setPlainText(self.command_sets['CommandSets'][state_name].replace(',','\n'))
        if state_name == 'acquire' and self.supervisor.acquireInMode3:
            # If we want to acquire in mode3 we just want to send the mode(3) command
            self.supervisor.sendContCmds(self.command_sets['CommandSets']['track'].split(','))
        else:
            self.supervisor.sendContCmds(self.command_sets['CommandSets'][state_name].split(','))
        #logger.info(self.command_sets['CommandSets'][state_name].split(','))

    def updateDepthCmd(self):
        cmd = 'zSearchMoveAbs(' + str(self.ui.depthSpinBox.value()) + ')'
        self.supervisor.sendContCmd(cmd)
        self.ui.depthSpinBox.clearFocus()

    def updateHeadingCmd(self):
        new_heading = self.ui.headingSpinBox.value()
        #if new_heading <= self.supervisor.latest_rpy[2]:
        #    self.supervisor.sendContCmd('yawSearchMoveAbsNeg(' + str(abs(new_heading)) + ')')
        #else:
        #    self.supervisor.sendContCmd('yawSearchMoveAbsPos(' + str(abs(new_heading)) + ')')

        # If new heading triggers a wrap around update the gui element and the clalback will fire
        if new_heading > 360:
            new_heading = new_heading - 360
            self.ui.headingSpinBox.setValue(new_heading)
            return
        elif new_heading < 0:
            new_heading = 360 + new_heading
            self.ui.headingSpinBox.setValue(new_heading)
            return

        # Need to pause the controller here to avoid winding up or down the heading traj
        # generator. When we update the heading goal the depth traj will still be paused 
        # so we need to send that command as well

        if new_heading < self.supervisor.latest_rpy[2] - 0.5:
            self.supervisor.sendContCmd('pause')
            self.supervisor.sendContCmd('yawSearchMoveAbsNeg(' + str(abs(new_heading)) + ')')
            cmd = 'zSearchMoveAbs(' + str(self.ui.depthSpinBox.value()) + ')'
            self.supervisor.sendContCmd(cmd)
        elif new_heading > self.supervisor.latest_rpy[2] + 0.5:
            self.supervisor.sendContCmd('pause')
            self.supervisor.sendContCmd('yawSearchMoveAbsPos(' + str(abs(new_heading)) + ')')
            cmd = 'zSearchMoveAbs(' + str(self.ui.depthSpinBox.value()) + ')'
            self.supervisor.sendContCmd(cmd)

        #rel_move = new_heading - self.supervisor.latest_rpy[2]
        #self.supervisor.sendContCmd('yawSearchMoveRel(' + str((rel_move)) + ')')
        
        self.ui.headingSpinBox.clearFocus()

    def update_state(self, new_state):
        self.state_buttons[new_state].setChecked(True)

    def update_depth(self,new_depth):
        self.ui.depthIndicator.setValue(new_depth)

    def update_heading(self,new_heading):
        self.ui.headingIndicator.setValue(new_heading)

    def handleIdleState(self):
        if self.ui.idleButton.isChecked():
            self.supervisor.current_state = 0
            self.handleState('idle')
            self.lcm_manager.handler.use_world_frame = False
            self.ui.useWorldFrame.setChecked(False)
            #self.ui.depthSpinBox.setValue(self.supervisor.latest_depth)
            #self.ui.headingSpinBox.setValue(self.supervisor.latest_rpy[2])

    def handleSearchState(self):
        if self.ui.searchButton.isChecked():
            self.handleState('search')
            #self.setContVar('xSearchEffort', self.ui.xEffortSpinBox.value())
            #self.setContVar('ySearchEffort', self.ui.yEffortSpinBox.value())
            self.lcm_manager.handler.use_world_frame = False
            self.ui.useWorldFrame.setChecked(False)
            self.supervisor.current_state = 1

            self.ui.depthSpinBox.setValue(self.supervisor.latest_depth)
            self.ui.headingSpinBox.setValue(self.supervisor.latest_rpy[2])
            if not self.supervisor.resume_search_flag:
                self.updateXEffort()
    
    def handleAcquireState(self):
        if self.ui.acquireButton.isChecked():
            self.supervisor.current_state = 2
            self.lcm_manager.handler.use_world_frame = False
            self.ui.useWorldFrame.setChecked(False)
            self.handleState('acquire')

    def handleTrackState(self):
        if self.ui.trackButton.isChecked():
            self.supervisor.current_state = 3
            self.lcm_manager.handler.use_world_frame = True
            self.ui.useWorldFrame.setChecked(True)
            ind = self.ui.targetClassComboBox.findText(self.supervisor.latest_ext_target.left_class_name)
            logger.info(ind)
            logger.info(self.supervisor.latest_ext_target.left_class_name)
            if ind != -1:
                self.ui.targetClassComboBox.setCurrentIndex(ind)
            
            self.handleState('track')
    

    def updateXEffort(self):
        self.supervisor.sendContCmd('xSearchEffort=' + str(self.ui.xEffortSpinBox.value()))
        self.supervisor.latest_search_xeff = self.ui.xEffortSpinBox.value()

    def updateYEffort(self):
        self.supervisor.sendContCmd('ySearchEffort=' + str(self.ui.yEffortSpinBox.value()))

    def updateCameraConfig(self):
        self.camera_control.updateCameraConfig(
            self.ui.exposureSpinBox.value(),
            self.ui.gainSpinBox.value(),
            self.ui.fpsSpinBox.value(),
        )

    def update_cam_cfg(self, msg):

        self.ui.exposureSpinBox.setValue(msg.exposure)
        self.ui.gainSpinBox.setValue(msg.gain)
        self.ui.fpsSpinBox.setValue(msg.framerate)

    def update_search_stat(self, data):
        stat = mwt_search_status_t.decode(data)
        
        # Populate the UI 
        if stat.control_mode == 1:
            self.ui.controlModeIdle.setChecked(True)
        elif stat.control_mode == 2:
            self.ui.controlModeSearch.setChecked(True)
        elif stat.control_mode == 3:
            self.ui.controlModeTrack.setChecked(True)

        if stat.x_mode == 1:
            self.ui.xEffortButton.setChecked(True)
        else:
            self.ui.xEffortButton.setChecked(False)
        self.ui.xEffortCmd.setValue(stat.x_effort_cmd)
        self.ui.xEffortOutput.setValue(stat.x_output)

        if stat.y_mode == 1:
            self.ui.yEffortButton.setChecked(True)
        else:
            self.ui.yEffortButton.setChecked(False)
        self.ui.yEffortCmd.setValue(stat.y_effort_cmd)
        self.ui.yEffortOutput.setValue(stat.y_output)

        if stat.z_mode == 1:
            self.ui.zEffortButton.setChecked(True)
        elif stat.z_mode == 2:
            self.ui.zDepthButton.setChecked(True)
        self.ui.zEffortCmd.setValue(stat.z_set_point)
        self.ui.zEffortOutput.setValue(stat.z_output)

        if stat.yaw_mode == 1:
            self.ui.yawEffortButton.setChecked(True)
        elif stat.yaw_mode == 2:
            self.ui.yawDepthButton.setChecked(True)
        self.ui.yawEffortCmd.setValue(stat.yaw_set_point)
        self.ui.yawEffortOutput.setValue(stat.yaw_output)

    def update_mwt_target(self, data):
        self.latest_mwt_target = mwt_target_t.decode(data)
        self.ui.rangeSpinBox.setValue(self.latest_mwt_target.range_meters)
        self.ui.bearingSpinBox.setValue(self.latest_mwt_target.bearing_degrees)
        self.ui.zSpinBox.setValue(self.latest_mwt_target.z_meters)

    def send_ml_cfg(self):
        msg = ml_cfg_t()
        msg.score_threshold = self.ui.scoreSlider.value()
        msg.box_deviation = self.ui.boxMatchSlider.value()
        msg.max_range = self.ui.maxRangeSpinBox.value()
        msg.min_range = self.ui.minRangeSpinBox.value()
        msg.max_alt = self.ui.maxAltitudeSpinBox.value()
        msg.min_alt = self.ui.minAltitudeSpinBox.value()
        msg.box_combiner = self.ui.matchMethodComboBox.currentText()
        msg.target_source = self.ui.targetBoxSourceComboBox.currentText()
        msg.target_class = self.ui.targetClassComboBox.currentText()
        msg.tracker_type = self.ui.trackerComboBox.currentText()
        msg.max_epipolar_error = int(self.ui.maxEpipolarErrorSpinBox.value())
        msg.box_size_x = self.ui.boxSizeXSpinBox.value()
        msg.box_size_y = self.ui.boxSizeYSpinBox.value()
        msg.box_padding = self.ui.boxPaddingSpinBox.value()
        msg.box_size_from_ml = int(self.ui.boxSizeFromMLCheckbox.isChecked())
        msg.publish_ext = int(self.ui.publishEXTTarget.isChecked())
        msg.publish_mwt = int(self.ui.publishMWTTarget.isChecked())
        msg.reinit_threshold = int(self.ui.reinitThresholdSpinBox.value()) # legacy double value convert to int
        msg.rate_limit = self.ui.rateLimitSpinBox.value()
        msg.min_reinit_time = int(self.ui.minReinitTimeSpinBox.value())
        msg.use_world_frame = int(self.ui.useWorldFrame.isChecked())
        msg.world_frame_timeout = int(self.ui.worldFrameTimeoutSpinBox.value())

        msg.model_name = self.ml_model_command
        self.lc.publish('ML_CFG', msg.encode())

    def send_supervisor_cfg(self):
        msg = supervisor_cfg_t()

        msg.max_target_jump = self.ui.maxTargetJump.value()
        msg.max_target_step = self.ui.maxTargetStepSpinBox.value()
        msg.min_track_iou = self.ui.minIOUSpinBox.value()
        msg.min_track_score = self.ui.minTrackScoreSpinBox.value()
        msg.range_setpoint = self.ui.rangeSetpointSpinBox.value()
        msg.range_deadband = self.ui.rangeDeadbandSpinBox.value()
        msg.bearing_deadband = self.ui.bearingDeadbandSpinBox.value()
        msg.z_deadband = self.ui.zDeadbandSpinBox.value()
        msg.acquire_max_range = self.ui.acquireMaxRangeSpinBox.value()
        msg.acquire_max_bearing = self.ui.acquireMaxBearingSpinBox.value()
        msg.acquire_max_z = self.ui.acquireMaxZSpinBox.value()
        msg.acquire_range_gain = self.ui.acquireRangeGainSpinBox.value()
        msg.acquire_bearing_gain = self.ui.acquireBearingGainSpinBox.value()
        msg.acquire_z_gain = self.ui.acquireZGainSpinBox.value()
        msg.bearing_setpoint = self.ui.bearingSetpointSpinBox.value()
        msg.vertical_setpoint = self.ui.verticalSetpointSpinBox.value()
        msg.search_timeout = self.ui.resumeSearchTimeoutSpinBox.value()
        msg.acquire_timeout = self.ui.acquireTimeoutSpinBox.value()
        msg.track_timeout = self.ui.trackTimeoutSpinBox.value()
        msg.track_duration = self.ui.trackDurationSpinBox.value()
        
        msg.track_max_range = self.ui.trackMaxRangeSpinBox.value()
        msg.track_max_bearing = self.ui.trackMaxBearingSpinBox.value()
        msg.track_max_z = self.ui.trackMaxZSpinBox.value()
        msg.track_range_gain = self.ui.trackRangeGainSpinBox.value()
        msg.track_bearing_gain = self.ui.trackBearingGainSpinBox.value()
        msg.track_z_gain = self.ui.trackZGainSpinBox.value()

        msg.classes_to_track = ",".join(self.get_trackable_classes())
        msg.target_class = self.ui.targetClassComboBox.currentText()
        msg.control_mode = self.ui.controlModeComboBox.currentText()
        msg.acquire_mode = self.ui.acquireModeComboBox.currentText()
        msg.track_mode = self.ui.trackModeComboBox.currentText()

        msg.ignore_class_label_in_track = int(self.ui.ignoreClassLabelInTrack.isChecked())
        msg.class_label_history = int(self.ui.classLabelHistorySpinBox.value())

        msg.state_number = self.supervisor.current_state
        if self.supervisor.current_state == 0:
            msg.state_name = "idle"
        elif self.supervisor.current_state == 1:
            msg.state_name = "search"
        elif self.supervisor.current_state == 2:
            msg.state_name = "acquire"
        elif self.supervisor.current_state == 3:
            msg.state_name = "track"

        self.lc.publish('SUPERVISOR_CFG', msg.encode())
        

    def drawStereoFrames(self, image):

        if len(self.video_output) > 0:
            self.video_buffer.append(image)
        
        # Add state overlay
        state_text = ''
        draw_color = (255, 255, 255)
        if self.supervisor.adv_nav:
            if self.supervisor.current_state == 0:
                state_text = 'IDLE'
                draw_color = (180, 180, 180)
            elif self.supervisor.current_state == 1:
                state_text = 'SEARCH'
                draw_color = (100, 255, 100)
            elif self.supervisor.current_state == 2:
                state_text = 'ACQUIRE'
                draw_color = (100, 200, 255) 
            elif self.supervisor.current_state == 3:
                state_text = 'TRACK'
                draw_color = (100, 100, 255)
        else:
            state_text = 'PILOT'
            draw_color = (220, 180, 180)

        cv2.putText(image, state_text, (40, 105), cv2.FONT_HERSHEY_COMPLEX, 1.0, draw_color)


        image = cv2.resize(image, (int(image.shape[1] / self.window_scaling), int(image.shape[0] / self.window_scaling)))
        cv2.imshow(self.cv_window_name, image)
        cv2.waitKey(1)

        if not self.window_enabled:
            cv2.setMouseCallback(self.cv_window_name, self.handle_mouse_input)
            self.window_enabled = True

    def handle_mouse_input(self, event, x, y, flags, param):
        if event == cv2.EVENT_LBUTTONDOWN and flags & cv2.EVENT_FLAG_ALTKEY:
            pos = (x*self.window_scaling, y*self.window_scaling)
            self.lcm_manager.handler.handle_mouse_input(pos)


    def closeEvent(self, event):

        if len(self.video_output) > 0:
            if len(self.video_buffer) > 0:
                logger.info('Saving video_buffer...')
                fourcc = cv2.VideoWriter_fourcc('h','2','6','4')
                self.video_out = cv2.VideoWriter('boxview-video-'+str(int(time.time()))+'.mp4', fourcc, 10.0, (2064, 772))
                for frame in self.video_buffer:
                    self.video_out.write(frame)
                self.video_out.release()
                logger.info('Done saving video buffer')
            else:
                self.video_out.release()
        GuiSave(self, QtCore.QSettings(os.path.join('config', 'gui.ini'), QtCore.QSettings.IniFormat))
        self.camera_control.stop()
        self.lcm_manager.handler.object_tracker.stop()
        self.lcm_manager.handler.world_frame.stop()
        self.lcm_manager.handler.stop()
        self.lcm_manager.stop()
        self.supervisor.stop()
        logger.info('GUI Closed')
        sys.exit(0)


## Main Entry Point
if __name__ == '__main__':
    import sys

    with logger.catch("Error in main entry point"):

        win = MainWindow(sys.argv)

        if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
            QtGui.QApplication.instance().exec_()