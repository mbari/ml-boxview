# -*- coding: utf-8 -*-
"""
image_sync.py -- Class for receiving left- and right-images from LCM and publishing combined stereo image
Author: pldr
Copyright 2020  Monterey Bay Aquarium Research Institute
Distributed under MIT license. See license.txt for more information.

"""

import lcm
import time
import numpy as np
from collections import deque
from mwt.stereo_image_t import stereo_image_t
from mwt.image_t import image_t

def stereo_frame_2_msg(msg, left, right):

    msg.left_utime = left.utime
    msg.right_utime = right.utime
    left_frame = msg_2_frame(left)
    right_frame = msg_2_frame(right)
    stereo_img = np.hstack((left_frame, right_frame))
    #print(stereo_img.shape)
    msg.width = stereo_img.shape[1]
    msg.height = stereo_img.shape[0]
    msg.data = stereo_img.tobytes()

    msg.size = len(msg.data)
    if len(stereo_img.shape) > 2:
        msg.pixelformat = image_t.PIXEL_FORMAT_RGB
    else:
        msg.pixelformat = image_t.PIXEL_FORMAT_GRAY
    return msg, stereo_img

def msg_2_frame(msg):

    frame = np.frombuffer(msg.data, dtype=np.uint8)
    # a few conditions here for image types, many more are possible
    if msg.pixelformat == image_t.PIXEL_FORMAT_RGB:
        bytes_per_pixel = 3
    if msg.pixelformat == image_t.PIXEL_FORMAT_GRAY:
        bytes_per_pixel = 1
    frame = np.squeeze(np.reshape(frame, newshape=(msg.height, msg.width, bytes_per_pixel)))

    return frame

class ImageSync:

    def __init__(self, max_wait=100):
        self.left_image = None
        self.right_image = None
        self.l_buf = deque()
        self.r_buf = deque()
        self.left_channel = 'MANTA_LEFT'
        self.right_channel = 'MANTA_RIGHT'
        # self.left_channel = 'Vim0' # used for pre April 2019 data
        # self.right_channel = 'Vim1' # used for pre April 2019 data
        self.lc = lcm.LCM("udpm://239.255.76.67:7667?ttl=0")

    def __del__(self):
        self.wait()

    def stop(self):
        self.stop_thread = True

    def handle_image(self, channel, data):
        if channel == self.left_channel:
            self.left_image = image_t.decode(data)
            if len(str(self.left_image.utime)) < 16:
                self.left_image.utime = self.left_image.utime * 1000
            self.l_buf.append(self.left_image)
        if channel == self.right_channel:
            self.right_image = image_t.decode(data)
            if len(str(self.right_image.utime)) < 16:
                self.right_image.utime = self.right_image.utime * 1000
            self.r_buf.append(self.right_image)

        for l_b in self.l_buf:
            for r_b in self.r_buf:
                if np.abs(l_b.utime - r_b.utime) < 100000:
                    img_msg = stereo_image_t()
                    stereo_msg, stereo_img = stereo_frame_2_msg(img_msg, self.left_image, self.right_image)
                    self.lc.publish('MWT_STEREO_IMAGE', stereo_msg.encode())
                    self.l_buf.remove(l_b)
                    self.r_buf.remove(r_b)
                    return


    def run(self):

        # LCM on local machine only
        # Image handles for ML
        self.lc.subscribe(self.left_channel, self.handle_image)
        self.lc.subscribe(self.right_channel, self.handle_image)

        try:
            while True:
                self.lc.handle_timeout(1000)

        except KeyboardInterrupt:
            pass


if __name__=="__main__":

    import sys

    image_sync = ImageSync()
    image_sync.run()


