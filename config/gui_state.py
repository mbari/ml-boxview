# -*- coding: utf-8 -*-
"""
gui_state.py -- two methods to saving and restoring the GUI state from pyqt
Author: pldr
Copyright 2020  Monterey Bay Aquarium Research Institute
Distributed under MIT license. See license.txt for more information.

"""

from pyqtgraph.Qt import QtWidgets
from pyqtgraph.Qt import QtCore
import inspect
from loguru import logger

def GetHandledTypes():
    return (QtWidgets.QTextEdit, QtWidgets.QComboBox, QtWidgets.QLineEdit, QtWidgets.QCheckBox, QtWidgets.QRadioButton, QtWidgets.QDoubleSpinBox, QtWidgets.QSpinBox, QtWidgets.QSlider, QtWidgets.QListWidget)

def IsHandledType(widget):
    return any(isinstance(widget, t) for t in GetHandledTypes())

#===================================================================
# save "ui" controls and values to registry "setting"
#===================================================================

def GuiSave(ui : QtWidgets.QWidget, settings : QtCore.QSettings, uiName="uiwidget"):
    namePrefix = f"{uiName}/"
    settings.setValue(namePrefix + "geometry", ui.saveGeometry())

    for name, obj in inspect.getmembers(ui.ui):

        if not IsHandledType(obj):
            continue

        name = obj.objectName()
        value = None
        if isinstance(obj, QtWidgets.QComboBox):
            index = obj.currentIndex()  # get current index from combobox
            value = obj.itemText(index)  # get the text for current index

        if isinstance(obj, QtWidgets.QLineEdit):
            value = obj.text()

        if isinstance(obj, QtWidgets.QCheckBox):
            value = obj.isChecked()

        if isinstance(obj, QtWidgets.QRadioButton):
            value = obj.isChecked()

        if isinstance(obj, QtWidgets.QSpinBox):
            value = obj.value()

        if isinstance(obj, QtWidgets.QDoubleSpinBox):
            value = obj.value()

        if isinstance(obj, QtWidgets.QSlider):
            value = obj.value()

        if isinstance(obj, QtWidgets.QTextEdit):
            value = obj.toPlainText()

        if isinstance(obj, QtWidgets.QListWidget):
            settings.beginWriteArray(name)
            for i in range(obj.count()):
                settings.setArrayIndex(i)
                settings.setValue(namePrefix + name, obj.item(i).text())
            settings.endArray()
        elif value is not None:
            settings.setValue(namePrefix + name, value)

        if value is not None:
            logger.info("Saving : " + name + " to " + str(value))

#===================================================================
# restore "ui" controls with values stored in registry "settings"
#===================================================================

def GuiRestore(ui : QtWidgets.QWidget, settings : QtCore.QSettings, uiName="uiwidget"):
    from distutils.util import strtobool

    namePrefix = f"{uiName}/"
    geometryValue = settings.value(namePrefix + "geometry")
    if geometryValue:
        ui.restoreGeometry(geometryValue)

    for name, obj in inspect.getmembers(ui.ui):

        if not IsHandledType(obj):
            continue

        name = obj.objectName()
        value = None
        if not isinstance(obj, QtWidgets.QListWidget):
            value = settings.value(namePrefix + name)
            if value is None:
                continue

        if isinstance(obj, QtWidgets.QComboBox):
            index = obj.findText(value)  # get the corresponding index for specified string in combobox

            if index == -1:  # add to list if not found
                obj.insertItems(0, [value])
                index = obj.findText(value)
                obj.setCurrentIndex(index)
            else:
                obj.setCurrentIndex(index)  # preselect a combobox value by index

        if isinstance(obj, QtWidgets.QLineEdit):
            obj.setText(value)

        if isinstance(obj, QtWidgets.QTextEdit):
            obj.setPlainText(value)

        if isinstance(obj, QtWidgets.QCheckBox):
            obj.setChecked(strtobool(value))

        if isinstance(obj, QtWidgets.QRadioButton):
            obj.setChecked(strtobool(value))

        if isinstance(obj, QtWidgets.QSlider):
            obj.setValue(int(value))

        if isinstance(obj, QtWidgets.QDoubleSpinBox):
            obj.setValue(float(value))

        elif isinstance(obj, QtWidgets.QSpinBox):
            obj.setValue(int(value))

        if isinstance(obj, QtWidgets.QListWidget):
            size = settings.beginReadArray(namePrefix + name)
            for i in range(size):
                settings.setArrayIndex(i)
                value = settings.value(namePrefix + name)

        if value is not None:
            logger.info("Restoring : " + name + " to " + str(value))