# -*- coding: utf-8 -*-
"""
box_sync.py -- Class to sync LCM box list messages and publish stereo box messages
Author: pldr
Copyright 2020  Monterey Bay Aquarium Research Institute
Distributed under MIT license. See license.txt for more information.

"""

import lcm
import time
import numpy as np
from collections import deque
from mwt.stereo_bounding_box_t import stereo_bounding_box_t
from mwt.bounding_box_list_t import bounding_box_list_t
from mwt.bounding_box_t import bounding_box_t
from mwt.track_list_t import track_list_t

class BoxSync:

    def __init__(self, max_wait=100):
        self.left_boxes = []
        self.right_boxes = []
        self.l_buf = deque()
        self.r_buf = deque()
        self.l_frame_num = 0
        self.r_frame_num = 0
        self.detection_channel = 'DETECTIONS'
        self.lc = lcm.LCM("udpm://239.255.76.67:7667?ttl=0")

    def __del__(self):
        self.wait()

    def stop(self):
        self.stop_thread = True

    def populate_stereo_boxes(self, l_b, r_b):

        output = stereo_bounding_box_t()
        left_box_list = bounding_box_list_t()
        left_box_list.num_boxes = len(l_b)
        right_box_list = bounding_box_list_t()
        right_box_list.num_boxes = len(r_b)
        for box in l_b:
            bounding_box = bounding_box_t()
            bounding_box.class_name = box.label
            bounding_box.top = box.top
            bounding_box.left = box.left
            bounding_box.width = box.width
            bounding_box.height = box.height 
            bounding_box.num_classes = 1
            bounding_box.scores = [box.confidence]
            left_box_list.boxes.append(bounding_box)
        
        for box in r_b:
            bounding_box = bounding_box_t()
            bounding_box.class_name = box.label
            bounding_box.top = box.top
            bounding_box.left = box.left
            bounding_box.width = box.width
            bounding_box.height = box.height 
            bounding_box.num_classes = 1
            bounding_box.scores = [box.confidence]
            right_box_list.boxes.append(bounding_box)
        
        output.left_boxes = left_box_list
        output.right_boxes = right_box_list

        return output

    def handle_box(self, channel, data):
        track_list = track_list_t.decode(data)
        
        print(track_list.num_tracks)

        # parse track_list into left and right 
        for trk in track_list.tracks:
            if trk.source_id == 0:
                """
                if self.left_boxes == None:
                    self.left_boxes = []
                    self.l_frame_num = trk.frame
                elif trk.frame > self.l_frame_num:
                    self.l_buf.append(self.left_boxes)
                    self.left_boxes = []
                    self.left_boxes.append(trk)
                    self.l_frame_num = trk.frame
                else:
                    self.left_boxes.append(trk)
                """
                self.left_boxes.append(trk)
            else:
                """
                if self.right_boxes == None:
                    self.right_boxes = []
                    self.r_frame_num = trk.frame
                elif trk.frame > self.l_frame_num:
                    self.r_buf.append(self.right_boxes)
                    self.right_boxes =[]
                    self.right_boxes.append(trk)
                    self.r_frame_num = trk.frame
                else:
                    self.right_boxes.append(trk)
                """
                self.right_boxes.append(trk)
            
            """
            for l_b in self.l_buf:
                for r_b in self.r_buf:
                    if len(l_b) > 0 and len(r_b) > 0:
                        output = self.populate_stereo_boxes(l_b, r_b)
                        self.lc.publish('BOX_STEREO', output.encode())
                        self.l_buf.remove(l_b)
                        self.r_buf.remove(r_b)
                        return
            """
        
        output = self.populate_stereo_boxes(self.left_boxes, self.right_boxes)
        self.lc.publish('BOX_STEREO', output.encode())
        #self.l_buf.remove(l_b)
        #self.r_buf.remove(r_b)
        self.left_boxes = []
        self.right_boxes = []
        return


    def run(self):

        # LCM on local machine only
        # Image handles for ML
        self.lc.subscribe(self.detection_channel, self.handle_box)

        try:
            while True:
                self.lc.handle_timeout(1000)

        except KeyboardInterrupt:
            pass

if __name__=="__main__":

    import sys

    bs = BoxSync()
    bs.run()


