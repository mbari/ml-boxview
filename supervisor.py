import os
import lcm
import json
import time
import numpy as np
from collections import deque
from libs.stereo_calc import StereoCalc, get_pos_in_rba
import pyqtgraph as pg
from loguru import logger
from pyqtgraph.Qt import QtCore, QtGui, QtWidgets
from config.gui_state import GuiSave, GuiRestore

# LCM types
from mwt.stereo_bounding_box_t import stereo_bounding_box_t
from mwt.vehicle_cmd_t import vehicle_cmd_t
from mwt.mwt_control_status_t import mwt_control_status_t
from mwt.mwt_search_status_t import mwt_search_status_t
from mwt.mini_rov_status_t import mini_rov_status_t
from mwt.mwt_target_t import mwt_target_t
from mwt.mini_rov_attitude_t import mini_rov_attitude_t
from mwt.mini_rov_depth_t import mini_rov_depth_t
from pcon.ui_msg_t import ui_msg_t

pg.mkQApp()

## Define main window class from template
path = os.path.dirname(os.path.abspath(__file__))
uiFile = os.path.join(path, 'supervisor.ui')
WindowTemplate, TemplateBaseClass = pg.Qt.loadUiType(uiFile)


class Supervisor(QtCore.QThread):

    # Passthough lcm objects to GUI
    mwt_target_sig = QtCore.Signal(object)
    search_stat_sig = QtCore.Signal(object)
    state_change_sig = QtCore.Signal(str)

    def __init__(self):
        QtCore.QThread.__init__(self)
        self.stop_thread = False
        self.target_class = 'solmissus'
        self.latest_mwt_target = None
        self.state_control = 'Manual'
        self.state = 'IDLE'
        self.possible_states = ['IDLE', 'SEARCH', 'ACQUIRE', 'CONFIRM', 'TRACK', 'REACQUIRE']
        self.state_changes = ['SEARCH-ACQUIRE', 'ACQUIRE-TRACK','TRACK-REACQUIRE','REACQUIRE-TRACK','REACQUIRE-SEARCH']

        self.state_change_buttons = None
        self.lc = lcm.LCM("udpm://239.255.76.67:7667?ttl=0")

        self.target_timer = QtCore.QTimer()
        self.target_timer.timeout.connect(self.state_update)
        self.target_timer.setInterval(100)
        self.target_timer.start()

        self.REAQUIRE_TIMEOUT = 5
        self.ACQUIRE_TIMEOUT = 4
        self.TRACK_TIMEOUT = 5
        self.CONFIRM_TIMEOUT = 4


    def __del__(self):
        self.wait()

    def stop(self):
        self.stop_thread = True

    def state_update(self):

        # If manual control just return
        if self.state_control.lower() == 'manual':
            return

        last_state = self.state

        # State transition logic
        if self.state == 'SEARCH':
            if self.latest_tracker_time > 0:
                self.state = 'ACQUIRE'
                self.first_tracker_time = time.time()
        elif self.state == 'ACQUIRE':
            if not self.tracker_data_valid:
                self.state = 'SEARCH'
                self.first_tracker_time = -1
                self.latest_tracker_time = -1
            elif self.latest_tracker_class == self.target_class and (self.latest_tracker_time - self.first_tracker_time > self.ACQUIRE_TIMEOUT):
                self.state = 'CONFIRM'
                self.first_tracker_time = time.time()
            else:
                if self.latest_tracker_time - self.first_tracker_time > self.ACQUIRE_TIMEOUT:
                    self.state = 'SEARCH'
                    self.first_tracker_time = -1
                    self.latest_tracker_time = -1
        elif self.state == 'CONFIRM':
            if not self.tracker_data_valid:
                self.state = 'REACQUIRE'
                self.first_tracker_time = -1
                self.latest_tracker_time = -1
                self.first_tracker_time = time.time()
            elif self.latest_tracker_class == self.target_class:
                self.state = 'TRACK'
                self.first_tracker_time = time.time()
            elif self.latest_tracker_time - self.first_tracker_time > self.CONFIRM_TIMEOUT:
                self.state = 'SEARCH'
                self.first_tracker_time = -1
                self.latest_tracker_time = -1
        elif self.state == 'REACQUIRE':
            if not self.tracker_data_valid:
                self.state = 'SEARCH'
            if self.latest_tracker_time > 0 and time.time() - self.latest_tracker_time > self.REAQUIRE_TIMEOUT:
                self.state = 'SEARCH'
            elif self.latest_tracker_class == self.target_class:
                self.state = 'ACQUIRE'
                self.first_tracker_time = time.time()
        elif self.state == 'TRACK':
            if not self.tracker_data_valid:
                self.state = 'REACQUIRE'
                self.first_tracker_time = time.time()
            if self.latest_tracker_time > 0 and time.time() - self.latest_tracker_time > self.TRACK_TIMEOUT:
                self.state = 'REACQUIRE'
                self.first_tracker_time = time.time()
        
        if last_state != self.state:
            self.state_change_sig.emit(self.state)

    def handle_mwt_target(self, channel, data):
        self.mwt_target_sig.emit(data)
        self.latest_mwt_target = mwt_target_t.decode(data)
        self.latest_tracker_time = time.time()
        logger.debug('Received MWT_TARGET at: ' + str(time.time()))

    def handle_search_stat(self, channel, data):
        self.search_stat_sig.emit(data)

    def setContVar(self, cmd, val):
        """send string to the controller of the form: cmd = val 

        Args:
            cmd (string): The variable name
            val (double): the variable value
        """
        self.sendContCmd(cmd + ' = ' + str(val))

    def callContFunc(self,cmd, val):
        """Send string to the controller of the form: func(val)

        Args:
            cmd (string): The function name
            val (double): the variable value
        """
        self.sendContCmd(cmd + '(' + str(val) + ')')

    def sendContCmd(self, cmd):
        """Send command string to controller

        Args:
            cnd (string): the string to send
        """
        msg = ui_msg_t()
        msg.msg = cmd
        self.lc.publish('UI_CONTROL_REQ', msg.encode())

    def sendContCmds(self, cmds):
        """Send list of command strings to controller

        Args:
            cmds (list): list of strings
        """
        for cmd in cmds:
            self.sendContCmd(cmd)
            time.sleep(0.05)

    def run(self):

        # LCM on local machine only

        logger.info("Creating subscriptions...")
        self.lc.subscribe('MWT_TARGET', self.handle_mwt_target)
        self.lc.subscribe(('MWT_SEARCH_STAT'), self.handle_search_stat )

        logger.info('Supervisor thread starting...')

        while self.isRunning and not self.stop_thread:
            self.lc.handle_timeout(1000)

        logger.info('Supervisor thread stopped.')

class MainWindow(TemplateBaseClass):

    def __init__(self, argv):

        TemplateBaseClass.__init__(self)
        self.setWindowTitle('Supervisor - Python - Qt')

        # Create the main windowLOG.info('Supervisor thread stopped')
        self.ui = WindowTemplate()
        self.ui.setupUi(self)

        # Create the supervisor
        self.supervisor = Supervisor()
        self.latest_mwt_target = None

        # populate target class from labels.txt
        with open(os.path.join('config', 'labels.txt')) as f:
            lines = f.readlines()
            self.ui.targetClassComboBox.clear()
            for line in sorted(lines):
                self.ui.targetClassComboBox.addItem(line.lower().strip('\t\r\n'))

        # restore previous GUI settings
        GuiRestore(self, QtCore.QSettings(os.path.join('config', 'supervisor_gui.ini'), QtCore.QSettings.IniFormat))

        # Populate commands
        self.command_names = []
        with open(os.path.join('config', 'controller_cmds.txt')) as f:
            lines = f.readlines()
            for line in sorted(lines):
                self.command_names.append(line.strip('\t\r\n'))
        self.command_completer = QtWidgets.QCompleter(self.command_names)
        self.ui.commandEdit.setCompleter(self.command_completer)

        # Command editors
        self.cmd_editors = [
            self.ui.idleEdit,
            self.ui.idleToSearchEdit,
            self.ui.searchToAcquireEdit,
            self.ui.acquireToTrackEdit,
            self.ui.trackToReacquireEdit,
            self.ui.reacquireToTrackEdit,
            self.ui.reacquireToSearchEdit,
        ]

        # Command lists
        self.cmd_lists = [
            self.ui.idleCmds,
            self.ui.idleToSearchCmds,
            self.ui.searchToAcquireCmds,
            self.ui.acquireToTrackCmds,
            self.ui.trackToReacquireCmds,
            self.ui.reacquireToTrackCmds,
            self.ui.reacquireToSearchCmds,
        ]

        # Command Buttons
        self.cmd_buttons = [
            self.ui.sendIdle,
            self.ui.sendIdleToSearch,
            self.ui.sendSearchToAcquire,
            self.ui.sendAcquireToTrack,
            self.ui.sendTrackToReacquire,
            self.ui.sendReacquireToTrack,
            self.ui.sendReacquireToSearch,
        ]

        # Hacky, but helpful to have these buttons in the surpervisor state loop
        self.supervisor.state_change_buttons = self.cmd_buttons

        # State transitions
        self.cmd_states = [
            'IDLE',
            'SEARCH',
            'ACQUIRE',
            'TRACK',
            'REACQUIRE',
            'TRACK',
            'SEARCH'
        ]

        # connect autocomplete and slots for state buttons and command lists
        for i, ed in enumerate(self.cmd_editors):
            ed.setCompleter(self.command_completer)
            ed.returnPressed.connect(self.updateCmds)
            self.cmd_buttons[i].clicked.connect(self.sendCmdList)

        # Set the initial supervisor values from UI defaults
        self.supervisor.target_class = self.ui.targetClassComboBox.currentText()
        self.supervisor.state_control = self.ui.stateControlComboBox.currentText()

        # Signals from Supervisor
        self.supervisor.search_stat_sig.connect(self.update_search_stat)
        self.supervisor.mwt_target_sig.connect(self.update_mwt_target)
        self.supervisor.state_change_sig.connect(self.update_state)

        # Start the supervisor
        self.supervisor.start()
        self.last_supervisor_state = self.supervisor.state

        # Signals from UI
        self.ui.yEffort.editingFinished.connect(self.updateYEffort)
        self.ui.xEffort.editingFinished.connect(self.updateXEffort)
        self.ui.depthCmd.editingFinished.connect(self.updateDepthCmd)
        self.ui.headingCmd.editingFinished.connect(self.updateHeadingCmd)
        self.ui.zEffort.editingFinished.connect(self.updateZEffort)
        self.ui.yawEffort.editingFinished.connect(self.updateYawEffort)
        self.ui.searchModeComboBox.currentTextChanged.connect(self.updateSearchMode)
        self.ui.targetClassComboBox.currentTextChanged.connect(self.updateTargetClass)
        self.ui.stateControlComboBox.currentTextChanged.connect(self.updateStateControl)
        self.ui.sendCommand.clicked.connect(self.pubCommand)
        self.ui.commandEdit.returnPressed.connect(self.pubCommand)

        # State management in UI
        self.states = {}
        self.states['IDLE'] = self.ui.idleButton
        self.states['SEARCH'] = self.ui.searchButton
        self.states['REACQUIRE'] = self.ui.reacquireButton
        self.states['ACQUIRE'] = self.ui.acquireButton
        self.states['CONFIRM'] = self.ui.confirmButton
        self.states['TRACK'] = self.ui.trackButton

        # Connect buttons to allow manual control over state transitions
        self.states['IDLE'].clicked.connect(lambda: self.manual_state_update('IDLE'))
        self.states['SEARCH'].clicked.connect(lambda: self.manual_state_update('SEARCH'))
        self.states['REACQUIRE'].clicked.connect(lambda: self.manual_state_update('REACQUIRE'))
        self.states['ACQUIRE'].clicked.connect(lambda: self.manual_state_update('ACQUIRE'))
        self.states['CONFIRM'].clicked.connect(lambda: self.manual_state_update('CONFIRM'))
        self.states['TRACK'].clicked.connect(lambda: self.manual_state_update('TRACK'))

        # State display timer
        #self.state_timer = QtCore.QTimer()
        #self.state_timer.timeout.connect(self.state_display)
        #self.state_timer.setInterval(250)
        #self.state_timer.start()

        # Show the GUI controls
        self.show()

    def closeEvent(self, event):

        GuiSave(self, QtCore.QSettings(os.path.join('config', 'supervisor_gui.ini'), QtCore.QSettings.IniFormat))
        self.supervisor.stop()
        logger.info('GUI Closed')
        sys.exit(0)

    def validateCmd(self,cmd):
        """check that cmd matches one in the command list

        Args:
            cmd (string): the command string 
        """
        stub = ''
        if '(' in cmd:
            stub = cmd.split('(')[0] + '('
            logger.debug(stub)
        elif '=' in cmd:
            stub = cmd.split('=')[0]
            logger.debug(stub)
        else:
            return False

        try:
            ind = self.command_names.index(stub)
            return True
        except ValueError as e:
            logger.debug(e + ' : ' + stub)
            return False
        
        return False

    def updateCmds(self):
        # validate input
        cmd = self.sender().text()
        if not self.validateCmd(cmd):
            return False
        else:
            ind = self.cmd_editors.index(self.sender())
            cur_text = self.cmd_lists[ind].toPlainText()
            if len(cur_text) > 0:
                 cur_text += '\n'
            cur_text += cmd
            self.cmd_lists[ind].setPlainText(cur_text)
            self.sender().clear()
            return True

    def sendCmdList(self):
        ind = self.cmd_buttons.index(self.sender())
        self.supervisor.sendContCmds(self.cmd_lists[ind].toPlainText().split('\n'))
        self.manual_state_update(self.cmd_states[ind])

    def pubCommand(self):
        cmd = self.ui.commandEdit.text()
        if self.validateCmd(cmd):
            self.supervisor.sendContCmd(cmd)
            self.ui.commandEdit.clear()

    def updateSearchMode(self, search_mode):
        if search_mode == 'Idle':
            self.supervisor.sendContCmd('mode(1)')
        elif search_mode == 'Search':
            self.supervisor.sendContCmd('mode(2)')
            self.supervisor.sendContCmd('zSearchMode(2)')
            self.supervisor.sendContCmd('yawSearchMode(2)')
        elif search_mode == 'Track':
            self.supervisor.sendContCmd('mode(3)')

    def updateStateControl(self, val):
        self.supervisor.state_control = val

    def updateXEffort(self):
        self.supervisor.sendContCmd('xSearchEffort=' + str(self.ui.xEffort.value()))

    def updateYEffort(self):
        self.supervisor.sendContCmd('ySearchEffort=' + str(self.ui.yEffort.value()))

    def updateZEffort(self):
        self.supervisor.sendContCmd('zSearchEffort=' + str(self.ui.zEffort.value()))

    def updateYawEffort(self):
        self.supervisor.sendContCmd('yawSearchEffort=' + str(self.ui.yawEffort.value()))

    def updateDepthCmd(self):
        cmd = 'zSearchMoveAbs(' + str(self.ui.depthCmd.value()) + ')'
        logger.debug(cmd)
        self.supervisor.sendContCmd(cmd)

    def updateHeadingCmd(self):
        new_heading = self.ui.headingCmd.value()
        if new_heading < 0:
            self.supervisor.sendContCmd('yawSearchMoveAbsNeg(' + str(abs(new_heading)) + ')')
        else:
            self.supervisor.sendContCmd('yawSearchMoveAbsPos(' + str(abs(new_heading)) + ')')

    def manual_state_update(self, state_val='NONE'):
        logger.debug(state_val)
        self.supervisor.state = state_val

    def updateTargetClass(self, target_class):
        self.supervisor.target_class = target_class

    def update_mwt_target(self, data):
        self.latest_mwt_target = mwt_target_t.decode(data)
        self.ui.rangeSpinBox.setValue(self.latest_mwt_target.range_meters)
        self.ui.bearingSpinBox.setValue(self.latest_mwt_target.bearing_degrees)
        self.ui.zSpinBox.setValue(self.latest_mwt_target.z_meters)

    def update_search_stat(self, data):
        stat = mwt_search_status_t.decode(data)
        
        # Populate the UI 
        if stat.control_mode == 1:
            self.ui.controlModeIdle.setChecked(True)
        elif stat.control_mode == 2:
            self.ui.controlModeSearch.setChecked(True)
        elif stat.control_mode == 3:
            self.ui.controlModeTrack.setChecked(True)

        if stat.x_mode == 1:
            self.ui.xEffortButton.setChecked(True)
        else:
            self.ui.xEffortButton.setChecked(False)
        self.ui.xEffortCmd.setValue(stat.x_effort_cmd)
        self.ui.xEffortOutput.setValue(stat.x_output)

        if stat.y_mode == 1:
            self.ui.yEffortButton.setChecked(True)
        else:
            self.ui.yEffortButton.setChecked(False)
        self.ui.yEffortCmd.setValue(stat.y_effort_cmd)
        self.ui.yEffortOutput.setValue(stat.y_output)

        if stat.z_mode == 1:
            self.ui.zEffortButton.setChecked(True)
        elif stat.z_mode == 2:
            self.ui.zDepthButton.setChecked(True)
        self.ui.zEffortCmd.setValue(stat.z_set_point)
        self.ui.zEffortOutput.setValue(stat.z_output)

        if stat.yaw_mode == 1:
            self.ui.yawEffortButton.setChecked(True)
        elif stat.yaw_mode == 2:
            self.ui.yawDepthButton.setChecked(True)
        self.ui.yawEffortCmd.setValue(stat.yaw_set_point)
        self.ui.yawEffortOutput.setValue(stat.yaw_output)
        

    def update_state(self, state):
        for s in self.states.keys():
            if s == state:
                self.states[s].setStyleSheet("background-color : yellow")
            else:
                self.states[s].setStyleSheet("")    
        
        # click button for state transiton
        if self.last_supervisor_state == 'SEARCH' and state == 'ACQUIRE':
            self.ui.sendSearchToAcquire.click()
        if self.last_supervisor_state == 'ACQUIRE' and state == 'TRACK':
            self.ui.sendAcquireToTrack.click()
        if self.last_supervisor_state == 'TRACK' and state == 'REACQUIRE':
            self.ui.sendTrackToReacquire.click()
        if self.last_supervisor_state == 'REACQUIRE' and state == 'TRACK':
            self.ui.sendReacquireToTrack.click()
        if self.last_supervisor_state == 'REACQUIRE' and state == 'SEARCH':
            self.ui.sendReacquireToSearch.click()

        self.last_supervisor_state = state

## Main Entry Point
if __name__ == '__main__':
    import sys

    win = MainWindow(sys.argv)

    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()